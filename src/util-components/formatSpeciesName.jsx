// Formats the APNI Species name with genus, species and author if applicable
export function formatSpeciesName(input) {
  if (!input || typeof input !== "string") {
    throw new Error("Input must be a non-empty string.");
  }

  // Split the input into words
  const words = input.trim().split(/\s+/);

  if (words.length < 2) {
    return <span>{input}</span>;
  }

  // Extract the parts of original string
  const [genus, species, ...authorParts] = words;

  // Capitalize genus and lowercase species
  const updatedGenus = genus.charAt(0).toUpperCase() + genus.slice(1).toLowerCase();
  const updatedSpecies = species.toLowerCase();

  const author = authorParts.join(" ");

  return (
    <span>
      <i>
        {updatedGenus}
        &nbsp;
        {updatedSpecies}
      </i>
      {author && ` ${author}`}
    </span>
  );
}
