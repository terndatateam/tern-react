import {
  MapContainer,
  ScaleControl,
  TileLayer,
  LayersControl,
  WMSTileLayer,
} from "react-leaflet";
import "leaflet-draw";
import "leaflet/dist/leaflet.css";
import "./Map.scss";
import PropTypes from "prop-types";
import L from "leaflet";
import { MapControl } from "./MapControl.jsx";
import { DrawLayer } from "./DrawLayer.jsx";
import { Notifications } from "../notification/Notifications.jsx";
import { useNotifications } from "../hooks/useNotification.js";
import { getChildComponent } from "./mapUtil.js";
import { UsageGuide } from "./UsageGuide.jsx";

/**
 *
 * This is the main map component.
 *
 * This will handle rendering the map and toolbars, legends, and additional custom components to inject.
 *
 * - initMap: developers can initilise and pass through the hook if access map state is needed such
 *  as zoom or bounds
 * - Cluster Markers can be rendered using the `ClusterMarker` component which can be rendered
 * as a child under the `Map` component.
 * Available colours include, "green", "yellow", "orange", "red".
 * - Simple location markers can be rendered as a child of the `Map` component as well.
 * Custom icons can be passed in as HTML.
 * - All markers must provide `location` with an object `{ lat: number, lon: number }`, `id` string
 * - Passing in configs can be done by passing an object to the `useTernMap()` hook as a parameter. Any new configs
 * will override default settings such as `{ ENABLE_DRAWING: false, ENABLE_GEOSEARCH: false }`
 * - height and width can be passed into control the styling of the react-leaflet map container
 * - FeatureId to show specific layers in the WMS Layer from geoserver
 *
 */
export function Map({
  initMap,
  height,
  width,
  children,
  featureId,
  toolbarSize,
  onDeleteShape,
  onCreateShape,
  zoomToolsOnly,
  scaleControlPosition,
  hasLayerSwitch,
}) {
  const {
    mapInitState,
    config,
    drawMode,
    toggleDrawing,
    bounds,
    currentShape,
    shapeStyling,
    setCurrentShape,
  } = initMap;

  const initialBounds = L.geoJson(bounds).getBounds();

  // initiliase notifications for map errors
  const { pushNotification, notifications, closeNotification } = useNotifications();

  const mapInitPosition = [mapInitState.lat, mapInitState.lng];

  const onCreate = (map) => {
    const resizeObserver = new ResizeObserver(() => {
      map.invalidateSize();
    });
    resizeObserver.observe(map.getContainer());
  };

  // attempts to get usage guide, map control will not render if undefined
  const usageGuide = getChildComponent(children, UsageGuide);

  return (
    <div className="map-container">
      <Notifications notifications={notifications} closeNotification={closeNotification} />
      <MapContainer
        className="region-map"
        center={mapInitPosition}
        style={{ height, width }}
        scrollWheelZoom={false}
        zoomSnap={mapInitState.zoomSnap}
        bounds={initialBounds}
        whenCreated={onCreate}
        doubleClickZoom="center"
        attributionControl={false}
        zoomControl={false}
      >
        <ScaleControl position={scaleControlPosition} />
        {!hasLayerSwitch && (
          <TileLayer
            attribution='&copy; <a href="https://carto.com/attributions">CARTO</a>'
            url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
          />
        )}
        <MapControl
          ternMap={initMap}
          usageGuide={usageGuide}
          size={toolbarSize}
          onDeleteShape={onDeleteShape}
          zoomToolsOnly={zoomToolsOnly}
        />
        {hasLayerSwitch && (
          <LayersControl position="topright">
            <LayersControl.BaseLayer name="Default" checked>
              <TileLayer
                attribution='&copy; <a href="https://carto.com/attributions">CARTO</a>'
                url="https://{s}.basemaps.cartocdn.com/light_all/{z}/{x}/{y}{r}.png"
              />
            </LayersControl.BaseLayer>
            <LayersControl.BaseLayer name="Satellite">
              <TileLayer
                // eslint-disable-next-line max-len
                attribution="Tiles © Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community"
                url="https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}"
              />
            </LayersControl.BaseLayer>
            <LayersControl.BaseLayer name="Terrain">
              <TileLayer
                attribution="Map data: © OpenStreetMap contributors, SRTM | Map style: © OpenTopoMap (CC-BY-SA)"
                url="https://{s}.tile.opentopomap.org/{z}/{x}/{y}.png"
              />
            </LayersControl.BaseLayer>
          </LayersControl>
        )}
        <DrawLayer
          initMap={initMap}
          drawMode={drawMode}
          toggleDrawing={toggleDrawing}
          currentShape={currentShape}
          setCurrentShape={setCurrentShape}
          shapeStyling={shapeStyling}
          pushNotification={pushNotification}
          onCreateShape={onCreateShape}
        />
        <WMSTileLayer
          key={featureId}
          url={config.GEOSERVER_URL}
          layers={config.GEOSERVER_LAYER}
          format={config.WMS_FORMAT}
          styles={config.WMS_STYLES}
          colors={config.WMS_COLORS}
          srs={config.WMS_SRS}
          version={config.WMS_VERSION}
          opacity={config.WMS_OPACITY}
          featureid={featureId}
          tiled
          transparent
          service="WMS"
          request="GetMap"
          noRedraw={false}
        />
        {children}
      </MapContainer>
    </div>
  );
}

Map.defaultProps = {
  children: null,
  height: "500px",
  width: "500px",
  featureId: null,
  toolbarSize: "sm",
  onCreateShape: () => null,
  onDeleteShape: () => null,
  zoomToolsOnly: false,
  scaleControlPosition: "topright",
  hasLayerSwitch: false,
};

Map.propTypes = {
  initMap: PropTypes.shape().isRequired,
  height: PropTypes.string,
  toolbarSize: PropTypes.oneOf(["md", "sm"]),
  scaleControlPosition: PropTypes.string,
  width: PropTypes.string,
  children: PropTypes.node,
  featureId: PropTypes.string,
  onDeleteShape: PropTypes.func,
  onCreateShape: PropTypes.func,
  zoomToolsOnly: PropTypes.bool,
  hasLayerSwitch: PropTypes.bool,
};
