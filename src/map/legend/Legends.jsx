import Leaflet from "leaflet";
import { useMap } from "react-leaflet";
import PropTypes from "prop-types";
import "./Legends.scss";
import { useEffect, useState } from "react";
import { renderToStaticMarkup } from "react-dom/server";

/** *
 * Legend component
 *
 * Used to list individual legend on map with option to set color, shape (square or circle), and title
 *
 * Custom icon can be passed through which will be rendered as it is and styled further with div.legend-custom
 */
export function Legend({
  color, shape, icon, children,
}) {
  return (
    <div className="legend-item">
      {icon && <div className="legend-custom">{icon}</div>}
      {(shape && color) && <i className={`legend-${shape}`} style={{ backgroundColor: color }} />}
      {children}
      <br />
    </div>
  );
}

Legend.defaultProps = {
  color: null,
  shape: null,
  icon: null,
};

Legend.propTypes = {
  color: PropTypes.string,
  shape: PropTypes.string,
  children: PropTypes.node.isRequired,
  icon: PropTypes.node,
};

/**
 *
 * Legends component
 *
 * This is a reusable component to create a table of legends in the map component
 *
 * All individual legends can be rendered using the `Legend` component as well.
 *
 *
 */
export function Legends({ position, children }) {
  const map = useMap();
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const legend = Leaflet.control({ position });

    legend.onAdd = () => {
      const div = Leaflet.DomUtil.create("div", "info legend map-legend");
      div.innerHTML = `
        <button class="toggle-button ${isOpen ? "open" : ""}">
          ${isOpen ? "Hide" : "Show"} Legends
        </button>
        <div class="legend-content ${isOpen ? "open" : "closed"}">
          ${renderToStaticMarkup(children)}
        </div>
      `;

      div.querySelector(".toggle-button").onclick = () => {
        setIsOpen(!isOpen);
      };

      return div;
    };

    legend.addTo(map);

    return () => {
      legend.remove();
    };
  }, [isOpen, map, position, children]);

  return null;
}

Legends.defaultProps = {
  position: "bottomright",
};

Legends.propTypes = {
  position: PropTypes.string,
  children: PropTypes.node.isRequired,
};
