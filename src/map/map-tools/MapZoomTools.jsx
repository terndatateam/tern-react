import PropTypes from "prop-types";
import {
  Button, UncontrolledTooltip,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faExpand,
  faMinus,
  faPlus,
} from "@fortawesome/free-solid-svg-icons";

export function MapZoomTools({
  zoomIn, zoomOut, resetZoom,
}) {
  return (
    <>
      <Button className="tool-btn map-tools-zoom-in" onClick={zoomIn}>
        <FontAwesomeIcon icon={faPlus} />
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-zoom-in"
          trigger="hover"
        >
          Zoom In
        </UncontrolledTooltip>
      </Button>
      <Button className="tool-btn map-tools-zoom-out" onClick={zoomOut}>
        <FontAwesomeIcon icon={faMinus} />
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-zoom-out"
          trigger="hover"
        >
          Zoom Out
        </UncontrolledTooltip>
      </Button>
      <Button className="tool-btn map-tools-reset" onClick={resetZoom}>
        <FontAwesomeIcon icon={faExpand} />
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-reset"
          trigger="hover"
        >
          Reset Zoom
        </UncontrolledTooltip>
      </Button>
    </>
  );
}

MapZoomTools.propTypes = {
  zoomIn: PropTypes.func.isRequired,
  zoomOut: PropTypes.func.isRequired,
  resetZoom: PropTypes.func.isRequired,
};
