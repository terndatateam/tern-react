import { useState, useEffect, useCallback } from "react";
import { useMap } from "react-leaflet";
import {
  Button, Input, FormGroup, UncontrolledTooltip,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";
import "./ZoomSlider.scss";

/**
 * ZoomSlider component allows users to control the zoom level of a map using a slider.
 * It provides a toggle button to show/hide the slider and updates the map zoom level interactively.
 */
export function ZoomSlider() {
  const map = useMap();
  const [isOpen, setIsOpen] = useState(false);
  const [zoom, setZoom] = useState(map.getZoom());
  const [isSliding, setIsSliding] = useState(false);

  /**
   * Updates the zoom level state when the map zoom changes.
   */
  useEffect(() => {
    const handleZoom = () => {
      setZoom(map.getZoom());
    };

    map.on("zoomend", handleZoom);
    return () => {
      map.off("zoomend", handleZoom);
    };
  }, [map]);

  /**
   * Handles the change in zoom level via the slider input.
   * @param {Event} e - The change event from the slider input.
   */
  const handleZoomChange = useCallback((e) => {
    const newZoom = parseInt(e.target.value, 10);
    setZoom(newZoom);
    map.setZoom(newZoom);
  }, [map]);

  /**
   * Disables map dragging and sets sliding state to true when slider thumb is pressed.
   * @param {Event} e - The mouse down event.
   */
  const handleMouseDown = useCallback((e) => {
    e.stopPropagation();
    map.dragging.disable();
    setIsSliding(true);
  }, [map]);

  /**
   * Enables map dragging and sets sliding state to false when slider thumb is released.
   * @param {Event} e - The mouse up event.
   */
  const handleMouseUp = useCallback((e) => {
    e.stopPropagation();
    setIsSliding(false);
    map.dragging.enable();
  }, [map]);

  /**
   * Updates the zoom level while sliding the slider thumb.
   * @param {Event} e - The mouse move event.
   */
  const handleMouseMove = useCallback((e) => {
    e.stopPropagation();
    if (isSliding) {
      handleZoomChange(e);
    }
  }, [isSliding, handleZoomChange]);

  /**
   * Toggles the visibility of the zoom slider.
   */
  const toggleSlider = () => {
    setIsOpen(!isOpen);
  };

  /**
   * Adds or removes mouse event listeners based on sliding state.
   */
  useEffect(() => {
    if (isSliding) {
      document.addEventListener("mousemove", handleMouseMove);
      document.addEventListener("mouseup", handleMouseUp);
    } else {
      document.removeEventListener("mousemove", handleMouseMove);
      document.removeEventListener("mouseup", handleMouseUp);
    }

    return () => {
      document.removeEventListener("mousemove", handleMouseMove);
      document.removeEventListener("mouseup", handleMouseUp);
    };
  }, [isSliding, handleMouseMove, handleMouseUp]);

  return (
    <div className={`zoom-slider ${isOpen ? "open" : ""}`}>
      <Button id="toggle-button" color="primary" className="toggle-button" onClick={toggleSlider}>
        <FontAwesomeIcon icon={faChevronDown} />
      </Button>
      { !isOpen && (
        <UncontrolledTooltip
          placement="bottom"
          target="toggle-button"
          trigger="hover"
        >
          Zoom Control
        </UncontrolledTooltip>
      )}
      {isOpen && (
        <FormGroup style={{ marginBottom: 0 }}>
          <Input
            id="exampleRange"
            name="range"
            type="range"
            min={map.getMinZoom()}
            max={map.getMaxZoom()}
            value={zoom}
            onChange={handleZoomChange}
            onMouseDown={handleMouseDown}
            onMouseUp={handleMouseUp}
            onTouchStart={handleMouseDown}
            onTouchEnd={handleMouseUp}
            className="slider form-control-range"
          />
        </FormGroup>
      )}
    </div>
  );
}
