import {
  Modal, ModalHeader, ModalBody, ModalFooter,
} from "reactstrap";
import PropTypes from "prop-types";
import { getChildComponent, updateChildComponent } from "./mapUtil.js";
import "./MapToolBar.scss";

/** Usage Guide Body component to add text content */
export function UsageGuideBody({ children }) {
  return (
    <ModalBody className="usage-modal-body">
      {children}
    </ModalBody>
  );
}

UsageGuideBody.propTypes = {
  children: PropTypes.node.isRequired,
};

/** Usage Guide Footer component to add footer to the modal */
export function UsageGuideFooter({ children }) {
  return (
    <ModalFooter className="usage-modal-footer">
      {children}
    </ModalFooter>
  );
}

UsageGuideFooter.propTypes = {
  children: PropTypes.node.isRequired,
};

/** Usage Guide Header component for custom header */
export function UsageGuideHeader({ children, toggle }) {
  return (
    <ModalHeader className="usage-modal-header" toggle={toggle}>
      {children}
    </ModalHeader>
  );
}
UsageGuideHeader.defaultProps = {
  toggle: null,
};

UsageGuideHeader.propTypes = {
  children: PropTypes.node.isRequired,
  toggle: PropTypes.func,
};

export function UsageGuide({
  modalSize,
  guideOpen,
  toggle,
  children,
}) {
  // updates header with toggle props
  const header = getChildComponent(children, UsageGuideHeader);
  const updateHeader = header ? updateChildComponent(header, { toggle }) : null;

  const body = getChildComponent(children, UsageGuideBody);
  const footer = getChildComponent(children, UsageGuideFooter);

  return (
    <Modal isOpen={guideOpen} toggle={toggle} size={modalSize} className="usage-modal">
      {updateHeader || <UsageGuideHeader toggle={toggle}>Usage Guide</UsageGuideHeader>}
      {body}
      {footer}
    </Modal>
  );
}

UsageGuide.defaultProps = {
  modalSize: "lg",
  guideOpen: null,
  toggle: null,
};

UsageGuide.propTypes = {
  modalSize: PropTypes.string,
  guideOpen: PropTypes.bool,
  toggle: PropTypes.func,
  children: PropTypes.node.isRequired,
};
