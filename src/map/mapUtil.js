import L from "leaflet";
import { Children, cloneElement } from "react";

// helper function to return bounds for GeoJson
export const createBoundsGeoJson = (map) => {
  const mapBounds = map.getBounds();
  return {
    type: "Polygon", // geometry type
    coordinates: [[
      [mapBounds.getWest(), mapBounds.getNorth()], // top left
      [mapBounds.getEast(), mapBounds.getNorth()], // top right
      [mapBounds.getEast(), mapBounds.getSouth()], // bottom right
      [mapBounds.getWest(), mapBounds.getSouth()], // bottom left
      [mapBounds.getWest(), mapBounds.getNorth()], // top left (completes)
    ]],
  };
};

// helper function to inflate the current mapo
export const getInflatedBounds = (bounds, inflateByLat, inflateByLng) => {
  const southWest = bounds.getSouthWest();
  const northEast = bounds.getNorthEast();

  const inflatedSouthWest = L.latLng(southWest.lat - inflateByLat, southWest.lng - inflateByLng);
  const inflatedNorthEast = L.latLng(northEast.lat + inflateByLat, northEast.lng + inflateByLng);

  return L.latLngBounds(inflatedSouthWest, inflatedNorthEast);
};

// helper function to check if geojson is rectangle
export const isRectangle = (coordinates) => {
  // rectangle will have five points with the first and last point being identical
  const firstPoint = coordinates[0];
  const lastPoint = coordinates.pop();

  return coordinates.length === 5 && firstPoint === lastPoint;
};

// creates geojson object
export const createGeoJsonFromRectangle = (layer) => {
  const bounds = layer.getBounds();
  const { _southWest, _northEast } = bounds;
  // Calculate the dimensions of the rectangle
  const width = Math.abs(_northEast.lng - _southWest.lng);
  const height = Math.abs(_northEast.lat - _southWest.lat);

  // Set a minimum size threshold for the rectangle to be considered valid
  const MIN_SIZE_THRESHOLD = 0.0001; // Adjust the threshold as needed

  // Check if the rectangle has an area larger than the threshold
  if (width > MIN_SIZE_THRESHOLD && height > MIN_SIZE_THRESHOLD) {
    const coordinates = [
      [
        [_southWest.lng, _southWest.lat],
        [_northEast.lng, _southWest.lat],
        [_northEast.lng, _northEast.lat],
        [_southWest.lng, _northEast.lat],
        [_southWest.lng, _southWest.lat], // Closing the loop
      ],
    ];
    return {
      type: "Polygon",
      coordinates,
    };
  }
  // If the rectangle is too small
  return null;
};

// TODO: ESLint keeps turning function below into one line and then complain about max len
// eslint-disable-next-line max-len
export const getChildComponent = (child, component) => Children.toArray(child).find((childElement) => childElement.type && childElement.type === component);

// returns a cloned element with new props
export const updateChildComponent = (child, props) => {
  if (child) {
    return cloneElement(child, props);
  }

  return undefined;
};
