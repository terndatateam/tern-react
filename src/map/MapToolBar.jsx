import {
  useEffect, useState, useRef,
} from "react";
import L from "leaflet";
import PropTypes from "prop-types";
import "./MapToolBar.scss";
import {
  Button, ButtonGroup, Input, Card, UncontrolledTooltip,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faDrawPolygon,
  faExpand,
  faEye,
  faEyeSlash,
  faFloppyDisk,
  faMagnifyingGlass,
  faMapLocation,
  faMinus,
  faPenToSquare,
  faPencil,
  faPlus,
  faQuestion,
  faSpinner,
  faTrash,
  faXmark,
} from "@fortawesome/free-solid-svg-icons";
import { OpenStreetMapProvider } from "leaflet-geosearch";
import { DrawSquareIcon } from "../icons/DrawSquare.jsx";
import { updateChildComponent } from "./mapUtil.js";
import { MapZoomTools } from "./map-tools/MapZoomTools.jsx";
import "../icons/icons.scss";

/**
 *
 * Map Toolbar component.
 *
 * This component will act as toolbar for interacting with a map.
 *
 * The toolbar will automatically contain options to zoom in, zoom out, and reset zoom.
 *
 * MapSearch and DrawTools are rendered as children inside this component from the MapControl
 * to help with readability.
 *
 *
 * */

export function MapToolBar({
  zoomIn, zoomOut, resetZoom, currentShape, zoomToShapeBounds, zoomToolsOnly, size, children,
}) {
  const [collapsed, setCollapsed] = useState(false);
  const toggleCollapsed = () => setCollapsed(!collapsed);

  // onDoubleClickCapture prevents leaflet zoom in if users double click inside map tools
  if (zoomToolsOnly) {
    return (
      <div
        className={`map-tools-container toolbar-size-${size}`}
        onDoubleClickCapture={(event) => event.stopPropagation()}
      >
        <div className="additional-buttons zoom-only">
          <ButtonGroup vertical className="fab-btn-group">
            <MapZoomTools
              zoomIn={zoomIn}
              zoomOut={zoomOut}
              resetZoom={resetZoom}
            />
          </ButtonGroup>
        </div>
      </div>
    );
  }

  return (
    <div
      className={`map-tools-container toolbar-size-${size}`}
      onDoubleClickCapture={(event) => event.stopPropagation()}
    >
      <Button className="menu-button map-tools-toggle" onClick={toggleCollapsed}>
        {collapsed ? <FontAwesomeIcon icon={faEyeSlash} /> : <FontAwesomeIcon icon={faEye} />}
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-toggle"
          trigger="hover"
        >
          Toggle Menu
        </UncontrolledTooltip>
      </Button>

      {!collapsed && (
        <div className="additional-buttons">
          <ButtonGroup vertical className="fab-btn-group">
            <Button className="tool-btn" onClick={zoomIn}>
              <FontAwesomeIcon icon={faPlus} />
            </Button>
            <Button className="tool-btn" onClick={zoomOut}>
              <FontAwesomeIcon icon={faMinus} />
            </Button>
            <Button className="tool-btn map-tools-reset" onClick={resetZoom}>
              <FontAwesomeIcon icon={faExpand} />
              <UncontrolledTooltip
                placement="right"
                target=".map-tools-reset"
                trigger="hover"
              >
                Reset Zoom
              </UncontrolledTooltip>
            </Button>

            {currentShape && (
              <Button className="tool-btn map-tools-zoom-shape" onClick={zoomToShapeBounds}>
                <FontAwesomeIcon icon={faMagnifyingGlass} />
                <UncontrolledTooltip
                  placement="right"
                  target=".map-tools-zoom-shape"
                  trigger="hover"
                >
                  Zoom To Shape
                </UncontrolledTooltip>
              </Button>
            )}
          </ButtonGroup>
          {/** Renders the map search and draw tools */}
          {children}
        </div>
      )}
    </div>
  );
}

/**
 *
 *
 * Map Search Component
 *
 * This component is used for quickly search for a location on the map.
 *
 * This component takes in two props which is the focusLocation (location user is hovering on the map),
 * zoomToBounds (function passed into fly the map to the bounds of the result the user clicks on)
 *
 *
 */

export function MapSearch({ setFocusLocation, zoomToBounds }) {
  // general ui states
  const [collapsed, setCollapsed] = useState(true);
  const toggleCollapsed = () => setCollapsed(!collapsed);

  const divRef = useRef(null);

  const [query, setQuery] = useState("");

  // create states for queries and search results
  const [results, setResults] = useState([]);
  const [loading, setLoading] = useState(false);

  // converts the results into bounds object to use by leaflet
  const zoom = (resultBounds) => {
    const southWest = L.latLng(resultBounds[0][0], resultBounds[0][1]);
    const northEast = L.latLng(resultBounds[1][0], resultBounds[1][1]);

    const bounds = L.latLngBounds(southWest, northEast);

    zoomToBounds(bounds, 0.2);
  };

  // register clicks outside
  const toggleOutsideClick = (event) => {
    if ((divRef.current && !divRef.current.contains(event.target)) && !collapsed) {
      toggleCollapsed();
    }
  };

  // use effect to handle outside clicks to toggle off geosearch easily
  useEffect(() => {
    document.addEventListener("click", toggleOutsideClick);

    return () => {
      document.removeEventListener("click", toggleOutsideClick);
    };
  });

  // use effect to manage search results from open streetmap
  useEffect(() => {
    // stores initial state
    const initialQueryState = query;

    let timer;

    const search = async () => {
      if (query !== initialQueryState) {
        // query has changed restart race to allow users to finish typing
        timer = setTimeout(search, 1500);
        return;
      }
      // retrieve search results
      const provider = new OpenStreetMapProvider({
        params: {
          // Limit results to Australia
          countrycodes: "au",
        },
      });

      try {
        const searchResults = await provider.search({ query });
        setResults(searchResults);
      } catch {
        setResults([]);
      } finally {
        setLoading(false);
      }
    };

    if (query) {
      setLoading(true);
      // start the race condition
      timer = setTimeout(search, 1500);
    }

    return () => {
      clearTimeout(timer);
    };
  }, [query, collapsed]);

  const onQueryChange = (value) => {
    setQuery(value);
  };

  return (
    <div className="search-tool fab-btn-group" ref={divRef}>
      <Button className="tool-btn map-tools-geosearch" onClick={toggleCollapsed}>
        <FontAwesomeIcon icon={faMapLocation} />
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-geosearch"
          trigger="hover"
        >
          Search Location
        </UncontrolledTooltip>
      </Button>

      {!collapsed
        && (
        <div className="collapsible-search-field">
          <Input
            type="text"
            className="pill-input"
            onChange={(e) => onQueryChange(e.target.value)}
            defaultValue={query}
            placeholder="Search for location"
          />

          {query !== "" && (
          <Card className="results">

            {(!loading && query !== "" && results.length > 0)
            && results.map((result, index) => (
              <div key={result.label}>
                <button
                  className="result"
                  key={result.label}
                  type="button"
                  onMouseEnter={() => {
                    setFocusLocation({ label: result.label, lat: result.y, lon: result.x });
                  }}
                  onClick={
                () => zoom(result.bounds)
                  }
                >
                  <span>{result.label}</span>
                </button>
                {(index + 1) !== results.length && <hr /> }
              </div>
            ))}
            {loading && <div className="result"><FontAwesomeIcon icon={faSpinner} spin /></div>}
            {(!loading && results.length === 0 && query)
            && (
            <div className="result">
              <span>
                No results found for
                {`"${query}"`}
                . Please try refining your search.
              </span>
            </div>
            )}
          </Card>
          )}
        </div>
        )}
    </div>
  );
}

/**
 *
 * Draw tools component.
 *
 * This component will handle activating and toggling draw modes
 *
 * All drawing tool logic is defined in the DrawLayer component
 *
 *
 */

export function DrawTools({
  enableDraw, drawMode, toggleDrawing, currentShape, deleteCurrentShape, saveEdit, cancelEdit,
}) {
  // collapse draw tools by default
  const [collapsed, setCollapsed] = useState(true);
  const toggleCollapsed = () => {
    // toggles off the draw mode when collapsed
    if (!collapsed) {
      toggleDrawing(false);
    }

    setCollapsed(!collapsed);
  };

  return (
    <ButtonGroup vertical className="draw-tools fab-btn-group">
      <Button
        className={`tool-btn map-tools-draw ${collapsed ? "" : "active"}`}
        onClick={toggleCollapsed}
      >
        <FontAwesomeIcon icon={faPencil} />
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-draw"
          trigger="hover"
        >
          Toggle Draw Shape
        </UncontrolledTooltip>
      </Button>

      {!collapsed && (
        <>
          {!currentShape && enableDraw.includes("polygon") && (
            <Button
              className="draw-polygon tool-btn map-tools-draw-polygon"
              onClick={() => toggleDrawing("polygon")}
            >
              <FontAwesomeIcon icon={faDrawPolygon} />
              <UncontrolledTooltip
                placement="right"
                target=".map-tools-draw-polygon"
                trigger="hover"
              >
                Draw Polygon
              </UncontrolledTooltip>
            </Button>
          )}

          {!currentShape && enableDraw.includes("rectangle") && (
            <Button
              className="draw-rectangle tool-btn map-tools-draw-square"
              onClick={() => toggleDrawing("rectangle")}
            >
              <DrawSquareIcon className="draw-square-icon" />
              <UncontrolledTooltip
                placement="right"
                target=".map-tools-draw-square"
                trigger="hover"
              >
                Draw Square
              </UncontrolledTooltip>
            </Button>
          )}

          {currentShape && drawMode !== "edit" && (
            <Button className="map-tools-draw-edit tool-btn" id="" onClick={() => toggleDrawing("edit")}>
              <FontAwesomeIcon icon={faPenToSquare} />
              <UncontrolledTooltip
                placement="right"
                target=".map-tools-draw-edit"
                trigger="hover"
              >
                Edit Existing Shape
              </UncontrolledTooltip>
            </Button>
          )}

          {drawMode === "edit" && (
            <>
              <Button className="map-tools-draw-edit-save tool-btn" onClick={() => saveEdit()}>
                <FontAwesomeIcon icon={faFloppyDisk} />
                <UncontrolledTooltip
                  placement="right"
                  target=".map-tools-draw-edit-save"
                  trigger="hover"
                >
                  Save Edited Shape
                </UncontrolledTooltip>
              </Button>
              <Button className="map-tools-draw-edit-cancel tool-btn" onClick={() => cancelEdit()}>
                <FontAwesomeIcon icon={faXmark} />
                <UncontrolledTooltip
                  placement="right"
                  target=".map-tools-draw-edit-cancel"
                  trigger="hover"
                >
                  Cancel Edits
                </UncontrolledTooltip>
              </Button>
            </>
          )}

          {currentShape && (
            <Button className="map-tools-draw-edit-delete tool-btn" onClick={deleteCurrentShape}>
              <FontAwesomeIcon icon={faTrash} />
              <UncontrolledTooltip
                placement="right"
                target=".map-tools-draw-edit-delete"
                trigger="hover"
              >
                Delete Existing Shape
              </UncontrolledTooltip>
            </Button>
          )}
        </>
      )}
    </ButtonGroup>
  );
}

export function UsageGuideButton({
  content, toggle, guideOpen,
}) {
  const usageGuide = updateChildComponent(content, { guideOpen, toggle });

  return (
    <div className="usage-guide-tool fab-btn-group">
      <Button className="map-tools-usage tool-btn" id="" onClick={toggle}>
        <FontAwesomeIcon icon={faQuestion} />
        {usageGuide}
        <UncontrolledTooltip
          placement="right"
          target=".map-tools-usage"
          trigger="hover"
        >
          Open Usage Guide
        </UncontrolledTooltip>
      </Button>
    </div>
  );
}

DrawTools.defaultProps = {
  currentShape: null,
};

DrawTools.propTypes = {
  enableDraw: PropTypes.oneOfType([
    PropTypes.bool.isRequired,
    PropTypes.arrayOf(PropTypes.string),
  ]).isRequired,
  toggleDrawing: PropTypes.func.isRequired,
  currentShape: PropTypes.shape(),
  deleteCurrentShape: PropTypes.func.isRequired,
  saveEdit: PropTypes.func.isRequired,
  cancelEdit: PropTypes.func.isRequired,
  drawMode: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.bool,
  ]).isRequired,
};

MapSearch.propTypes = {
  zoomToBounds: PropTypes.func.isRequired,
  setFocusLocation: PropTypes.func.isRequired,
};

MapToolBar.defaultProps = {
  children: null,
  currentShape: null,
};

MapToolBar.propTypes = {
  zoomIn: PropTypes.func.isRequired,
  zoomOut: PropTypes.func.isRequired,
  size: PropTypes.string.isRequired,
  resetZoom: PropTypes.func.isRequired,
  currentShape: PropTypes.shape(),
  zoomToShapeBounds: PropTypes.func.isRequired,
  zoomToolsOnly: PropTypes.bool.isRequired,
  children: PropTypes.node,
};

UsageGuideButton.propTypes = {
  content: PropTypes.node.isRequired,
  toggle: PropTypes.func.isRequired,
  guideOpen: PropTypes.bool.isRequired,
};
