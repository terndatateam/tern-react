import colors from "../scss/colors.module.scss";

export const defaultConfig = {
  GEOSERVER_URL: "https://geoserver.tern.org.au/geoserver/wms",
  GEOSERVER_LAYER: "nr:regions",
  WMS_STYLES: "polygon",
  WMS_FORMAT: "image/png",
  WMS_SRS: "EPSG:900913",
  WMS_COLORS: colors.orangeDark,
  WMS_OPACITY: 0.5,
  WMS_VERSION: "1.1.1",

  // set the default map bounds in geo json
  DEFAULT_MAP_BOUNDS: {
    type: "Polygon",
    coordinates: [
      [
        [
          89.5361229497511,
          -7.848911442125588,
        ],
        [
          178.0971040895702,
          -7.848911442125588,
        ],
        [
          178.0971040895702,
          -45.92109443959872,
        ],
        [
          89.5361229497511,
          -45.92109443959872,
        ],
        [
          89.5361229497511,
          -7.848911442125588,
        ],
      ],
    ],
  },

  ENABLE_DRAWING: ["polygon", "rectangle"],
  ENABLE_GEOSEARCH: true,
  ENABLE_MAP_LAYERS: true,

  MAP_INIT_STATE: {
    lat: -26.47,
    lng: 134.02,
    zoomSnap: 0.5,
    maxZoom: 30,
    minZoom: 4.5,
    zoom: 4,
  },
  MAP_POLYGON_FILL_OPACITY: 0.2,
  MAP_POLYGON_COLOR: colors.orangeDark,
  MAP_POLYGON_FILL_COLOR: "",
  MAP_POLYGON_SMOOTH_FACTOR: 1,
  MAP_POLYGON_WEIGHT: 1,

};
