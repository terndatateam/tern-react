import { Polygon } from "react-leaflet";
import PropTypes from "prop-types";
import { defaultConfig } from "../defaultConfig.js";
import "./MarkerStyles.scss";

// define shape styling from config
const defaultShapeStyling = {
  fillOpacity: defaultConfig.MAP_POLYGON_FILL_OPACITY,
  fillColor: defaultConfig.MAP_POLYGON_FILL_COLOR,
  color: defaultConfig.MAP_POLYGON_COLOR,
  smoothFactor: defaultConfig.MAP_POLYGON_SMOOTH_FACTOR,
  weight: defaultConfig.MAP_POLYGON_WEIGHT,
  lineJoin: "round",
};

/**
 *
 * Simple component to expose the polygon component from leaflet
 *
 * Additional shape styling can be added as a prop and will merge with default config
 */
function PolygonLayer({ positions, shapeStyling }) {
  const styling = { ...defaultShapeStyling, shapeStyling };
  return (
    <Polygon positions={positions} {...styling} />
  );
}

// To differtiate between Leaflet polygon and tern-react polygon
export { PolygonLayer as Polygon };

PolygonLayer.defaultProps = {
  shapeStyling: {},
};

PolygonLayer.propTypes = {
  // need to take both polygon and multipolygon into account here
  positions: PropTypes.oneOfType([
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number)),
    PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.arrayOf(PropTypes.number))),
  ]).isRequired,
  shapeStyling: PropTypes.shape(),
};
