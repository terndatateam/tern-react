import { Marker } from "react-leaflet";
import PropTypes from "prop-types";
import L from "leaflet";
import { renderToStaticMarkup } from "react-dom/server";
import { ClusterIcon } from "../../icons/icons.jsx";
import "./MarkerStyles.scss";

/**
 *
 * Cluster Marker used to signal a cluster of data points
 *
 * Basic icon and colour selection including green, red, orange, and yellow
 *
 *
 */
export function ClusterMarker({
  number, color, location, options, children, events,
}) {
  // available cluster colours - TODO: Allow for custom set of colours?
  const COLORS = ["green", "yellow", "red", "orange"];

  // return null and send console error if no id exists
  if (!location || !(location.lon || location.lat)) {
    return null;
  }

  const { iconSize } = options;

  const clusterIcon = L.divIcon({
    className: `cluster-marker ${COLORS.includes(color) ? color : "green"}-cluster`,
    iconSize: L.point(iconSize, iconSize, true),
    html: renderToStaticMarkup(ClusterIcon(number, color)),
  });

  return (
    <Marker
      icon={clusterIcon}
      position={location}
      eventHandlers={events}
    >
      {children}
    </Marker>
  );
}

ClusterMarker.defaultProps = {
  color: "green",
  options: {
    iconSize: 33,
  },
  children: null,
  events: {},
};

ClusterMarker.propTypes = {
  number: PropTypes.number.isRequired,
  color: PropTypes.string,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }).isRequired,
  options: PropTypes.shape({
    iconSize: PropTypes.number,
  }),
  children: PropTypes.node,
  events: PropTypes.shape(),
};
