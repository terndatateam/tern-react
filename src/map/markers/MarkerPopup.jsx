import { Popup } from "react-leaflet";
import PropTypes from "prop-types";
import "./MarkerStyles.scss";

/**
 *
 * Simple component to expose the popup component from leaflet
 */
export function MarkerPopup({
  children, position, toggleClose, options,
}) {
  return (
    <Popup className="popup" position={position} closeButton={toggleClose} options={options}>
      {children}
    </Popup>
  );
}

MarkerPopup.defaultProps = {
  toggleClose: true,
  position: null,
  options: {},

};

MarkerPopup.propTypes = {
  children: PropTypes.node.isRequired,
  position: PropTypes.shape(),
  toggleClose: PropTypes.bool,
  options: PropTypes.shape(),
};
