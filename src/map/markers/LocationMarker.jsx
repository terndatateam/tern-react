import { Marker } from "react-leaflet";
import L from "leaflet";
import PropTypes from "prop-types";
import { renderToStaticMarkup } from "react-dom/server";
import "./MarkerStyles.scss";

/**
 *
 *
 * Simple location marker to use inisde leaflet map.
 *
 * Required props include an id, and location to place the marker on the map
 *
 * Icon is an optional props to render a custom React/Html element as an icon
 * instead of the default.
 *
 * If locations are not provided to the location marker, it can be used to return just the divIcon.
 * This can be used for a custom cluster marker group icon or expose divIcon to use in another way.
 */
export function LocationMarker({
  customIconClass, location, icon, children, options, events,
}) {
  const { iconSize, iconAnchor } = options;

  const markerIcon = L.divIcon({
    className: `default-marker ${customIconClass || ""}`,
    iconSize: L.point(iconSize, iconSize, true),
    iconAnchor,
    html: icon ? renderToStaticMarkup(icon) : "",
  });

  // if locations are null, simply return the divIcon marker by itself (can be used in cluster marker group)
  if (!location || !Number.isFinite(location.lon) || !Number.isFinite(location.lat)) {
    return markerIcon;
  }

  return (
    <Marker
      icon={markerIcon}
      position={location}
      eventHandlers={events}
    >
      {children}
    </Marker>
  );
}

LocationMarker.defaultProps = {
  customIconClass: "",
  icon: undefined,
  options: {
    iconSize: 20,
    iconAnchor: null,
  },
  location: null,
  children: null,
  events: {},
};

LocationMarker.propTypes = {
  customIconClass: PropTypes.string,
  icon: PropTypes.node,
  location: PropTypes.shape({
    lat: PropTypes.number,
    lon: PropTypes.number,
  }),
  options: PropTypes.shape({
    iconSize: PropTypes.number,
    iconAnchor: PropTypes.arrayOf(PropTypes.number),
  }),
  children: PropTypes.node,
  events: PropTypes.shape(),
};
