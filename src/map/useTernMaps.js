import { useState } from "react";
import { defaultConfig } from "./defaultConfig.js";

/**
 *
 * useTernMap Hook Component
 *
 * This is a useMap hook that exposes map configuration values and allows devs
 * to pass in custom configurations that will be merged with the default map
 * configuration.
 *
 * This hook is also used for retrieveing the current map state such as zoom and bounds
 *
 * TODO: ensure that set the states outside of the component sets the map accordingly.
 *
 */

export function useTernMap(customConfig = {}) {
  // merge two configs, custom config should overwrite default
  const config = { ...defaultConfig, ...customConfig };
  const mapInitState = config.MAP_INIT_STATE;

  const [zoom, setZoom] = useState(mapInitState.zoom);

  const defaultBounds = config.DEFAULT_MAP_BOUNDS;

  const [bounds, setBounds] = useState(defaultBounds);
  const enableDraw = config.ENABLE_DRAWING;
  const searchable = config.ENABLE_GEOSEARCH;
  // this state must change to reflect the shape the user is drawing with.
  // false - draw mode disabled, saquare - drawing a square, polygon - drawing a polygon etc
  const [drawMode, toggleDrawing] = useState(false);
  // this state represents the shape a user has drawn in the map
  const [currentShape, setCurrentShape] = useState(null);

  // define shape styling from config
  const shapeStyling = {
    fillOpacity: config.MAP_POLYGON_FILL_OPACITY,
    fillColor: config.MAP_POLYGON_FILL_COLOR,
    color: config.MAP_POLYGON_COLOR,
    smoothFactor: config.MAP_POLYGON_SMOOTH_FACTOR,
    weight: config.MAP_POLYGON_WEIGHT,
    lineJoin: "round",
  };

  return {
    config,
    mapInitState,

    zoom,
    setZoom,

    bounds,
    setBounds,

    enableDraw,
    drawMode,
    toggleDrawing,

    currentShape,
    setCurrentShape,
    shapeStyling,

    searchable,
  };
}
