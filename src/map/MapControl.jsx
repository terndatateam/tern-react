import { useMapEvents } from "react-leaflet";
import L, { divIcon } from "leaflet";
import PropTypes from "prop-types";
import { useEffect, useRef, useState } from "react";
import { renderToStaticMarkup } from "react-dom/server";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faLocationPin } from "@fortawesome/free-solid-svg-icons";
import {
  MapToolBar, DrawTools, MapSearch, UsageGuideButton,
} from "./MapToolBar.jsx";
import { createBoundsGeoJson, getInflatedBounds } from "./mapUtil.js";
import "./MapControl.scss";
import colors from "../scss/colors.module.scss";

/**
 *
 * This MapControl component is developed to help seperate interactive UI elements with the map
 *
 * This component will handle all of the events related to the tools and search
 *
 */

export function MapControl({
  ternMap, usageGuide, size, onDeleteShape, zoomToolsOnly,
}) {
  const {
    setZoom,
    bounds,
    setBounds,
    drawMode,
    enableDraw,
    config,
    toggleDrawing,
    currentShape,
    setCurrentShape,
    searchable,
  } = ternMap;

  // state for map search
  // null or { label, lat, lon}
  const [focusLocation, setFocusLocation] = useState(null);

  const [usageGuideOpen, setUsageGuide] = useState(false);
  const toggleGuide = () => setUsageGuide(!usageGuideOpen);

  const configBounds = config.DEFAULT_MAP_BOUNDS;
  const initialBounds = L.geoJson(configBounds).getBounds();

  const boundsRef = useRef(null);
  const boundsTriggeredExternally = useRef(false);

  // initilise leaflet map and mapEvents instance
  const map = useMapEvents({
    // updates the current bounds in hook once movement stops
    moveend: () => {
      if (!boundsTriggeredExternally.current) {
        // bounds are changed internally
        const newBounds = createBoundsGeoJson(map);
        setBounds(newBounds);
      }
    },
    // updates the zoom levels in hook
    zoom: () => {
      setZoom(map.getZoom());
    },
  });

  // useEffect to manage bounds that are updated externally through setBounds hook
  useEffect(() => {
    if (!boundsRef.current) {
      boundsRef.current = bounds;
    } else if (boundsRef.current !== bounds) {
      // flag to prevent infinite renders from moveend event
      boundsTriggeredExternally.current = true;

      const externallyChangedBounds = L.geoJSON(bounds).getBounds();
      map.flyToBounds(externallyChangedBounds);
    }
  }, [bounds, map]);

  // zoom control functions to update map and hook
  const zoomIn = () => { map.zoomIn(1); };
  const zoomOut = () => { map.zoomOut(1); };
  const resetZoom = () => (initialBounds && map.flyToBounds(initialBounds));

  // generalised zoom to bounds with optional scale param
  const zoomToBounds = (zoomBounds, scale = 2) => {
    // inflates the bounds for a better view.
    const inflatedBounds = getInflatedBounds(zoomBounds, scale, scale);
    map.flyToBounds(inflatedBounds);
  };

  // zoom to bounds for current shapes passed in, bounds in hook
  // is updated in the map events as well
  const zoomToShapeBounds = () => {
    const shapeBounds = L.geoJSON(currentShape).getBounds();
    zoomToBounds(shapeBounds);
  };

  // Draw tools functions
  // either false, square, polygon TODO: refactor
  const deleteCurrentShape = () => {
    onDeleteShape();
    setCurrentShape(null);
  };

  // edit function that fires edited map function recieved from draw layer
  const saveEdit = () => {
    map.fire(L.Draw.Event.EDITED);
  };

  const cancelEdit = () => {
    map.fire("cancelEdit");
  };

  // use effect to focus location in geosearch
  useEffect(() => {
    let locationMarker;

    if (focusLocation) {
      // create pin icon
      const pin = <div><FontAwesomeIcon size="xl" style={{ color: colors.teal }} icon={faLocationPin} /></div>;
      const locationPinIcon = divIcon({
        className: "custom-icon",
        html: renderToStaticMarkup(pin),
      });

      locationMarker = L.marker([focusLocation.lat, focusLocation.lon], {
        icon: locationPinIcon,
      });

      // adds the marker to map
      locationMarker.addTo(map);
    }

    // clean up function to remove the marker
    return () => {
      if (locationMarker) locationMarker.remove();
    };
  }, [focusLocation, map]);

  return (
    <MapToolBar
      zoomIn={zoomIn}
      size={size}
      zoomOut={zoomOut}
      resetZoom={resetZoom}
      currentShape={currentShape}
      zoomToShapeBounds={zoomToShapeBounds}
      zoomToolsOnly={zoomToolsOnly}
    >
      {/** Draw shapes functionality */}
      {enableDraw && (
      <DrawTools
        enableDraw={enableDraw}
        drawMode={drawMode}
        size={size}
        toggleDrawing={toggleDrawing}
        currentShape={currentShape}
        deleteCurrentShape={deleteCurrentShape}
        zoomToShapeBounds={zoomToShapeBounds}
        saveEdit={saveEdit}
        cancelEdit={cancelEdit}
      />
      )}

      {/** Map search functionality */}
      {searchable && (
      <MapSearch
        setFocusLocation={setFocusLocation}
        size={size}
        zoomToBounds={zoomToBounds}
      />
      )}

      {usageGuide
        && (
        <UsageGuideButton
          toggle={toggleGuide}
          size={size}
          guideOpen={usageGuideOpen}
          content={usageGuide}
        />
        )}
    </MapToolBar>
  );
}

MapControl.defaultProps = {
  usageGuide: null,
};

MapControl.propTypes = {
  ternMap: PropTypes.shape().isRequired,
  size: PropTypes.string.isRequired,
  onDeleteShape: PropTypes.func.isRequired,
  zoomToolsOnly: PropTypes.bool.isRequired,
  usageGuide: PropTypes.shape({
    header: PropTypes.func,
    body: PropTypes.func,
    footer: PropTypes.func,
    modalSize: PropTypes.string,
  }),
};
