import { useMapEvents } from "react-leaflet";
import L, { divIcon } from "leaflet";
import "leaflet-draw/dist/leaflet.draw.css";
import PropTypes from "prop-types";
import { useEffect, useRef } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapPin } from "@fortawesome/free-solid-svg-icons";
import { renderToStaticMarkup } from "react-dom/server";
import { getInflatedBounds, isRectangle, createGeoJsonFromRectangle } from "./mapUtil.js";
import "./DrawLayer.scss";

/**
 *
 * DrawLayer component
 *
 * This handles all the drawing logic which listens for updates from the toolbar.
 *
 * This utlises the useMapEvents function from react-leaflet, this reduces useEffectsin the
 * component code
 *
 *
 */
export function DrawLayer({
  drawMode,
  toggleDrawing,
  currentShape,
  setCurrentShape,
  pushNotification,
  onCreateShape,
  shapeStyling,
}) {
  const drawHandler = useRef(null);

  // const [editMode, setIsEditMode] = useState(false);
  const drawnItems = useRef(new L.FeatureGroup()).current;
  const originalDrawn = useRef(new L.FeatureGroup()).current;

  const pinIcon = <FontAwesomeIcon size="lg" icon={faMapPin} />;

  // disable mouse prompt on edit
  L.drawLocal.edit.handlers.edit.tooltip.subtext = "";
  L.drawLocal.edit.handlers.edit.tooltip.text = "";

  // create custom vertex marker
  const vertexMarker = divIcon({
    html: renderToStaticMarkup(pinIcon),
    className: "vertex-marker-icon",
  });

  // handles all map events here
  const map = useMapEvents({
    // event that listens when new vertecies are placed when drawing
    [L.Draw.Event.DRAWVERTEX]: (event) => {
      const { layers } = event;
      const vertice = layers.getLayers().pop();
      vertice.setIcon(vertexMarker);
    },
    // edit events here
    [L.Draw.Event.EDITSTART]: () => {
      // custom event to draw a grey original layer for reference
      drawnItems.eachLayer((layer) => {
        originalDrawn.addLayer(L.polygon(layer.getLatLngs(), { ...shapeStyling, color: "#808080" }));
        originalDrawn.addTo(map);
      });
    },
    [L.Draw.Event.EDITED]: () => {
      toggleDrawing(false);

      originalDrawn.clearLayers();

      drawnItems.eachLayer((layer) => {
        const geoJson = layer.toGeoJSON().geometry;
        if (geoJson) {
          const inflatedBounds = getInflatedBounds(layer.getBounds(), 2, 2);
          map.fitBounds(inflatedBounds, { animate: true });
          setCurrentShape(geoJson);
          onCreateShape(geoJson);

          pushNotification("Edited Shape", "Shape has been updated", "success");
        } else {
          pushNotification("Failed", "The new shape has failed to update.", "success");
        }
      });
    },
    // custom event for cancelling edits
    cancelEdit: () => {
      originalDrawn.clearLayers();

      // drawnItems.current.editing.disable(); // Disable edit mode
      drawHandler.current.revertLayers();
      toggleDrawing(false);
    },
    // used when saving editing or cancelling edits
    [L.Draw.Event.EDITSTOP]: () => {
      toggleDrawing(false);
    },
    // runs once a shape has been completed/created
    [L.Draw.Event.CREATED]: (event) => {
      const { layerType, layer } = event;

      // Clear existing layers
      toggleDrawing(false);
      drawnItems.clearLayers();
      drawnItems.addLayer(layer);

      let geoJson;

      if (layerType === "rectangle") {
        geoJson = createGeoJsonFromRectangle(layer);

        if (!geoJson) {
          pushNotification(
            "Failed to create shape.",
            "The shape was too small to create. Please try again.",
            "error",
          );
          drawnItems.clearLayers(); // Clear the point layer
        }
        const inflatedBounds = getInflatedBounds(layer.getBounds(), 2, 2);
        map.fitBounds(inflatedBounds, { animate: true });
      } else if (layerType === "polygon") {
        geoJson = layer.toGeoJSON().geometry;
        // zoom in to focus layer
        const inflatedBounds = getInflatedBounds(layer.getBounds(), 2, 2);
        map.fitBounds(inflatedBounds, { animate: true });
      } else {
        pushNotification(
          "Shape not supported.",
          `Created layer is not a supported shape: ${layerType}`,
          "error",
        );
        return;
      }

      // passes the shape outside of the component
      onCreateShape(geoJson);
      setCurrentShape(geoJson);
    },
  });

  useEffect(() => {
    // function to trigger draw tool on the layer
    const activateTool = (tool) => {
      // Disable any active drawing tool or edit handler
      if (drawHandler.current) {
        drawHandler.current.disable();
        drawHandler.current = null;
      }

      if (tool === "edit") {
        if (drawnItems.getLayers().length > 0) {
          drawHandler.current = new L.EditToolbar.Edit(map, {
            featureGroup: drawnItems,
            selectedPathOptions: {
              // Styles for the editable paths
            },
          });
          drawHandler.current.enable();
          pushNotification("Edit Mode", "Edit by adjusting the vertices of your drawn shape.");
        }
      } else {
        // setIsEditMode(false);
        drawnItems.clearLayers(); // Clear any existing shapes

        if (tool === "rectangle") {
          drawHandler.current = new L.Draw.Rectangle(map, { shapeOptions: shapeStyling });
        } else if (tool === "polygon") {
          drawHandler.current = new L.Draw.Polygon(map, { shapeOptions: shapeStyling });
        }

        if (drawHandler.current) {
          drawHandler.current.enable();
        }
      }
    };

    if (drawMode) {
      activateTool(drawMode);
    } else {
      // disables any draw handlers that may still be enabled
      if (drawHandler.current) drawHandler.current.disable();
      drawHandler.current = null;
    }
    // TODO: refactor this to remove useEffect
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [drawMode]);

  useEffect(() => {
    const drawLayer = (geoJson) => {
      const coordinates = geoJson.coordinates[0];
      let layer;

      if (isRectangle(coordinates)) {
        // Draw rectangle
        const bounds = [
          [coordinates[0][1], coordinates[0][0]],
          [coordinates[2][1], coordinates[2][0]],
        ];
        layer = L.rectangle(bounds, shapeStyling);
      } else {
        // Draw polygon
        const latLngs = coordinates.map((coord) => [coord[1], coord[0]]);
        layer = L.polygon(latLngs, shapeStyling);
      }

      drawnItems.clearLayers(); // Clear existing layers
      drawnItems.addLayer(layer); // Add the new layer
      map.addLayer(drawnItems); // Add feature group to map
      map.fitBounds(getInflatedBounds(layer.getBounds(), 2, 2), { animate: true }); // Zoom to the layer bounds
    };

    if (currentShape) {
      drawLayer(currentShape);
    } else {
      drawnItems.clearLayers();
    }
    // TODO: refactor this to remove useEffect
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentShape, drawnItems]);

  return (null);
}
DrawLayer.defaultProps = {
  currentShape: null,
};

DrawLayer.propTypes = {
  drawMode: PropTypes.oneOfType([PropTypes.bool, PropTypes.string]).isRequired,
  toggleDrawing: PropTypes.func.isRequired,
  currentShape: PropTypes.shape(),
  setCurrentShape: PropTypes.func.isRequired,
  shapeStyling: PropTypes.shape().isRequired,
  pushNotification: PropTypes.func.isRequired,
  onCreateShape: PropTypes.func.isRequired,
};
