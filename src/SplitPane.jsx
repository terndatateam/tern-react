import { useState, useRef, useEffect } from "react";
import { DraggableCore } from "react-draggable";
import PropTypes from "prop-types";
import { Button } from "reactstrap";
import { faChevronRight, faChevronLeft } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import "./SplitPane.scss";

export function SplitPane({
  children, initialSize, isCollapsed, onChange,
  ...props
}) {
  // min panel sizes (ensure they are 0 or greater)
  const minSize = Math.max(props.minSize || 0, 0);
  // max panel sizes (ensure they are Infinity or smaller)
  const maxSize = Math.min(props.maxSize || Infinity, Infinity);
  // store current state when dragging starts
  const [dragStart, setDragStart] = useState(null);
  // store current width as state, so that it can be restored on collapes
  const [size, setSize] = useState(initialSize);
  // store currnt collapse state
  const [collapsed, setCollapsed] = useState(isCollapsed);

  // kepp a reference to left and right panel dom nodes
  const leftRef = useRef();
  const rightRef = useRef();

  const icon = collapsed ? faChevronRight : faChevronLeft;

  const onDragStart = (e, data) => {
    // remember where user started dragging (leftWidth and mouse / touch start point)
    setDragStart({
      dragX: data.x,
      leftWidth: leftRef.current.getBoundingClientRect().width,
    });
  };

  const saveState = () => {
    if (onChange) {
      onChange({ isCollapsed: collapsed, initialSize: size });
    }
  };

  const setWidth = (width) => {
    // update state
    setSize(width);
    // update dom
    leftRef.current.style["flex-basis"] = `${width}px`;
  };

  const onDragStop = () => {
    // notify parent about new state
    saveState();
  };

  const onDragMove = (e, data) => {
    // e ... mouse
    // data ... drag data
    // 1. how far did we move since start ?
    //    if negative we moved to the left.
    const moved = data.x - dragStart.dragX;
    // 2. new width is orig width + moved
    const newLeftWidth = Math.min(
      // use minsize if new width is smaller
      Math.max(dragStart.leftWidth + moved, minSize),
      // use maxsize if new width is greater
      maxSize,
    );
    // 3. resize left pane
    setWidth(newLeftWidth);
  };

  const onCollapse = () => {
    setCollapsed(!collapsed);
    if (!collapsed) {
      // store current size
      setSize(leftRef.current.getBoundingClientRect().width);
      leftRef.current.style["flex-basis"] = "0px";
    } else {
      // restore current size
      leftRef.current.style["flex-basis"] = `${size}px`;
    }
    // notify parent
    saveState();
  };

  // FIXME: ... as it is a functional component, this updates state on 2nd render.
  //            meaning, there is a flicker at the start. Either convert to class component,
  //            or have bette logic to set initial state from props.
  useEffect(() => {
    // set initial size on first render
    if (collapsed) {
      leftRef.current.style["flex-basis"] = "0px";
    } else if (size !== null && size !== undefined && size >= 0) {
      setWidth(size);
    }
  // FIXME: the way it currently works this effect should only run on initial mount.
  //        logic above needs to be reworkd to make linter happy
  // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  const hitboxClass = [
    "gutter-hitbox", collapsed ? "disabled" : "",
  ].join(" ");

  return (
    <div className="split-pane">
      <div className="left-pane" ref={leftRef}>
        {children[0]}
      </div>
      <DraggableCore
        cancel=".btn-collapse"
        disabled={collapsed}
        onStart={onDragStart}
        onDrag={onDragMove}
        onStop={onDragStop}
      >
        <div className="gutter">
          <Button color="collapse" onClick={onCollapse}>
            <FontAwesomeIcon icon={icon} />
          </Button>
          <div className={hitboxClass} />
        </div>
      </DraggableCore>
      <div className="right-pane" ref={rightRef}>
        {children[1]}
      </div>
    </div>
  );
}

SplitPane.propTypes = {
  children: PropTypes.arrayOf(PropTypes.node).isRequired,
  // size of left pane on first render (ignored when not set or null)
  initialSize: PropTypes.number,
  // min sizes in pixel
  minSize: PropTypes.number,
  // max sizes is pikel
  maxSize: PropTypes.number,
  // whether left pane should be collapsed on first render
  isCollapsed: PropTypes.bool,
  // called when state changes ... enables storing and restoring state
  onChange: PropTypes.func,
};

SplitPane.defaultProps = {
  initialSize: null,
  minSize: 0,
  maxSize: Infinity,
  isCollapsed: false,
  onChange: null,
};
