import {
  Spinner, Toast, ToastBody, ToastHeader,
} from "reactstrap";
import { useRef } from "react";
import PropTypes from "prop-types";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCircleXmark, faCircleCheck, faCircleInfo } from "@fortawesome/free-solid-svg-icons";
import colors from "../scss/colors.module.scss";
import "./Notifications.scss";
/**
 *
 * Notifcations Component
 *
 * Handles rendering a list of notifcaitons with styling to position at
 * the bottom right corner of the screen.
 *
 *
 * New notifications can be pushed using the pushNotifications() function retrieve from initialising
 * the hook useNotifications.
 *
 * useNotifcations hook is a required prop as it's the only way to actually interact with
 * and display notifications
 *
 *  * To use this component just do:
 *
 * `import { useNotification, Notifications } from "tern-react";`
 *
 * Initialise hook:
 *
 * `const notificationsHook = useNotifications;
 * const { pushNotifications } = notifications;
 * pushNotifications("Title", "message", "success");
 * `
 *
 * Render `<Notifications useNotfications={notficationsHook} />` in your component to use it.
 *
 */
export function Notifications({
  isDismissable, timer, duration, notifications, closeNotification,
}) {
  return (
    <div className="notifications-overlay">
      {
        notifications.map((notification) => (
          <Notification
            closeNotification={closeNotification}
            id={notification.id}
            isDismissable={isDismissable}
            timer={timer}
            duration={duration}
            title={notification.title}
            message={notification.message}
            status={notification.status}
          />
        ))
      }
    </div>
  );
}

/**
 *
 * Private notification  component used to render individual notifications.
 *
 * Notification look depends on the status prop passed in which
 * is generalised to a success, error, laoding component, or info component (default case )
 */

function Notification(props) {
  const {
    id, title, message, status, closeNotification, timer, duration, isDismissable,
  } = props;

  const timeoutRef = useRef(null);

  // timeout function
  const onTimeout = () => {
    closeNotification(id);
  };

  // begin timeout if enabled
  if (timer) {
    timeoutRef.current = setTimeout(onTimeout, duration);
  }

  // toggle function to remove notification
  const dismiss = () => {
    if (timeoutRef.current) {
      clearTimeout(timeoutRef.current);
    }
    closeNotification(id);
  };

  let icon;

  switch (status) {
    case "success":
      icon = (
        <FontAwesomeIcon
          size="sm"
          style={{ color: colors.greenMid, borderColor: colors.greenMid }}
          icon={faCircleCheck}
        />
      );
      break;
    case "error":
      icon = (
        <FontAwesomeIcon
          size="sm"
          style={{ color: colors.orangeDark, borderColor: colors.orangeDark }}
          icon={faCircleXmark}
        />
      );
      break;
    case "loading":
      icon = <Spinner size="sm">Loading...</Spinner>;
      break;
    default:
      icon = (
        <FontAwesomeIcon
          size="sm"
          style={{ color: colors.blueMid, borderColor: colors.blueMid }}
          icon={faCircleInfo}
        />
      );
      break;
  }

  return (
    <Toast key={id}>
      <ToastHeader icon={icon} toggle={isDismissable ? dismiss : null}>
        {title}
      </ToastHeader>
      <ToastBody>
        {message}
      </ToastBody>
    </Toast>
  );
}

// notifications wrapper props
Notifications.propTypes = {
  notifications: PropTypes.arrayOf(PropTypes.shape()),
  closeNotification: PropTypes.func,
  isDismissable: PropTypes.bool,
  duration: PropTypes.number,
  timer: PropTypes.bool,
};

Notifications.defaultProps = {
  isDismissable: false,
  notifications: [],
  closeNotification: () => null,
  timer: true,
  duration: 5000,
};

// notification prop
Notification.defaultProps = {
  status: "info",
};

Notification.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  message: PropTypes.string.isRequired,
  timer: PropTypes.bool.isRequired,
  duration: PropTypes.number.isRequired,
  isDismissable: PropTypes.bool.isRequired,
  status: PropTypes.string,
  closeNotification: PropTypes.func.isRequired,
};
