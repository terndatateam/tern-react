import { v4 as generateId } from "uuid";

/**
 *
 * Reusable functinon to help with external state management for notifications
 *
 * Generates a notification object with its own ID
 */
export const createNotification = (title, message, status = "info") => {
  const id = generateId();
  const notification = {
    id,
    title,
    message,
    status,
  };
  return notification;
};
