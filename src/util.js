import { Children } from "react";

const findChildElement = (component, children) => {
  const elements = Children.toArray(children).filter((child) => child.type && child.type === component);
  return elements;
};

const filterChildElement = (component, children) => {
  const elements = Children.toArray(children).filter((child) => child.type && child.type !== component);
  return elements;
};

// Simple function for title casing a string, useful for some scientific formatting
const toTitleCase = (str) => {
  const lowerWords = [
    "a", "an", "and", "as", "at", "but", "by", "for", "in", "nor", "of", "on",
    "or", "so", "the", "to", "up", "yet",
  ];

  return str
    .toLowerCase()
    .split(" ")
    .map((word, index) => {
      if (index === 0 || index === str.split(" ").length - 1 || !lowerWords.includes(word)) {
        return word.charAt(0).toUpperCase() + word.slice(1);
      }
      return word;
    })
    .join(" ");
};

const getStringCasing = (string, format) => {
  const casingFormatters = {
    upper: (label) => label.toUpperCase(),
    lower: (label) => label.toLowerCase(),
    title: (label) => toTitleCase(label),
  };

  const casingFormatter = casingFormatters[format] || ((newLabel) => newLabel); // Fallback to no casing if not provided

  return casingFormatter(string);
};

/**
 * Generate an array of numbers within the specified range.
 *
 * @param {number} min - The minimum number in the range.
 * @param {number} max - The maximum number in the range.
 * @returns {number[]} - An array containing all numbers between `min` and `max`.
 */

const range = (minYear, maxYear) => {
  const years = [];
  for (let i = minYear; i <= maxYear; i += 1) {
    years.push(i);
  }
  return years;
};

export {
  findChildElement, filterChildElement, getStringCasing, toTitleCase, range,
};
