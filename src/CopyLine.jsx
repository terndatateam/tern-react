import { useState, useRef } from "react";
import PropTypes from "prop-types";
import { Button, Badge, UncontrolledTooltip } from "reactstrap";
import colors from "./scss/colors.module.scss";
import "./CopyLine.scss";

function TooltipContent({ scheduleUpdate, children }) {
  scheduleUpdate();
  return children;
}
TooltipContent.propTypes = {
  scheduleUpdate: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
};

/**
 * This component is a code line with a badge and a copy button
 * where the badge and the button can be customized.
 * It supports both with and without a badge.
 *
 * @param {Object} props
 * @param {string} props.badgeText
 * @param {string} props.badgeColor
 * @param {boolean} props.allowCopy
 * @param {string} props.buttonColor
 * @param {React.ReactNode} props.children
 * @returns {React.ReactNode}
 */
export function CopyLine({
  allowCopy,
  buttonColor,
  badgeColor,
  badgeText,
  children,
}) {
  // ref to be used as tooltip target
  const containerRef = useRef(null);
  // timer reference to avoid running multiple timers
  const timerRef = useRef(0);
  // tooltip text to be changed via timer
  const [tooltipText, setTooltipText] = useState("Click the Copy button to copy the content.");

  // check if clipboard is available
  const clipboardAvailable = navigator.clipboard != null;
  // make copy button and tooltip available only if we can copy at all
  const copyButtonEnabled = allowCopy && clipboardAvailable;

  const onTimeout = () => {
    setTooltipText("Click the Copy button to copy the content");
    timerRef.current = 0;
  };

  const copyString = async () => {
    try {
      await navigator.clipboard.writeText(children);
      // update tooltip
      setTooltipText("Copied!");
    } catch {
      // update tooltip
      setTooltipText("Copy Failed!");
    } finally {
      // start time to reset tooltip msg
      // here we just rely on the first timer, alternatively could cancel existing timer and start a new one.
      if (timerRef.current === 0) {
        timerRef.current = setTimeout(onTimeout, 3000);
      }
    }
  };

  return (
    <>
      <div className="codeline-container" ref={containerRef}>
        <Badge style={{ backgroundColor: badgeColor }}>
          {badgeText}
        </Badge>
        <pre className="codeline">
          {children}
        </pre>
        {copyButtonEnabled && (
          <Button
            outline
            className="copybutton"
            size="sm"
            onClick={copyString}
            style={{
              backgroundColor: buttonColor,
              color: "white",
            }}
          >
            COPY
          </Button>
        )}
      </div>
      {copyButtonEnabled && (
        <UncontrolledTooltip placement="top" target={containerRef}>
          {({ scheduleUpdate }) => (
            <TooltipContent scheduleUpdate={scheduleUpdate}>{tooltipText}</TooltipContent>
          )}
        </UncontrolledTooltip>
      )}
    </>
  );
}

CopyLine.propTypes = {
  badgeText: PropTypes.string,
  badgeColor: PropTypes.string,
  allowCopy: PropTypes.bool,
  buttonColor: PropTypes.string,
  children: PropTypes.string.isRequired,
};

CopyLine.defaultProps = {
  allowCopy: true,
  badgeColor: colors.orangeDark,
  badgeText: "",
  buttonColor: colors.blueDark,
};
