import { createContext } from "react";

// This will be private to the facets component, (not accessible)
export const FacetsContext = createContext();
