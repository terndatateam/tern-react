import PropTypes from "prop-types";
import {
  useCallback,
  useState,
  useEffect,
  useMemo,
  useRef,
} from "react";
import { FacetsContext } from "../context/index.js";

import "./Facets.scss";

/**
 *
 * Wrapper facets component
 *
 * Hnadles general layout/styling of each facet.
 *
 * Collects inputs from all the facets, the facet handles the inputs individually
 */
export function Facets({
  children,
  updateOn,
  onChange,
  values,
  update,
  loading,
}) {
  // Values state
  const [openedFacet, setOpenedFacet] = useState(null);
  const facetsRef = useRef();

  const toggleFacet = useCallback((name) => {
    setOpenedFacet(name);
  }, [setOpenedFacet]);

  // Add a new value to a facet, passed directly to input
  const applyFacetSelection = useCallback(({ name, selection }) => {
    update((prevValues) => {
      const updatedValues = { ...prevValues };

      // If selection is falsy or an empty arr, delete key
      if (!selection || (Array.isArray(selection) && selection.length < 1)) {
        delete updatedValues[name];
      } else {
        updatedValues[name] = selection;
      }

      onChange(updatedValues);

      return updatedValues;
    });
  }, [update, onChange]);

  // Remove a single value from a facet
  const removeFacetValue = useCallback((facetValue) => {
    // Bit complex due to not being able to tell which input which
    const { value } = facetValue;
    const copyValues = Object.entries(values);

    const updatedValues = copyValues.map((entry) => {
      const [key, val] = entry;
      const updatedVal = [key];

      if (Array.isArray(val)) {
        const filtered = val.filter((item) => item !== value);
        updatedVal.push(filtered);
      } else if (val.value === value) {
        return null;
      }
      return updatedVal;
    });

    // const updatedFacet = facet.filter((item) => item !== value);
    update(Object.fromEntries(updatedValues.filter((val) => val.length > 1)));
  }, [values, update]);

  // Clears a whole facet from a list
  const clearAllFacetValues = useCallback(({ name }) => {
    const updatedValues = { ...values };
    delete updatedValues[name];
    update(updatedValues);
  }, [values, update]);

  // Facets context only accessed by Facets, or Facet component
  const context = useMemo(() => ({
    event: updateOn,
    applyFacetSelection,
    removeFacetValue,
    clearAllFacetValues,
    toggleFacet,
    openedFacet,
    loading,
  }), [updateOn, applyFacetSelection, removeFacetValue, clearAllFacetValues, loading, openedFacet, toggleFacet]);

  // Mount outside click capture
  useEffect(() => {
    const handleClickOutside = (eventTarget) => {
      if (facetsRef.current && !facetsRef.current.contains(eventTarget.target)) {
        setOpenedFacet(null);
      }
    };

    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  return (
    <div className="facets-wrapper" ref={facetsRef}>
      <div className="facets-content">
        <FacetsContext.Provider value={context}>
          {children}
        </FacetsContext.Provider>
      </div>
    </div>
  );
}

Facets.defaultProps = {
  loading: false,
  onChange: () => null,
};

Facets.propTypes = {
  children: PropTypes.node.isRequired,
  updateOn: PropTypes.string.isRequired,
  values: PropTypes.shape().isRequired,
  update: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  onChange: PropTypes.func,
};
