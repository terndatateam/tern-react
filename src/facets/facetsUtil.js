import {
  Children,
  cloneElement,
  isValidElement,
} from "react";

// Function that takes in child parameters, and applies a function handler to a specific "event"
export const updateAllChildComponentProps = (children, func, event, setOptionsAvailable) => {
  // recursively looks for components with available event prop to update
  const applyEventHandlerToChildren = (childrenArr) => Children.map(childrenArr, (child) => {
    if (!isValidElement(child)) {
      return child;
    }

    // get child prop keys list
    const propKeys = Object.keys(child.props);

    // If the child has an existing prop with event parameter
    if (propKeys.includes(event) && (typeof child.props[event] === "function" && child.props[event]() === null)) {
      // return a cloned element with the updated event handler with the func parameter
      if (propKeys.includes("options")) {
        if (!child.props?.options || (child.props.options && child.props.options.length < 1)) {
          setOptionsAvailable(false);
        } else {
          setOptionsAvailable(true);
        }
      }
      return cloneElement(child, { [event]: func });
    }

    // If the child has children, recursively apply the handler
    if (child.props && child.props.children) {
      return cloneElement(child, {
        children: applyEventHandlerToChildren(child.props.children),
      });
    }
    return child;
  });

  return applyEventHandlerToChildren(children);
};
