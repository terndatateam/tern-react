import PropTypes from "prop-types";
import {
  useContext,
  useCallback,
  useEffect,
  useRef,
} from "react";
import { Button, Collapse } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretDown, faCaretUp, faXmark } from "@fortawesome/free-solid-svg-icons";
import { FacetsContext } from "../context/index.js";
import { updateAllChildComponentProps } from "./facetsUtil.js";
import { handleFormat } from "../inputs/SelectInput.jsx";

import "./Facet.scss";
/**
 *
 * Facet value component to render a single facet value
 *
 * onDelete prop allows for passing function when user deletes facet
 *
 * Contains same formatting props as SelectInput for consistency
 *
 */

export function FacetValue({
  value, onDelete, hideDelete, italicised, casing, speciesFormat,
}) {
  if (!value) return null;

  const buttonClick = () => {
    onDelete({ value });
  };

  const label = handleFormat((value.label || value.value), italicised, casing, speciesFormat);

  return (
    <div className="facet-value">
      <p className="facet-value-content">
        {label}
      </p>
      {(!hideDelete && value) && (
        <Button className="facet-value-delete shadow-none" onClick={buttonClick}>
          <FontAwesomeIcon color="link" size="sm" icon={faXmark} />
        </Button>
      )}
    </div>
  );
}

FacetValue.defaultProps = {
  onDelete: null,
  hideDelete: false,
  italicised: false,
  casing: null,
  speciesFormat: false,
};

FacetValue.propTypes = {
  value: PropTypes.shape().isRequired,
  onDelete: PropTypes.func,
  hideDelete: PropTypes.bool,
  italicised: PropTypes.bool,
  casing: PropTypes.string,
  speciesFormat: PropTypes.bool,
};

/**
 *
 * Singular facet compoennt, input is passed as a child externally
 *
 * Prop drills on change to inputs retrieved from the facets component context.
 *
 * The facet will apply the onChange function provided by the parent facets component
 * onto all available inputs that do not have an function for the provided event handler
 * set within the props of the Facets Wrapper
*/
export function Facet({
  name,
  icon,
  value,
  defaultExpand,
  disabled,
  children,
}) {
  const defaultExpandApplied = useRef(false);

  const optionsAvailable = useRef(true);
  const setOptionsAvailable = (bool) => {
    optionsAvailable.current = bool;
  };

  const {
    applyFacetSelection: addValue,
    toggleFacet,
    openedFacet,
    removeFacetValue,
    event,
    loading,
  } = useContext(FacetsContext);

  const onToggle = useCallback(() => {
    toggleFacet(name);
  }, [toggleFacet, name]);

  const isOpen = openedFacet === name;

  const updatedChildren = updateAllChildComponentProps(children, addValue, event, setOptionsAvailable);

  useEffect(() => {
    // Opens facets if loading is false and defaultExpand is true
    if (!loading && (defaultExpand && !defaultExpandApplied.current)) {
      onToggle();
      defaultExpandApplied.current = true;
    }
  }, [loading, onToggle, isOpen, defaultExpand]);

  // Disables the facet if its loading or no options available AND a value is not already selected
  const disableFacet = (!optionsAvailable.current || loading) && (!value || value?.length < 1);

  return (
    <div className={`facet ${isOpen ? "opened" : "closed"}`}>
      <Button className="facet-header shadow-none" onClick={onToggle} disabled={(disabled && !loading) || disableFacet}>
        <span className="facet-title bold-title">
          {icon && <FontAwesomeIcon className="mr-2" icon={icon} /> }
          {name}
        </span>
        <div className="facet-toggle-btn">
          {((disabled || !optionsAvailable.current) && !loading)
            && <span className="helper-text mr-2">No Options</span> }
          {((optionsAvailable.current && !disabled) || loading)
            && <FontAwesomeIcon icon={!isOpen ? faCaretDown : faCaretUp} /> }
        </div>
      </Button>
      <Collapse className="facet-content" isOpen={isOpen}>
        {updatedChildren}
      </Collapse>
      <Collapse className="facet-values-wrapper" isOpen={!isOpen}>
        {value
        && (
        <div className="facet-values">
          {Array.isArray(value)
            ? value.map((val) => <FacetValue value={val} onDelete={removeFacetValue} />)
            : <FacetValue value={value} onDelete={removeFacetValue} />}
        </div>
        )}
      </Collapse>
    </div>
  );
}

Facet.defaultProps = {
  value: null,
  defaultExpand: false,
  disabled: false,
};

Facet.propTypes = {
  name: PropTypes.string.isRequired,
  icon: PropTypes.shape().isRequired,
  value: PropTypes.shape(),
  defaultExpand: PropTypes.bool,
  disabled: PropTypes.bool,
  children: PropTypes.node.isRequired,
  // isLoading: PropTypes.bool.isRequired,
};
