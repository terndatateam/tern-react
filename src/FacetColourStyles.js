import colors from "./scss/colors.module.scss";

/* Sidebar Styles */
// eslint-disable-next-line import/prefer-default-export
export const facetColourStyles = {
  control: (styles) => ({
    ...styles,
    backgroundColor: "transparent",
    fontSize: "13px",
    border: `1px solid ${colors.greenMid}`,
  }),
  option: (styles, {
    isDisabled, isFocused, isSelected,
  }) => ({
    ...styles,
    backgroundColor: (isSelected || isFocused) ? colors.greenLight : "transparent",
    color: isDisabled ? "gray" : colors.greenDark,
    fontSize: "13px",
    display: "flex",
    alignItems: "center",
    cursor: isDisabled ? "not-allowed" : "default",
    ".svg-icon": {
      marginRight: "0.3rem",
    },
    ":active": {
      backgroundColor: !isDisabled && ((isSelected || isFocused) ? colors.greenDark : "transparent"),
      // color: "#fff",
    },
    ":hover": {
      backgroundColor: colors.greenMid,
      // color: "#fff",
    },
  }),
  multiValue: (styles) => ({
    ...styles,
    backgroundColor: colors.greenLight,
    color: colors.greenDark,
    fontSize: "13px",
  }),
};
