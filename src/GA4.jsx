import PropTypes from "prop-types";
import { Helmet } from "react-helmet";

/**
 *
 *  A small renderless helper component to render GA4 integration (Google Analytics)
 *
 * To use this component just do:
 *
 * `import { GA4 } from "tern-react";`
 *
 * `<GA4  trackingId = {id}/>` to use it.
 *
 */
export function GA4(props) {
  const { trackingId } = props;

  if (!trackingId) {
    // setup dummy tracker to avoid errors in the app
    return (
      // insert script to add dummy gtag function
      <Helmet>
        <script>
          {"function gtag(){};"}
        </script>
      </Helmet>
    );
  }
  return (
    // insert tracking code for GA4
    <Helmet>
      <script async src={`https://www.googletagmanager.com/gtag/js?id=${trackingId}`} />
      <script>
        {`window.dataLayer = window.dataLayer || [];
          function gtag(){dataLayer.push(arguments);};
          gtag("js", new Date());
          gtag("config", "${trackingId}");`}
      </script>
    </Helmet>
  );
}

GA4.propTypes = {
  trackingId: PropTypes.string,
};
GA4.defaultProps = {
  trackingId: null,
};
