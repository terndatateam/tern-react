import "./icons.scss";
import { CircleIcon } from "./CircleIcon.jsx";
import { DrawSquareIcon } from "./DrawSquare.jsx";

function ClusterIcon(number = 0) {
  return (
    <div>
      <CircleIcon />
      <div className="cluster-number">{number}</div>
    </div>
  );
}

export { DrawSquareIcon, ClusterIcon };
