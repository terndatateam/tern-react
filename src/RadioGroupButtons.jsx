import PropTypes from "prop-types";
import { Input, FormGroup, Label } from "reactstrap";
import "./RadioGroupButtons.scss";

export function RadioGroupButtons({
  label, value, checked, onChange,
}) {
  return (
    <FormGroup check>
      <Label check>
        <Input
          type="radio"
          value={value}
          checked={checked}
          onChange={onChange}
        />
        {label}
      </Label>
    </FormGroup>
  );
}

RadioGroupButtons.propTypes = {
  label: PropTypes.string,
  value: PropTypes.string,
  checked: PropTypes.bool,
  onChange: PropTypes.func,
};

RadioGroupButtons.defaultProps = {
  label: null,
  value: null,
  checked: null,
  onChange: null,
};
