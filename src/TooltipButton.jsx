import { useRef } from "react";
import PropTypes from "prop-types";
import { Button, UncontrolledTooltip } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import "./TooltipButton.scss";

export function TooltipButton({
  children, color, onClick, icon, tooltip,
}) {
  const ref = useRef(null);
  // create icon here in case we need some more logic
  let iconComp = null;
  if (icon) {
    iconComp = [<FontAwesomeIcon icon={icon} />, " "];
  }
  // create tooltip here as well in case we need to check content
  let ttComp = null;
  if (tooltip) {
    ttComp = (
      <UncontrolledTooltip
        target={ref}
        placement="top"
        style={{ textAlign: "left" }}
      >
        {tooltip}
      </UncontrolledTooltip>
    );
  }
  return (
    <>
      <Button
        size="sm"
        color={color}
        onClick={onClick}
        innerRef={ref}
      >
        {iconComp}
        {children}
      </Button>
      {ttComp}
    </>
  );
}

TooltipButton.propTypes = {
  children: PropTypes.node.isRequired,
  color: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.shape(),
  tooltip: PropTypes.oneOfType([
    PropTypes.string,
    PropTypes.func,
  ]),
};

TooltipButton.defaultProps = {
  color: "secondary",
  onClick: null,
  icon: null,
  tooltip: null,
};
