import PropTypes from "prop-types";

export function DisplayCount({
  displayCount,
  onDisplayCountChange,
}) {
  const onChange = (event) => {
    onDisplayCountChange(Number(event.target.value));
  };

  return (
    <div className="result">
      Items per page:
      {" "}
      <select
        className="result-select"
        value={displayCount}
        onChange={onChange}
      >
        <option value={5}>5</option>
        <option value={10}>10</option>
        <option value={25}>25</option>
        <option value={50}>50</option>
        <option value={100}>100</option>
      </select>
    </div>
  );
}

DisplayCount.propTypes = {
  displayCount: PropTypes.number.isRequired,
  onDisplayCountChange: PropTypes.func.isRequired,
};
