import PropTypes from "prop-types";
import {
  Pagination as ReactstrapPagination,
  PaginationItem,
  PaginationLink,
} from "reactstrap";
import "./Pagination.scss";

export function Pagination(props) {
  const {
    onPageChange,
    currentPage,
    totalRecords,
    displayCount,
  } = props;
  const totalPages = Math.ceil(totalRecords / displayCount);

  return (
    <ReactstrapPagination>
      {/* disable first and previous buttons if on first page */}
      <PaginationItem
        disabled={currentPage === 1}
      >
        <PaginationLink
          onClick={() => onPageChange(1)}
          first
        />
      </PaginationItem>
      <PaginationItem
        disabled={currentPage === 1}
      >
        <PaginationLink
          onClick={() => onPageChange(Math.max(currentPage - 1, 1))}
          previous
        />
      </PaginationItem>
      <PaginationItem disabled key={currentPage}>
        <PaginationLink>{currentPage}</PaginationLink>
      </PaginationItem>
      {/* disable next and last buttons if on last page */}
      <PaginationItem
        disabled={currentPage === totalPages}
      >
        <PaginationLink
          onClick={() => onPageChange(Math.min(currentPage + 1, totalPages))}
          next
        />
      </PaginationItem>
      <PaginationItem
        disabled={currentPage === totalPages}
      >
        <PaginationLink
          onClick={() => onPageChange(totalPages)}
          last
        />
      </PaginationItem>
    </ReactstrapPagination>
  );
}

Pagination.propTypes = {
  onPageChange: PropTypes.func.isRequired,
  currentPage: PropTypes.number.isRequired,
  totalRecords: PropTypes.number.isRequired,
  displayCount: PropTypes.number.isRequired,
};
