import PropTypes from "prop-types";
import "./ResultsInfo.scss";

export function ResultsInfo({ displayCount, totalRecords }) {
  return (
    <div className="result">
      Showing
      {" "}
      {displayCount}
      {" "}
      of
      {" "}
      {totalRecords}
      {" "}
      results
    </div>
  );
}

ResultsInfo.propTypes = {
  displayCount: PropTypes.number.isRequired,
  totalRecords: PropTypes.number.isRequired,
};
