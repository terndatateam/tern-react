import PropTypes from "prop-types";

export function PageInfo({ currentPage, displayCount, totalRecords }) {
  const totalPages = Math.ceil(totalRecords / displayCount);

  return (
    <div className="result">
      Page:
      {" "}
      {currentPage}
      {" "}
      of
      {" "}
      {totalPages}
    </div>
  );
}

PageInfo.propTypes = {
  currentPage: PropTypes.number.isRequired,
  displayCount: PropTypes.number.isRequired,
  totalRecords: PropTypes.number.isRequired,
};
