import PropTypes from "prop-types";
import "./PaginationBar.scss";

/**
 * PaginationBar component contains only children.
 *
 * @returns {React.ReactNode}
 */
export function PaginationBar(props) {
  const {
    children,
  } = props;

  return (
    <div className="pagination-bar">
      {children}
    </div>
  );
}

PaginationBar.propTypes = {
  children: PropTypes.node,
};

PaginationBar.defaultProps = {
  children: null,
};
