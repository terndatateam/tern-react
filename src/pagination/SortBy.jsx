import PropTypes from "prop-types";

export function SortBy({ sortBy, onSortChange, sortOptions }) {
  const onChange = (event) => {
    onSortChange(event.target.value);
  };

  return (
    <div className="result">
      SortBy:
      {" "}
      <select
        className="result-select"
        onChange={onChange}
        value={sortBy}
      >
        {sortOptions.map((sortOption) => (
          <option key={sortOption.value} value={sortOption.value}>
            {sortOption.name}
          </option>
        ))}
      </select>
    </div>
  );
}

SortBy.propTypes = {
  sortBy: PropTypes.string.isRequired,
  onSortChange: PropTypes.func.isRequired,
  sortOptions: PropTypes.arrayOf(
    PropTypes.shape({
      name: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
};
