import {
  Button,
  Container,
  Col,
  Row,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import PropTypes from "prop-types";

import colors from "./scss/colors.module.scss";
import "./PageNotFound.scss";

function MailButton(props) {
  const { email, size } = props;

  return (
    <Button
      size={size}
      style={{ backgroundColor: colors.teal, borderColor: colors.teal }}
      href={`mailto:${email}`}
    >
      <FontAwesomeIcon icon={faEnvelope} />
    </Button>
  );
}

/**
 *
 * Component to display pages that don't exist in the Application.
 *
 * Support Email and a redirect element will be required props to ensure
 * users can email support or redirect back to homepage.
 *
 * Redirect Element props is used to inject a React Element, this can be a NavLink component or
 * an anchor element to redirect externally.
 *
 * Default prop for redirect element will be to link to tern.org.au.
 *
 */
export function PageNotFound(props) {
  const { supportEmail, redirectElement, btnSize } = props;

  return (
    <Container className="notfound-container">
      <Row>
        <Col sm={12}>
          <div className="notfound-content">
            <h1>Sorry, this page does not exist</h1>
            <h4>
              Return to
              {" "}
              {redirectElement}
              {" "}
              and email
              {" "}
              <MailButton email={supportEmail} size={btnSize} />
              {" "}
              for further support.
            </h4>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

MailButton.propTypes = {
  email: PropTypes.string.isRequired,
  size: PropTypes.string,
};

MailButton.defaultProps = {
  size: "sm",
};

PageNotFound.propTypes = {
  // support email button, generally passed in by config file
  supportEmail: PropTypes.string.isRequired,
  btnSize: PropTypes.string,
  // custom element to handle redirect
  redirectElement: PropTypes.element,
};

PageNotFound.defaultProps = {
  redirectElement: <a href="https://www.tern.org.au" target="_blank" rel="noreferrer">TERN</a>,
  btnSize: "sm",
};
