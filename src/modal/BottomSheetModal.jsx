import PropTypes from "prop-types";
import { useState, useEffect, useRef } from "react";
import Draggable from "react-draggable";
import "./BottomSheetModal.scss";

export function BottomSheetModal({
  children,
  initialBreakpoint, // represents a percentage of the parent element
  snapToBreakpoints,
  breakpoints,
  limitBoundsToBreakpoints,
  parentRef,
  isOpen,
  swipeDownToClose,
  toggle,
}) {
  const [parentHeight, setParentHeight] = useState(0);
  const [position, setPosition] = useState(0);
  const handlerHeight = 14;
  const minHeight = 50; // TODO: passable prop? Use breakpoints to define this min prop
  const toggledClosed = useRef(false);
  const resizeObserver = useRef(null);

  const [isMounted, setIsMounted] = useState(false);

  useEffect(() => {
    // Render null for SSR
    setIsMounted(true);
  }, []);

  const getBottomBounds = () => {
    if (limitBoundsToBreakpoints && breakpoints[0] && parentHeight) {
      // lowest bounds will match the first breakpoint
      return parentHeight * (1 - breakpoints[0]) - handlerHeight;
    }
    return parentHeight - minHeight - handlerHeight;
  };

  useEffect(() => {
    const currentParentRef = parentRef?.current;

    const updateParentHeight = () => {
      if (currentParentRef) {
        setParentHeight(currentParentRef.offsetHeight);

        if (initialBreakpoint) {
          setPosition(parentHeight * initialBreakpoint);
        }
      }
    };
    // Updates with screen size changes
    updateParentHeight();

    resizeObserver.current = new ResizeObserver(updateParentHeight);

    if (currentParentRef) {
      resizeObserver.current.observe(currentParentRef);
    }

    return () => {
      if (resizeObserver.current && currentParentRef) {
        resizeObserver.current.unobserve(currentParentRef);
      }
    };
  }, [parentRef, initialBreakpoint, parentHeight]);

  useEffect(() => {
    if (swipeDownToClose && (isOpen && toggledClosed.current)) {
      const heightPercentage = position / parentHeight;
      if ((1 - heightPercentage) < breakpoints[0]) {
        setPosition(parentHeight * initialBreakpoint); // Reset to initial position
        toggledClosed.current = false; // Reset toggled closed state
      }
    }
  }, [isOpen, initialBreakpoint, parentHeight, position, breakpoints, swipeDownToClose]);

  const handleDrag = (e, data) => {
    const pos = data.y;
    const newPos = Math.min(parentHeight - minHeight, pos);
    setPosition(newPos);
  };

  const onStopDrag = (e, data) => {
    const pos = data.y;
    const heightPercentage = pos / parentHeight;

    // If its smaller than first breakpoint then toggle closed
    if (swipeDownToClose && (1 - heightPercentage) < breakpoints[0]) {
      toggledClosed.current = true;
      toggle();
    }

    // Finds the closest break if enabled and breakpoints are added
    if (snapToBreakpoints && (Array.isArray(breakpoints) && breakpoints.length > 0)) {
      const closestPercentage = breakpoints.reduce(
        (closest, curr) => (
          Math.abs(curr - heightPercentage) < Math.abs(closest - heightPercentage) ? curr : closest
        ),
      );
      const newPos = closestPercentage * parentHeight;
      setPosition(newPos);
    }
  };

  const contentHeight = parentHeight - position - handlerHeight;

  if (!isMounted) return null;

  if (!parentHeight) return null;

  return (
    <div className={`bottom-drawer-container ${isOpen ? "open" : "closed"}`}>
      <div className="bottom-sheet-modal">
        <Draggable
          axis="y"
          handle=".drawer-handle"
          bounds={{
            top: 0,
            bottom: getBottomBounds(),
          }}
          position={{ x: 0, y: position }}
          onDrag={handleDrag}
          onStop={onStopDrag}
        >
          <div className="bottom-drawer">
            <div
              className="drawer-handle"
              style={{ height: `${handlerHeight}px` }}
            >
              <div className="handle-bar" />
            </div>
          </div>
        </Draggable>

        {/* Ensure content height is calculated and rendered properly */}
        <div
          className="content"
          style={{
            height: `${contentHeight}px`, // Dynamically adjusts the height of the content
            top: `${position + handlerHeight}px`, // Position content below the handle
          }}
        >
          {children}
        </div>
      </div>

    </div>
  );
}

BottomSheetModal.propTypes = {
  children: PropTypes.node.isRequired,
  parentRef: PropTypes.shape({ current: PropTypes.instanceOf(Element) }), // Parent container ref
  initialBreakpoint: PropTypes.number, // Initial position of the drawer
  breakpoints: PropTypes.arrayOf(PropTypes.number),
  snapToBreakpoints: PropTypes.bool,
  limitBoundsToBreakpoints: PropTypes.bool,
  isOpen: PropTypes.bool,
  toggle: PropTypes.func,
  swipeDownToClose: PropTypes.bool,
};

BottomSheetModal.defaultProps = {
  initialBreakpoint: 0.5, // Default initial breakpoint height (can be customized)
  parentRef: null,
  breakpoints: [0.25, 0.5, 0.75],
  limitBoundsToBreakpoints: false,
  snapToBreakpoints: false,
  swipeDownToClose: true,
  isOpen: true,
  toggle: () => null,
};
