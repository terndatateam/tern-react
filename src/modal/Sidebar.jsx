import PropTypes from "prop-types";

import "./Sidebar.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faClose } from "@fortawesome/free-solid-svg-icons";

export function Sidebar({
  isOpen,
  toggle,
  children,
  direction,
  width,
}) {
  return (
    <div
      className={`sidebar ${isOpen ? "open" : ""} ${direction}`}
      style={{ "--sidebar-width": width }}
    >
      <button type="button" className="close-btn" onClick={toggle}>
        <FontAwesomeIcon size="lg" icon={faClose} />
      </button>
      <div className="sidebar-content">
        {children}
      </div>
    </div>
  );
}

Sidebar.defaultProps = {
  direction: "right",
  width: "100%",
};

Sidebar.propTypes = {
  isOpen: PropTypes.bool.isRequired,
  toggle: PropTypes.func.isRequired,
  children: PropTypes.node.isRequired,
  width: PropTypes.string,
  direction: PropTypes.oneOf(["left", "right", "up"]),
};
