import { Button, Container } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import PropTypes from "prop-types";
import "./SubHeader.scss";

/**
 * This is the subheader component.
 * Will really be used for the home page in all UI applications
 *
 * Fairly customizable including background image.
 * If image is from the internet then it will be required to pass it in
 * a string surrounded by `url()`.
 *
 * Button tag can be used to direct to another page in the application
 * or else specify the tag as "a" to link to an external site
 */
export function SubHeader({
  title, tagline, route, buttonText, buttonIcon, buttonTag, backgroundImage, children,
}) {
  return (
    <div className="home-header" style={{ backgroundImage }}>
      <Container>
        {/* SUBHEADER */}
        <div className="home-caption">
          <div style={{ fontSize: "min(6vw, 2rem)", fontWeight: "500" }}>{title}</div>
          <p>
            {tagline}
          </p>
          {children && children}

          {!children
            && (
              <Button
                className="btn-homesearch mt-0"
                tag={buttonTag}
                href={buttonTag === "a" && route}
                to={route}
              >
                { buttonIcon && <FontAwesomeIcon className="mr-2" icon={buttonIcon} /> }
                {buttonText}
              </Button>
            )}
        </div>
      </Container>
    </div>
  );
}

SubHeader.defaultProps = {
  children: null,
};

SubHeader.propTypes = {
  title: PropTypes.string.isRequired,
  tagline: PropTypes.string.isRequired,
  route: PropTypes.string.isRequired,
  buttonText: PropTypes.string.isRequired,
  buttonIcon: PropTypes.shape().isRequired,
  buttonTag: PropTypes.shape().isRequired,
  backgroundImage: PropTypes.string.isRequired,
  children: PropTypes.node,
};
