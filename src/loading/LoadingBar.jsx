import "./LoadingBar.scss";

/**
 * Simple CSS loading bar animation. TODO: add more customisability, shapes, color etc.
 */
export function LoadingBar() {
  return (
    <div className="progress-bar-container">
      <div className="progress-slider" />
    </div>
  );
}
