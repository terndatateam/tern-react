import PropTypes from "prop-types";
import "./BlockUi.scss";
/**
 *
 *  A custom component that blocks the ui until some action is completed.
 *
 * To use this component just do:
 *
 * `import { BlockUi } from "tern-react";`
 *
 * `Usage:
 * const [loading, isLoading] = useState(false);
 * <BlockUi
 *       keepInView
 *       tag="div"
 *       blocking={isLoading}
 *     >
 *     <p>This content is a child of BlockUi component and will be blocked.</p>
 * <BlockUi/>`
 *
 */

function SpinnerLoader() {
  return (
    <div id="block-ui-spinner" className="la-ball-spin-fade la-green la-2x">
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
      <div />
    </div>
  );
}

export function BlockUi({
  keepInView, tag: Tag, blocking, children,
}) {
  const overlayStyle = {
    position: keepInView ? "absolute" : "fixed",
    zIndex: blocking ? 1040 : -1, // hide when not blocking
    cursor: blocking ? "wait" : "auto",
  };

  return (
    <Tag id="block-ui-container" className="block-ui">
      {blocking && <div id="block-ui-overlay" className="overlay" style={overlayStyle}><SpinnerLoader /></div>}
      <div className="children">
        {children}
      </div>
    </Tag>
  );
}

BlockUi.propTypes = {
  /** Set whether the blocking component should follow
   * the scroll or stay at a fixed position */
  keepInView: PropTypes.bool,
  /** tag to render as container element */
  tag: PropTypes.oneOfType([PropTypes.string, PropTypes.func, PropTypes.object]),
  /** Set whether the component should block its children */
  blocking: PropTypes.bool.isRequired,
  /** children to display */
  children: PropTypes.node,
};

BlockUi.defaultProps = {
  keepInView: false,
  tag: "div",
  children: null,
};
