import merge from "lodash/merge.js";

import PropTypes from "prop-types";

// default menu definitions
const menuResources = {
  termsOfUse: {
    label: "Terms Of Use",
    href: "https://www.tern.org.au/terms-of-use",
    order: 1,
  },
  disclaimer: {
    label: "Disclaimer",
    href: "https://www.tern.org.au/terms-of-use/#disclaimer",
    order: 2,
  },
  copyRight: {
    label: "Copyright",
    href: "https://www.tern.org.au/terms-of-use/#copyright",
    order: 3,
  },
  dataLicensing: {
    label: "Data Licensing",
    href: "https://www.tern.org.au/datalicence/",
    order: 4,
  },
  helpSupport: {
    label: "Help & Support",
    href: "https://ternaus.atlassian.net/wiki/spaces/TERNSup/overview",
    order: 5,
  },
};

// Keep the ordering of menu data and menu tools in alphabetical order.
const menuData = {
  ecoImages: {
    label: "EcoImages",
    href: "https://ecoimages.tern.org.au",
    order: 1,
  },
  ecoPlots: {
    label: "EcoPlots",
    href: "https://ecoplots.tern.org.au",
    order: 2,
  },
};

const menuTools = {
  coesra: {
    label: "CoESRA Virtual Desktop",
    href: "https://coesra.tern.org.au",
    order: 1,
  },
  tddp: {
    label: "Data Discovery Portal",
    href: "https://portal.tern.org.au",
    order: 2,
  },
  maps: {
    label: "Landscape Data Visualiser",
    href: "https://maps.tern.org.au",
    order: 3,
  },
  shared: {
    label: "SHaRED Data Submission",
    href: "https://shared.tern.org.au",
    order: 4,
  },
  linkeddata: {
    label: "TERN Linked Data Resources",
    href: "https://linkeddata.tern.org.au",
    order: 5,
  },
  ternaccount: {
    label: "TERN Account",
    href: "https://account.tern.org.au",
    order: 6,
  },
};

const menuDataTest = {
  ecoImages: {
    label: "EcoImages",
    href: "https://ecoimages-test.tern.org.au",
    order: 1,
  },
  ecoPlots: {
    label: "EcoPlots",
    href: "https://ecoplots-test.tern.org.au",
    order: 2,
  },
};

const menuToolsTest = {
  coesra: {
    label: "CoESRA Virtual Desktop",
    href: "https://coesra-test.tern.org.au",
    order: 1,
  },
  tddp: {
    label: "Data Discovery Portal",
    href: "https://portal-test.tern.org.au",
    order: 2,
  },
  maps: {
    label: "Landscape Data Visualiser",
    href: "https://maps-test.tern.org.au",
    order: 3,
  },
  shared: {
    label: "SHaRED Data Submission",
    href: "https://shared-test.tern.org.au",
    order: 4,
  },
  linkeddata: {
    label: "TERN Linked Data Resources",
    href: "https://linkeddata-test.tern.org.au",
    order: 5,
  },
  ternaccount: {
    label: "TERN Account",
    href: "https://account-test.tern.org.au",
    order: 6,
  },
};

const envMenus = {
  prod: {
    data: { label: "Data Apps", items: menuData, order: 1 },
    tools: { label: "Tools", items: menuTools, order: 2 },
    resources: { label: "Resources", items: menuResources, order: 3 },
  },
  test: {
    data: { label: "Data Apps", items: menuDataTest, order: 1 },
    tools: { label: "Tools", items: menuToolsTest, order: 2 },
    resources: { label: "Resources", items: menuResources, order: 3 },
  },
};

export function getTernMenu({ env = "test", overrides = {} } = {}) {
  const ret = merge({}, envMenus[env], overrides);
  return ret;
}

export function getTernAccountLink(env = "test") {
  switch (env) {
    case "prod":
      return "https://account.tern.org.au/";
    case "test":
    default:
      return "https://account-test.tern.org.au/";
  }
}

export const menuItemType = PropTypes.shape({
  label: PropTypes.string,
  href: PropTypes.string,
  order: PropTypes.number,
});

export const menuItemsType = PropTypes.shape({
  label: PropTypes.string,
  // TODO: would need a better proptype here ... each key in items is a menuItemType,
  //       but keys can be anything
  items: PropTypes.shape({}),
  order: PropTypes.number,
});

export const menuConfigType = PropTypes.shape({
  data: menuItemsType,
  tools: menuItemsType,
  resources: menuItemsType,
});
