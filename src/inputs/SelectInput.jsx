import PropTypes from "prop-types";
import { useState } from "react";
import Select, { components } from "react-select";
import { getStringCasing } from "../util.js";
import { InputWrapper } from "./InputWrapper.jsx";
import { formatSpeciesName } from "../util-components/formatSpeciesName.jsx";

import "./SelectInput.scss";

export function handleFormat(label, italicised, casing, speciesFormat) {
  if (speciesFormat) {
    return formatSpeciesName(label);
  }

  if (italicised) {
    return <i>{getStringCasing(label, casing)}</i>;
  }

  return getStringCasing(label, casing);
}

function TernOption(props, italicised, casing, speciesFormat) {
  const { data } = props;
  const { label } = data;

  return (
    <components.Option {...props}>
      {handleFormat(label, italicised, casing, speciesFormat)}
    </components.Option>
  );
}

TernOption.propTypes = {
  data: PropTypes.shape().isRequired,
};

function TernSingleValue(props, italicised, casing, speciesFormat) {
  const { data } = props;
  const { label } = data;

  return (
    <components.SingleValue {...props}>
      {handleFormat(label, italicised, casing, speciesFormat)}
    </components.SingleValue>
  );
}

TernSingleValue.propTypes = {
  data: PropTypes.shape().isRequired,
};

function TernMultiValue(props, italicised, casing, speciesFormat) {
  const { data } = props;
  const { label } = data;

  return (
    <components.MultiValue {...props}>
      {handleFormat(label, italicised, casing, speciesFormat)}
    </components.MultiValue>
  );
}

TernMultiValue.propTypes = {
  data: PropTypes.shape().isRequired,
};

/**
 * Standardized Select Input
 *
 * A reusable select input component built with `react-select`, designed for handling large datasets
 * and customizable styling options.
 *
 * Key Features:
 * - **Default Value**: Applies the `defaultValue` prop if options are available.
 * - **Scientific Styling**: Format species names with the `speciesFormat` prop for scientific presentations.
 * - **Text Casing**: Control text casing with the `casing` prop (`lower`, `upper`, or `title` for title case).
 * - **Italics**: Options can be italicized for emphasis.
 *
 * Note:
 * The `speciesFormat` prop overrides other styling options, such as `italicised` and `casing`.
 */

export function SelectInput({
  name,
  label,
  inputKey,
  description,
  tooltipText,
  tooltipPlacement,
  options,
  onBlur,
  onChange,
  italicised,
  speciesFormat,
  casing,
  values,
  defaultValue, // Default value can be just the value of the option you want applied once options are available
  placeholder,
  selectProps,
}) {
  const [selectedOption, setSelectedOption] = useState(defaultValue);

  const onSelectChange = (e) => {
    onChange({ name, selection: e });
    setSelectedOption(e);
  };

  return (
    <InputWrapper
      label={label}
      name={name}
      inputKey={inputKey}
      description={description}
      tooltipPlacement={tooltipPlacement}
      tooltipText={tooltipText}
    >

      {/* eslint-disable-next-line jsx-a11y/no-static-element-interactions, jsx-a11y/click-events-have-key-events */}
      <div className="select-wrapper" onClick={(e) => e.stopPropagation()}>
        <Select
          name={name}
          className="select-input"
          classNamePrefix="tern-select"
          closeMenuOnSelect={!selectProps?.isMulti}
          inputId={name}
          // options={options}
          options={options}
          value={values}
          defaultValue={selectedOption}
          components={{
            Option: (props) => TernOption(props, italicised, casing, speciesFormat),
            SingleValue: (props) => TernSingleValue(props, italicised, casing, speciesFormat),
            MultiValue: (props) => TernMultiValue(props, italicised, casing, speciesFormat),
          }}
          onChange={(e) => onSelectChange(e)}
          placeholder={placeholder}
          onBlur={(e) => onBlur(e)}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...selectProps}
        />
      </div>
    </InputWrapper>
  );
}

SelectInput.propTypes = {
  onChange: PropTypes.func,
  onInputChange: PropTypes.func,
  description: PropTypes.string,
  onBlur: PropTypes.func,
  inputKey: PropTypes.string,
  onSubmit: PropTypes.func,
  submitElement: PropTypes.element,
  italicised: PropTypes.bool,
  casing: PropTypes.string,
  speciesFormat: PropTypes.bool,
  values: PropTypes.arrayOf(PropTypes.shape()),
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(PropTypes.shape()).isRequired,
  label: PropTypes.string,
  defaultValue: PropTypes.arrayOf(PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
  })),
  placeholder: PropTypes.string,
  tooltipPlacement: PropTypes.oneOf(["top", "left", "right", "bottom"]),
  tooltipText: PropTypes.string,
  selectProps: PropTypes.shape(),
};

SelectInput.defaultProps = {
  label: null,
  tooltipText: null,
  description: null,
  placeholder: "Select...",
  values: [],
  tooltipPlacement: "right",
  submitElement: null,
  selectProps: {},
  italicised: false,
  casing: null,
  speciesFormat: false,
  defaultValue: [],
  inputKey: null,
  onChange: () => null,
  onInputChange: () => null,
  onBlur: () => null,
  onSubmit: () => null,
};
