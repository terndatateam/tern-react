import PropTypes from "prop-types";
import { InputWrapper } from "./InputWrapper.jsx";

/**
 * Reusable native dropdown for smaller use cases
 *
 * Utilises the HTML select and option for smaller use cases
 * that dont require Select Input
 *
 * More useful for pagination functions
 *
 * Custom TERN design is applied here
 */
export function DropdownInput({
  name,
  label,
  inputKey,
  description,
  tooltipText,
  tooltipPlacement,
  value,
  children,
  onChange,
}) {
  // On change
  const onSelectChange = (event) => {
    onChange({ name, selection: event.target.value });
  };

  return (
    <InputWrapper
      label={label}
      name={name}
      inputKey={inputKey}
      description={description}
      tooltipPlacement={tooltipPlacement}
      tooltipText={tooltipText}
    >
      <select
        className="dropdown-input input"
        name={name}
        id={name}
        value={value}
        onChange={onSelectChange}
      >
        {children}
      </select>
    </InputWrapper>
  );
}

DropdownInput.propTypes = {
  onChange: PropTypes.func,
  inputKey: PropTypes.string,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  label: PropTypes.string,
  children: PropTypes.node.isRequired,
  value: PropTypes.shape({
    label: PropTypes.string.isRequired,
    value: PropTypes.string,
  }),
  tooltipPlacement: PropTypes.oneOf(["top", "left", "right", "bottom"]),
  tooltipText: PropTypes.string,
};

DropdownInput.defaultProps = {
  label: null,
  inputKey: null,
  description: null,
  tooltipText: null,
  tooltipPlacement: "right",
  value: null,
  onChange: () => null,
};
