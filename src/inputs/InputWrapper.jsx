import PropTypes from "prop-types";
import {
  Label,
  Tooltip,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useState } from "react";
import { faQuestionCircle } from "@fortawesome/free-solid-svg-icons";
import "./InputWrapper.scss";

/**
 *
 * Input wrapper component used to standardise labelling and tooltip control
 *
 * Descriptions can also be supplied which renders below the input following usual standards
 * with input design
 *
 */

export function InputWrapper({
  label,
  name,
  inputKey,
  description,
  tooltipText,
  tooltipPlacement,
  children,
  error,
}) {
  const [toolTipOpen, setToolTipOpen] = useState(false);
  const toggleTip = () => setToolTipOpen(!toolTipOpen);

  return (
    <div className={`input-container ${error ? "error" : ""}`} key={inputKey}>
      {label
          && (
            <Label className="input-label" for={name}>
              {label}
              {tooltipText
              && (
              <>
                <FontAwesomeIcon className={`${name}-input ml-1`} icon={faQuestionCircle} />
                <Tooltip
                  isOpen={toolTipOpen}
                  className="input-tooltip"
                  target={`.${name}-input`}
                  placement={tooltipPlacement}
                  toggle={toggleTip}
                >
                  {tooltipText}
                </Tooltip>
              </>
              )}
            </Label>
          )}
      {children}
      {(description && !error)
          && <small className="input-description">{description}</small>}
      {error
        && <small className="input-description error">{error}</small>}
    </div>
  );
}

InputWrapper.propTypes = {
  label: PropTypes.string,
  inputKey: PropTypes.string,
  description: PropTypes.string,
  name: PropTypes.string.isRequired,
  tooltipText: PropTypes.string,
  tooltipPlacement: PropTypes.oneOf(["top", "right", "bottom", "left"]),
  children: PropTypes.node.isRequired,
  error: PropTypes.string,
};

InputWrapper.defaultProps = {
  tooltipText: null,
  description: null,
  tooltipPlacement: null,
  label: null,
  inputKey: null,
  error: null,
};
