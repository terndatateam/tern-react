import PropTypes from "prop-types";
import { useState } from "react";
import {
  InputGroup,
  Input,
  Button,
  InputGroupAddon,
} from "reactstrap";
import { InputWrapper } from "./InputWrapper.jsx";
import "./TextInput.scss";

/**
 * Text Input
 *
 * Basic text input
 *
*/

export function TextInput({
  name,
  label,
  inputKey,
  description,
  tooltipText,
  tooltipPlacement,
  value,
  onBlur,
  onChange,
  onSubmit,
  error,
  submitElement,
  placeholder,
  inputProps,
}) {
  const [input, setInput] = useState("");
  // on input change
  const onInputChange = (e) => {
    const inputValue = e.target.value;
    setInput(e.target.value);
    onChange({ name, selection: { value: inputValue } });
  };

  return (
    <InputWrapper
      label={label}
      name={name}
      description={description}
      error={error}
      tooltipPlacement={tooltipPlacement}
      tooltipText={tooltipText}
      inputKey={inputKey}
    >
      <InputGroup>
        <Input
          onBlur={(e) => onBlur(e)}
          onChange={onInputChange}
          className="text-input input"
          placeholder={placeholder}
          id={name}
          type="text"
          name={name}
          value={value}
          // eslint-disable-next-line react/jsx-props-no-spreading
          {...inputProps}
        />
        {
        submitElement
        && (
        <InputGroupAddon addonType="append">
          <span className="separator" />
          <Button
            className="submit-input-btn"
            onClick={() => onSubmit({ name, selection: { value: input } })}
          >
            {submitElement}
          </Button>
        </InputGroupAddon>
        )
      }
      </InputGroup>
    </InputWrapper>
  );
}

TextInput.propTypes = {
  onChange: PropTypes.func,
  onBlur: PropTypes.func,
  onSubmit: PropTypes.func,
  submitElement: PropTypes.element,
  name: PropTypes.string.isRequired,
  error: PropTypes.string,
  description: PropTypes.string,
  value: PropTypes.string,
  inputKey: PropTypes.string,
  label: PropTypes.string,
  icon: PropTypes.element,
  placeholder: PropTypes.string,
  tooltipPlacement: PropTypes.oneOf(["top", "left", "right", "bottom"]),
  tooltipText: PropTypes.string,
  inputProps: PropTypes.shape(),
};

TextInput.defaultProps = {
  description: null,
  inputKey: null,
  icon: null,
  value: null,
  label: null,
  error: null,
  tooltipText: null,
  inputProps: {},
  placeholder: "",
  tooltipPlacement: "right",
  submitElement: null,
  onChange: () => null,
  onBlur: () => null,
  onSubmit: () => null,
};
