import { components } from "react-select";
import PropTypes from "prop-types";
// css the checkbox

import "./Option.scss";

export function Option(props) {
  const { data, label } = props;
  return (
    // eslint-disable-next-line react/jsx-props-no-spreading
    <components.Option {...props}>
      <div>
        <span>
          <span>
            <div className="label-checkbox">
              {label}
              {" "}
              {data.count > 0 && (
                ` (${data.count.toLocaleString()})`
              )}
            </div>
          </span>
        </span>
      </div>
    </components.Option>
  );
}

Option.propTypes = {
  label: PropTypes.string.isRequired,
  data: PropTypes.shape({
    label: PropTypes.string,
    value: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    count: PropTypes.number,
  }).isRequired,
  isSelected: PropTypes.bool.isRequired,
};
