import { useState, useEffect } from "react";
import PropTypes from "prop-types";
import DatePicker from "react-datepicker";
import {
  formatISO, parseISO, isValid, differenceInCalendarMonths, format, startOfMonth, endOfMonth,
} from "date-fns";
import { Input } from "reactstrap";
import { Range } from "rc-slider";
import addMonths from "date-fns/addMonths/index.js";
import { InputWrapper } from "./InputWrapper.jsx";
import { SelectInput } from "./SelectInput.jsx";
import colors from "../scss/colors.module.scss";
import { range } from "../util.js";

import "./DateRangeInput.scss";
import "./InputWrapper.scss";

// Function that returns a new iso date with ability to add date, date is validated for iso as well
const ensureISODate = (date) => {
  if (!date) return formatISO(new Date(), { representation: "date" });
  if (typeof date === "string" && isValid(parseISO(date))) return date;
  return formatISO(new Date(date), { representation: "date" });
};

// Generates the slider markas
function generateRangeMarks(minDate, maxDate, diff) {
  // when the range includes only single month
  if (diff === 0) {
    return {
      0: { label: format(new Date(minDate), "yyyy-MM") },
      1: { label: format(new Date(minDate), "yyyy-MM") },
    };
  }
  const marks = {
    0: { label: format(new Date(minDate), "yyyy-MM") },
    [diff]: { label: format(new Date(maxDate), "yyyy-MM") },
  };
  if (diff > 24) {
    // add 1/3 and 2/3 markers
    const pos1 = Math.floor(diff / 3);
    const pos2 = Math.floor(diff * (2 / 3));
    // TODO: can't show labels if display becomes too small....
    //       need a way to dynamically hide/show labels visible if there is enough space
    marks[pos1] = ""; // format(addMonths(minDate, pos1), "yyyy-MM");
    marks[pos2] = ""; // format(addMonths(minDate, pos2), "yyyy-MM");
  } else if (diff > 12) {
    // add 1/2 marker
    const pos = Math.floor(diff / 2);
    marks[pos] = ""; // format(addMonths(minDate, pos), "yyyy-MM");
  }
  return marks;
}

/** Custom header for date month picker */
function CalendarHeader({
  date,
  name,
  minDate,
  maxDate,
  onYearChange,
}) {
  const year = new Date(date).getFullYear();
  const minYear = new Date(minDate).getFullYear();
  const maxYear = new Date(maxDate).getFullYear();

  const yearOptions = range(minYear, maxYear);
  const selectOptions = yearOptions.map((val) => ({ label: val, value: val }));

  const handleYearChange = ({ selection }) => {
    const newYear = parseInt(selection.value, 10);
    const currentMonth = new Date(date).getMonth();
    const newDate = new Date(newYear, currentMonth || 0, 1);
    onYearChange(newDate);
  };

  return (
    <div className="calendar-header">
      <SelectInput
        name={name}
        label="Year"
        values={[{ label: year, value: year }]}
        options={selectOptions}
        onChange={handleYearChange}
        selectProps={{ menuScrollToFocusedOption: true }}
      >
        {yearOptions.map((yearOption) => (
          <option key={yearOption} value={yearOption}>{yearOption}</option>
        ))}
      </SelectInput>
    </div>
  );
}

CalendarHeader.propTypes = {
  date: PropTypes.string.isRequired,
  minDate: PropTypes.string.isRequired,
  maxDate: PropTypes.string.isRequired,
  onYearChange: PropTypes.func.isRequired,
  name: PropTypes.string.isRequired,
};

export function DateRangeInput({
  name,
  defaultValue,
  minDate,
  maxDate,
  onChange,
}) {
  const minimumDate = ensureISODate(minDate);
  const maximumDate = ensureISODate(maxDate);

  // States for selected dates
  const [dateValues, setDateValues] = useState(
    { start: defaultValue?.from || minimumDate, end: defaultValue?.to || maximumDate },
  );

  const rangeMonthDifference = differenceInCalendarMonths(new Date(maximumDate), new Date(minimumDate));

  const rangeValue = [
    differenceInCalendarMonths(new Date(dateValues.start), new Date(minimumDate)),
    differenceInCalendarMonths(new Date(dateValues.end), new Date(minimumDate)),
  ];

  // Slider properties including range marks and intervals
  const rangeMarks = generateRangeMarks(minimumDate, maximumDate, rangeMonthDifference);

  useEffect(() => {
    // Resets the range values and max min dates if changes
    const min = ensureISODate(minDate);
    const max = ensureISODate(maxDate);

    setDateValues({ start: min, end: max });
  }, [minDate, maxDate]);

  useEffect(() => {
    // Reset the date values if default changes
    if (defaultValue?.from && defaultValue?.to) {
      setDateValues({ start: defaultValue.from, end: defaultValue.to });
    }
  }, [defaultValue]);

  const handleOnInputDateChange = (type, value) => {
    const updatedDate = { ...dateValues };

    if (!value) {
      updatedDate[type] = type === "start" ? ensureISODate(minimumDate) : ensureISODate(maximumDate);
    } else {
      // Ensures updated month/year is start or end of month
      const newDate = type === "start" ? startOfMonth(value) : endOfMonth(value);
      updatedDate[type] = ensureISODate(format(newDate, "yyyy-MM-dd"));
    }

    setDateValues({ start: updatedDate.start, end: updatedDate.end });
    onChange({ name, selection: { from: updatedDate.start, to: updatedDate.end } });
  };

  const handleSliderChange = (value) => {
    // Converts slider value to dates, start and end go to beginning and end of month respectively
    const start = format(startOfMonth(addMonths(new Date(minDate), value[0])), "yyyy-MM-dd");
    const end = format(endOfMonth(addMonths(new Date(minDate), value[1])), "yyyy-MM-dd");

    setDateValues({ start, end });
  };

  const handleSliderAfterChange = () => {
    onChange({ name, selection: { from: dateValues.start, to: dateValues.end } });
  };

  return (
    <div className="date-range-picker">
      <div className="date-inputs">
        <InputWrapper
          label="Start Date"
          name={`daterange-start-${name}`}
          tooltipText="Adjust the start date selected."
          tooltipPlacement="right"
        >
          <DatePicker
            selected={new Date(dateValues.start)}
            onChange={(date) => handleOnInputDateChange("start", date)}
            isClearable
            minDate={new Date(minimumDate)}
            maxDate={new Date(dateValues.end)}
            openToDate={new Date(dateValues.start)}
            showMonthYearPicker
            placeholderText="yyyy-MM"
            customInput={<Input name={`daterange-start-${name}`} className="start-date date-input input" />}
            dateFormat="yyyy-MM"
            renderCustomHeader={(props) => (
              <CalendarHeader
                {...props}
                date={dateValues.start}
                name={`year-start-${name}`}
                type="start"
                minDate={minimumDate}
                maxDate={dateValues.end}
                onYearChange={(newDate) => handleOnInputDateChange("start", newDate)}
              />
            )}
          />
        </InputWrapper>

        <InputWrapper
          label="End Date"
          name={`daterange-end-${name}`}
          tooltipText="Adjust the end date selected."
          tooltipPlacement="right"
        >
          <DatePicker
            selected={new Date(dateValues.end)}
            dateFormat="yyyy-MM"
            placeholderText="yyyy-MM"
            showMonthYearPicker
            minDate={new Date(dateValues.start)}
            maxDate={new Date(maximumDate)}
            openToDate={new Date(dateValues.end)}
            onChange={(date) => handleOnInputDateChange("end", date)}
            isClearable
            popperProps={{
              strategy: "fixed",
            }}
            customInput={<Input name={`daterange-end-${name}`} className="end-date date-input input" />}
            renderCustomHeader={(props) => (
              <CalendarHeader
                {...props}
                date={dateValues.end}
                name={`year-end-${name}`}
                minDate={dateValues.start}
                maxDate={maximumDate}
                type="end"
                onYearChange={(newDate) => handleOnInputDateChange("end", newDate)}
              />
            )}
          />
        </InputWrapper>
      </div>
      <div className="date-slider">
        <Range
          min={0}
          max={rangeMonthDifference}
          marks={rangeMarks}
          value={rangeValue}
          tabIndex={[-1, -1]}
          trackStyle={[{ backgroundColor: colors.greenMid }]}
          handleStyle={[{ borderColor: colors.greenMid }, { borderColor: colors.greenMid }]}
          activeDotStyle={{ borderColor: colors.greenMid }}
          onChange={handleSliderChange}
          onAfterChange={handleSliderAfterChange}
        />
      </div>
    </div>
  );
}

DateRangeInput.defaultProps = {
  minDate: "1970-01-01",
  maxDate: ensureISODate(),
  onChange: () => null,
  defaultValue: {
    from: "1970-01-01",
    to: ensureISODate(),
  },
};

DateRangeInput.propTypes = {
  minDate: PropTypes.string,
  maxDate: PropTypes.string,
  defaultValue: PropTypes.shape({
    from: PropTypes.string,
    to: PropTypes.string,
  }),
  onChange: PropTypes.func,
  name: PropTypes.string.isRequired,
};
