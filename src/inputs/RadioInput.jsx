import PropTypes from "prop-types";
import { useState } from "react";
import {
  Input,
  FormGroup,
  Label,
} from "reactstrap";
import { InputWrapper } from "./InputWrapper.jsx";
import "./RadioInput.scss";

/**
 * Radio Input
 *
 * Useful for boolean inputs like true or false etc.
 *
 * If multiple values need to be selected, then change the type to checkbox
 */
export function RadioInput({
  name,
  label,
  description,
  tooltipText,
  tooltipPlacement,
  options,
  inputKey,
  defaultValue,
  type,
  onChange,
}) {
  const isCheckbox = type === "checkbox";

  let initialValues = "";
  if (isCheckbox) {
    initialValues = Array.isArray(defaultValue) ? defaultValue : [];
  } else if (defaultValue) {
    initialValues = defaultValue;
  }

  const [selectedValues, setSelectedValues] = useState(initialValues);

  const handleChange = (option) => {
    if (isCheckbox) {
      const isChecked = selectedValues.includes(option.value);
      let updatedValues;

      if (isChecked) {
        updatedValues = selectedValues.filter((val) => val !== option.value);
      } else {
        updatedValues = [...selectedValues, option.value];
      }

      const selection = options.filter((opt) => updatedValues.includes(opt.value));

      setSelectedValues(updatedValues);
      onChange({ name, selection });
    } else {
      setSelectedValues(option.value);
      onChange({ name, selection: { label: option.label, value: option.value } });
    }
  };

  return (
    <InputWrapper
      label={label}
      name={name}
      inputKey={inputKey}
      description={description}
      tooltipPlacement={tooltipPlacement}
      tooltipText={tooltipText}
    >
      {options.map((option) => {
        const inputId = `input-${name}-${option.value}`;
        const isChecked = isCheckbox
          ? selectedValues.includes(option.value)
          : selectedValues === option.value;

        return (
          <FormGroup key={inputId} check>
            <Input
              type={type}
              id={inputId} // Unique ID
              name={name}
              value={option.value}
              checked={isChecked}
              onChange={() => handleChange(option)}
            />
            <Label for={inputId} check>
              {option.label}
            </Label>
          </FormGroup>
        );
      })}
    </InputWrapper>
  );
}
RadioInput.propTypes = {
  onChange: PropTypes.func,
  inputKey: PropTypes.string,
  name: PropTypes.string.isRequired,
  options: PropTypes.arrayOf(
    PropTypes.shape({
      label: PropTypes.string.isRequired,
      value: PropTypes.string.isRequired,
    }),
  ).isRequired,
  description: PropTypes.string,
  label: PropTypes.string,
  tooltipPlacement: PropTypes.oneOf(["top", "left", "right", "bottom"]),
  tooltipText: PropTypes.string,
  defaultValue: PropTypes.oneOfType([PropTypes.string, PropTypes.arrayOf(PropTypes.string)]),
  type: PropTypes.oneOf(["radio", "checkbox"]),
};

RadioInput.defaultProps = {
  label: null,
  inputKey: null,
  description: null,
  tooltipText: null,
  defaultValue: null,
  type: "radio",
  tooltipPlacement: "right",
  onChange: () => null,
};
