import PropTypes from "prop-types";
import { useState } from "react";
import { InputWrapper } from "./InputWrapper.jsx";
import "./SwitchInput.scss";

/**
 * Switch Input
 *
 * This is a simple toggle input. To return one value only
 * in onChange function
 *
 *
*/

export function SwitchInput({
  name,
  label,
  inputKey,
  description,
  tooltipText,
  tooltipPlacement,
  iconOn,
  iconSwitch,
  iconOff,
  defaultValue,
  onChange,
}) {
  const [input, setInput] = useState(defaultValue);

  const toggle = () => {
    setInput(!input);
    onChange({ name, selection: !input });
  };

  return (
    <InputWrapper
      label={label}
      name={name}
      description={description}
      tooltipPlacement={tooltipPlacement}
      tooltipText={tooltipText}
      inputKey={inputKey}
    >
      <button id={name} type="button" className={`switch-input ${input ? "active" : "unactive"}`} onClick={toggle}>
        <div className="toggle-off">
          {iconOff}
        </div>
        <div className="switch-toggle">
          {iconSwitch}
        </div>
        <div className="toggle-on">
          {iconOn}
        </div>
      </button>
    </InputWrapper>
  );
}

SwitchInput.propTypes = {
  onChange: PropTypes.func,
  name: PropTypes.string.isRequired,
  description: PropTypes.string,
  inputKey: PropTypes.string,
  label: PropTypes.string,
  iconOn: PropTypes.element,
  iconOff: PropTypes.element,
  iconSwitch: PropTypes.element,
  tooltipPlacement: PropTypes.oneOf(["top", "left", "right", "bottom"]),
  tooltipText: PropTypes.string,
  defaultValue: PropTypes.bool,
};

SwitchInput.defaultProps = {
  description: null,
  defaultValue: false,
  inputKey: null,
  iconOn: null,
  iconOff: null,
  iconSwitch: null,
  label: null,
  tooltipText: null,
  tooltipPlacement: "right",
  onChange: () => null,
};
