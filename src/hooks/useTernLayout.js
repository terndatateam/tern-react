import { useEffect, useState } from "react";

export const useTernLayout = () => {
  const getLayout = () => ({
    width: window?.innerWidth,
    height: window?.innerHeight,
    isMobile: window?.innerWidth < 768,
    orientation: window?.innerHeight < window?.innerWidth ? "landscape" : "portrait",
  });

  const [layout, setLayout] = useState(getLayout());

  useEffect(() => {
    const handleResize = () => {
      setLayout(getLayout());
    };

    window.addEventListener("resize", handleResize);

    return () => {
      window.removeEventListener("resize", handleResize);
    };
  }, []);

  return layout;
};
