import { useState } from "react";
import { v4 as generateId } from "uuid";

/**
 *
 * useNotifications Hook Component
 *
 * This is a notification hook to send UI messages for a short time.
 *
 */

export function useNotifications() {
  // notifications list
  const [notifications, setNotifications] = useState([]);

  // status is not required for this, a default icon will show
  const pushNotification = (title, message, status = "info") => {
    const id = generateId();
    const notification = {
      id,
      title,
      message,
      status,
    };
    setNotifications((prevNotifications) => [...prevNotifications, notification]);
  };

  const closeNotification = (id) => {
    // removes notification by id
    setNotifications((prevNotifications) => prevNotifications.filter((notification) => notification.id !== id));
  };

  return {
    notifications,
    setNotifications,

    pushNotification,
    closeNotification,

  };
}
