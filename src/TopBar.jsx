import { useState } from "react";
import PropTypes from "prop-types";
import {
  Col, Container, Row, Dropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from "reactstrap";
import orderBy from "lodash/orderBy.js";
import { faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";

import { getTernMenu, menuConfigType } from "./menu.js";

import logo from "./images/logo-mini-all.png";

import "./TopBar.scss";

export function TopBar({ menuConfig, home }) {
  return (
    <Container className="above-header" fluid>
      <Container>
        <Row className="justify-content-end">
          <Col xs="12" sm="auto" className="above-header-menu">
            <Row className="no-gutters">
              {orderBy(Object.values(menuConfig).filter((x) => x), "order").map(
                (item) => (
                  <Col xs="auto" key={item.label}><TopBarDropdown label={item.label} items={item.items} /></Col>
                ),
              )}
            </Row>
          </Col>
          <Col xs="12" sm="auto" className="above-header-links">
            <Row className="justify-content-end">
              <Col xs="auto" className="link-tern-home">
                <a href={home} target="_blank" rel="noreferrer"><img src={logo} alt="logo" /></a>
              </Col>
              <Col xs="auto" className="link-data-portal">
                <a href={menuConfig.tools.items.tddp.href} target="_blank" rel="noreferrer">
                  <FontAwesomeIcon icon={faSearch} />
                  {" "}
                  Data
                </a>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </Container>
  );
}

TopBar.propTypes = {
  menuConfig: menuConfigType,
  home: PropTypes.string,
};

TopBar.defaultProps = {
  menuConfig: getTernMenu(),
  home: "https://www.tern.org.au",
};

function TopBarDropdown({ label, items }) {
  const [open, setOpen] = useState(false);

  const toggle = () => setOpen(!open);

  return (
    <Dropdown
      isOpen={open}
      toggle={toggle}
    >
      <DropdownToggle caret tag="span">{label}</DropdownToggle>
      <DropdownMenu>
        {orderBy(Object.values(items).filter((x) => x), ["order"]).map(
          (item) => (
            <DropdownItem key={item.label} tag="a" href={item.href} target="_blank" rel="noopener noreferrer">
              {item.label}
            </DropdownItem>
          ),
        )}
      </DropdownMenu>
    </Dropdown>
  );
}

TopBarDropdown.propTypes = {
  label: PropTypes.string.isRequired,
  items: PropTypes.PropTypes.shape({}).isRequired,
};
