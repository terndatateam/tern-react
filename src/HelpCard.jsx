import PropTypes from "prop-types";
import {
  Card, CardHeader, CardTitle, CardText, CardBody,
} from "reactstrap";
import ternLogoSmall from "./images/tern-logo-small.png";
import "./HelpCard.scss";
// a generic message component that can be used to display a message
export function HelpCard({
  title, code, message, icon,
}) {
  return (
    <Card style={{ width: "50%", margin: "auto" }}>
      <CardHeader className="pb-0" style={{ textAlign: "center" }}>
        <CardTitle tag="h4">
          {title}
          {" "}
          { code }
        </CardTitle>
      </CardHeader>
      <img alt="TERN" className="mt-3" src={ternLogoSmall} style={{ margin: "auto" }} width="50%" />
      <CardBody style={{ textAlign: "center" }}>
        <CardText>
          {message}
        </CardText>
        {icon}
      </CardBody>
    </Card>
  );
}

HelpCard.propTypes = {
  code: PropTypes.number,
  message: PropTypes.node,
  title: PropTypes.string.isRequired,
  icon: PropTypes.element,
};

HelpCard.defaultProps = {
  code: null,
  message: null,
  icon: null,
};
