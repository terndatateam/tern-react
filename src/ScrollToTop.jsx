import PropTypes from "prop-types";
import "./ScrollToTop.scss";

export function ScrollToTop(props) {
  const { message } = props;

  const scrollToTop = () => {
    window.scrollTo(0, 0);
  };

  return (
    <button type="button" className="scroll" onClick={scrollToTop}>
      <h5 className="scroll-to-top">
        {message}
      </h5>
    </button>
  );
}

ScrollToTop.propTypes = {
  // string for custom text
  message: PropTypes.string,
};

ScrollToTop.defaultProps = {
  message: "Scroll To Top",
};
