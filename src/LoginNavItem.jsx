import PropTypes from "prop-types";
import {
  Button,
  NavItem,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faSpinner,
  faSignInAlt,
  faSignOutAlt,
  faUserCircle,
  faUser,
} from "@fortawesome/free-solid-svg-icons";
import { getTernAccountLink } from "./menu.js";

import "./LoginNavItem.scss";

export function LoginNavItem({
  login, onLogin, onLogout, accountURL, showAccountURL, children,
}) {
  if (!login.user) {
    return (
      <NavItem className="login-nav-item">
        <Button
          color="login"
          className="ml-auto"
          style={{ whiteSpace: "normal" }}
          onClick={onLogin}
          disabled={login.checking}
        >
          Sign in
          <FontAwesomeIcon className="ml-2" icon={login.checking ? faSpinner : faSignInAlt} />
        </Button>
      </NavItem>
    );
  }
  // logged in ... show dropdown menu
  return (
    <UncontrolledDropdown nav className="login-nav-item">
      <DropdownToggle nav caret>
        {login.user.name}
        &nbsp;
        <FontAwesomeIcon icon={faUserCircle} />
      </DropdownToggle>
      <DropdownMenu right>
        {children}
        {children && (<DropdownItem divider />)}
        {showAccountURL && (
          <DropdownItem href={accountURL || getTernAccountLink()} target="_blank">
            TERN Account
            &nbsp;
            <FontAwesomeIcon icon={faUser} />
          </DropdownItem>
        )}
        <DropdownItem divider />
        <DropdownItem onClick={onLogout}>
          Sign Out
          &nbsp;
          <FontAwesomeIcon icon={faSignOutAlt} />
        </DropdownItem>
      </DropdownMenu>
    </UncontrolledDropdown>
  );
}

LoginNavItem.propTypes = {
  login: PropTypes.shape({
    // if user object evaluates to true, then this component assumes a user is logged in.
    user: PropTypes.shape({
      // user display name
      name: PropTypes.string,
    }),
    // if true, login/logout buttons are disabled and spinner is shown
    // can be used to show login/logout async task activity
    checking: PropTypes.bool,
  }),
  // onClick handler for log in action
  onLogin: PropTypes.func,
  // onClick handler fol log out action
  onLogout: PropTypes.func,
  // App specific URL for the Tern Account button. If not set,
  // it uses the default URL
  accountURL: PropTypes.string,
  // Show or not show the Tern Account button
  // Note: for the Tern Account itself, we don't want to show it!
  showAccountURL: PropTypes.bool,
  // addition submenu items only shown when user is logged in
  // rendered before logout item, with separator.
  // children should be a list of reactstrap DropDownItem.
  children: PropTypes.node,
};

LoginNavItem.defaultProps = {
  login: {
    user: null,
    checking: false,
  },
  onLogin: null,
  onLogout: null,
  children: null,
  accountURL: null,
  showAccountURL: true,
};
