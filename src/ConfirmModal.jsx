import {
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";
import PropTypes from "prop-types";
import { faCancel, faCheck } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import colors from "./scss/colors.module.scss";
import "./ConfirmModal.scss";

/**
 * This is a reusable modal component for confirming user interactions
 *
 * Required props include a boolean state variable that inidicates if the modal is open
 * or close. A toggle function to inverse this state variable is also required.
 *
 * The modal also requires a title (string) and child element/s (message to the user
 * rendered inside the modal body.
 *
 * Two functions need to be passed in as well to handle user confirmation or cancellation
 * by handleCancel or handleConfirm props. Toggle modal are immediately called after the handle
 * functions.
 *
 * Custom text for confirm or cancel buttons can be passed in by the confirmElement or
 * cancelElement props.
 *
 * cancelButtonProps and confirmButtonProps can be passed through to the modal as well.
 * The format is { classes: "text string for classes", styles: {key value array}}
 *
 * modalProps prop is any other props that we want to pass in directly to the modal as per the
 * documentation from: https://reactstrap.github.io/iframe.html?id=components-modal--modal&viewMode=docs#props
 */

export function ConfirmModal({
  showConfirmModal,
  toggleModal,
  handleConfirm,
  handleCancel,
  title,
  confirmElement,
  confirmButtonProps,
  cancelElement,
  cancelButtonProps,
  children,
  modalProps,
}) {
  const cancel = () => {
    handleCancel();
    toggleModal();
  };

  const confirm = () => {
    handleConfirm();
    toggleModal();
  };

  // validate modal props func
  const validateModalProps = (propsParam) => {
    const cleanProps = propsParam;
    delete cleanProps.toggle;
    delete cleanProps.isOpen;

    return cleanProps;
  };

  // validates the users modal props to remove isOpen, toggle etc which
  // are handled seperately
  const newModalProps = validateModalProps(modalProps);

  return (
    <Modal isOpen={showConfirmModal} toggle={toggleModal} {...newModalProps}>
      <ModalHeader toggle={toggleModal}>
        {title}
      </ModalHeader>
      <ModalBody>
        {children}
      </ModalBody>
      <ModalFooter>
        <Button
          className={cancelButtonProps.classes}
          size="sm"
          onClick={cancel}
          style={cancelButtonProps.styles}
        >
          {cancelElement}
        </Button>
        <Button
          className={confirmButtonProps.classes}
          size="sm"
          onClick={confirm}
          style={confirmButtonProps.styles}
        >
          {confirmElement}
        </Button>
      </ModalFooter>
    </Modal>
  );
}

ConfirmModal.propTypes = {
  showConfirmModal: PropTypes.bool.isRequired,
  toggleModal: PropTypes.func.isRequired,
  handleConfirm: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  handleCancel: PropTypes.func.isRequired,
  confirmElement: PropTypes.element,
  confirmButtonProps: PropTypes.shape({
    classes: PropTypes.string,
    styles: PropTypes.objectOf(PropTypes.string),
  }),
  cancelElement: PropTypes.element,
  cancelButtonProps: PropTypes.shape({
    classes: PropTypes.string,
    styles: PropTypes.objectOf(PropTypes.string),
  }),
  children: PropTypes.node.isRequired,
  modalProps: PropTypes.shape(),
};

ConfirmModal.defaultProps = {
  modalProps: {},
  confirmElement:
  <>
    <FontAwesomeIcon size="sm" className="mx-1" icon={faCheck} />
    Confirm
  </>,
  cancelElement:
  <>
    <FontAwesomeIcon size="sm" className="mx-1" icon={faCancel} />
    Cancel
  </>,
  confirmButtonProps: {
    classes: "confirm-box-button",
    styles: { backgroundColor: colors.orangeDark, border: "none" },
  },
  cancelButtonProps: {
    classes: "confirm-box-button",
    styles: { backgroundColor: colors.orangeDark, border: "none" },
  },

};
