import PropTypes from "prop-types";
import { NavLink } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import {
  faFileExcel, faFileWord, faFilePdf, faFileCode, faLink,
} from "@fortawesome/free-solid-svg-icons";

import "./AccessLink.scss";

const linkTypeMap = {
  "WWW:LINK-1.0-http--link": "HTTP",
  "OGC:WMS-1.3.0-http-get-map": "WMS",
  "OGC:WCS-1.1.0-http-get-capabilities": "WCS",
  "OGC:WFS-1.1.0-http-get-capabilities": "WFS",
  "WWW:LINK-1.0-http--opendap": "OPeNDAP",
  "WWW:DOWNLOAD-1.0-http--download": "HTTP",
  orcid: "ORCID",
  organization: "ORG",
  person: "PER",
  https: "HTTP",
  ftp: "FTP",
};

// File type mapping
const fileIcons = {
  xlsx: faFileExcel,
  xls: faFileExcel,
  docx: faFileWord,
  doc: faFileWord,
  pdf: faFilePdf,
  json: faFileCode,
  csv: faFileCode,
  xml: faFileCode,
};

// Regex to extract file extension
const getFileType = (url) => {
  const match = url.match(/\.([a-zA-Z0-9]+)(?:\?|$)/);
  return match ? match[1].toLowerCase() : null;
};

const getConciseTitle = (url, title) => {
  if (!title || title.length < 10) return title;

  const conciseTitle = title || url;
  // Checks if the title starts with / or if no title exits return filename
  if ((conciseTitle.startsWith("/") || conciseTitle.startsWith("http")) || (!title && url)) {
    const fileName = conciseTitle.split("/").pop();
    return fileName;
  }

  return conciseTitle;
};

/**
 * AccessLink Component
 *
 * This component renders links with icons based on the link type (e.g., HTTP, PDF, DOCX, etc.).
 * It shortens long URLs or file paths in the title to make them more readable.
 * Works for both internal and external links, and handles click events for tracking.
 *
 * Be sure to add the appropriate NavLink tag for internal routing
 *
 */

export function AccessLink({
  url, title, isInternal, hideType, onClick, protocol, tag,
}) {
  const type = linkTypeMap[protocol] || null;
  const fileType = getFileType(url);
  const fileIcon = fileType && fileIcons[fileType] ? fileIcons[fileType] : faLink;
  const conciseTitle = getConciseTitle(url, title);

  // Navigate to external link
  const onLinkClick = () => {
    onClick();
  };

  if (!isInternal) {
    return (
      <a className="access-link pr-1" href={url} target="_blank" rel="noreferrer" onClick={onLinkClick}>
        {!hideType && type && <span className="link-type">{type}</span>}
        <span className="link-title">{conciseTitle || url}</span>
        <FontAwesomeIcon className="link-icon pl-1" icon={fileIcon} />
      </a>
    );
  }

  return (
    <NavLink className="access-link p-1" color="link" onClick={onLinkClick} tag={tag} to={`${url}`}>
      {!hideType && type && <span className="link-type">{type}</span>}
      <span className="link-title">{conciseTitle}</span>
    </NavLink>
  );
}

AccessLink.defaultProps = {
  title: null,
  onClick: () => null,
  isInternal: false,
  protocol: null,
  tag: "a",
  hideType: false,
};

AccessLink.propTypes = {
  url: PropTypes.string.isRequired,
  title: PropTypes.string,
  protocol: PropTypes.string,
  onClick: PropTypes.func,
  tag: PropTypes.node,
  isInternal: PropTypes.bool,
  hideType: PropTypes.bool,
};
