import { useState } from "react";
import PropTypes from "prop-types";

import {
  Container, Collapse,
  Navbar, NavbarBrand, NavbarText, NavbarToggler,
} from "reactstrap";
import { faBars } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { TernLogoFull } from "../icons/TernLogoFull.jsx";

import "./DesktopAppHeader.scss";

export function DesktopAppHeader({
  appTitle, appIcon, fluid, children,
}) {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <Container fluid={fluid} tag="header" className="main-header">
      <Navbar expand="sm">
        <NavbarBrand href="/">
          <TernLogoFull />
        </NavbarBrand>
        {appTitle && (
          <NavbarText className="app-title-container">
            <h3 className="app-title">
              {appIcon}
              {appTitle}
            </h3>
          </NavbarText>
        )}
        {children
          && (
            <>
              <NavbarToggler onClick={toggle}>
                <FontAwesomeIcon icon={faBars} />
              </NavbarToggler>
              <Collapse isOpen={isOpen} navbar className="justify-content-end">
                {children}
              </Collapse>
            </>
          )}
      </Navbar>
    </Container>
  );
}

DesktopAppHeader.propTypes = {
  appTitle: PropTypes.string,
  appIcon: PropTypes.node,
  children: PropTypes.node,
  fluid: PropTypes.bool,
};

DesktopAppHeader.defaultProps = {
  appTitle: null,
  appIcon: null,
  children: null,
  fluid: true,
};
