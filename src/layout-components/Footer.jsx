import PropTypes from "prop-types";
import { Col, Container, Row } from "reactstrap";
import {
  faFacebookF,
  faInstagram,
  faLinkedinIn,
  faXTwitter,
} from "@fortawesome/free-brands-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import orderBy from "lodash/orderBy.js";
import { getTernMenu, menuItemsType } from "../menu.js";

import "./Footer.scss";

import logoNCRIS from "../images/logo-ncris@2x.png";
import coinvestmentLockup from "../images/Coinvestment-lockup.jpg";
import uqLogoPurple from "../images/uq-logo-purple.png";
import jcuLogo from "../images/james-cook-logo@2x.png";
import csiroLogo from "../images/csiro-logo@2x.png";
import uaLogo from "../images/ua-logo@2x.png";
import coreTrustSealLogo from "../images/logo-CoreTrustSeal.png";

export function Footer({ resources, version, showAffiliation }) {
  return (
    <Container tag="footer" className="main-footer" fluid>
      <Container fluid="lg">
        <Row className="footer-section-wrap no-gutters">
          <Col className="footer-section footer-section-contact">
            <div className="footer-border">
              <h3>Contact us</h3>
              <p>
                <strong>Physical &amp; Mail Address</strong>
                <span>
                  <br />
                  The University of Queensland
                  <br />
                  Long Pocket Precinct
                  <br />
                  Level 5, Foxtail Building #1019
                  <br />
                  80 Meiers Road
                  <br />
                  Indooroopilly QLD 4068 Australia
                  <br />
                </span>
              </p>
              <p>
                <strong>General enquiries</strong>
                <br />
                <span>
                  P:
                  <a href="tel:+61733659097"> (07) 3365 9097</a>
                  <br />
                  <a href="mailto:tern@uq.edu.au"> tern@uq.edu.au</a>
                </span>
              </p>
              <p>
                <strong>Data Support</strong>
                <br />
                <span>
                  <a href="mailto:esupport@tern.org.au"> esupport@tern.org.au</a>
                </span>
              </p>
              <p>
                <a
                  href="https://www.tern.org.au/news/"
                  target="_blank"
                  rel="noreferrer"
                >
                  Subscribe
                </a>
                {" "}
                for project updates, data releases, research findings, and users
                stories direct to your inbox.
              </p>
              <div className="contact-social">
                <a
                  href="https://www.facebook.com/TerrestrialEcosystemResearchNetwork"
                  target="_blank"
                  rel="noreferrer"
                >
                  <FontAwesomeIcon icon={faFacebookF} />
                </a>
                <a
                  href="https://x.com/TERN_Aus"
                  target="_blank"
                  rel="noreferrer"
                >
                  <FontAwesomeIcon icon={faXTwitter} />
                </a>
                <a
                  href="https://www.instagram.com/tern_au/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <FontAwesomeIcon icon={faInstagram} />
                </a>
                <a
                  href="https://www.linkedin.com/groups/3981661/"
                  target="_blank"
                  rel="noreferrer"
                >
                  <FontAwesomeIcon icon={faLinkedinIn} />
                </a>
              </div>
            </div>
          </Col>
          <Col className="footer-section footer-section-funding">
            <div className="footer-border">
              <h3>Funding</h3>
              <p>
                TERN is supported by the Australian Government through the
                National Collaborative Research Infrastructure Strategy,
                {" "}
                <a
                  href="https://www.education.gov.au/national-collaborative-research-infrastructure-strategy-ncris"
                  target="_blank"
                  rel="noreferrer"
                >
                  NCRIS
                </a>
                .
              </p>
              <a
                href="https://www.education.gov.au/national-collaborative-research-infrastructure-strategy-ncris"
                target="_blank"
                rel="noreferrer"
              >
                <img src={logoNCRIS} alt="" width="276" height="200" />
              </a>
            </div>
          </Col>
          <Col
            className="footer-section footer-section-investment"
          >
            <div className="footer-border">
              <h3>Co-investment</h3>
              <img src={coinvestmentLockup} alt="" width="710" height="904" />
            </div>
          </Col>
          {showAffiliation && (
            <Col
              className="footer-section footer-section-affiliation"
            >
              <div className="footer-border">
                <h3>Accreditation</h3>
                <a
                  href="https://doi.org/10.34894/JP1GEA"
                  target="_blank"
                  rel="noreferrer"
                >
                  <img src={coreTrustSealLogo} alt="CoreTrustSeal" />
                </a>
              </div>
            </Col>
          )}
          <Col
            className="footer-section footer-section-resources"
          >
            <div className="footer-border">
              <h3>{resources.label}</h3>
              {orderBy(Object.values(resources.items).filter((x) => x), "order").map((item) => (
                <p key={item.label}>
                  <a href={item.href} target="_blank" rel="noreferrer">
                    {item.label}
                  </a>
                </p>
              ))}
            </div>
          </Col>
        </Row>
      </Container>
      <Row className="align-items-center footer-partners">
        <Col>Key Operating Partners</Col>
      </Row>
      <Row
        className="justify-content-center align-items-end footer-partners-list"
        xs="6"
      >
        <Col xs="auto">
          <img src={uqLogoPurple} alt="" width="169" height="47" />
        </Col>
        <Col xs="auto">
          <img src={jcuLogo} alt="" width="145" />
        </Col>
        <Col xs="auto">
          <img src={csiroLogo} alt="" width="55" />
        </Col>
        <Col xs="auto">
          <img src={uaLogo} alt="" width="145" />
        </Col>
      </Row>
      <Row xs="1" className="text-md-right version">
        <Col>{`Version:${version}`}</Col>
      </Row>
    </Container>
  );
}

Footer.propTypes = {
  // resources menu items
  resources: menuItemsType,
  // portal version number
  version: PropTypes.string,
  showAffiliation: PropTypes.bool,
};

Footer.defaultProps = {
  resources: getTernMenu().resources,
  version: "0.0.0",
  showAffiliation: true,
};
