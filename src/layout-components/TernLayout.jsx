import PropTypes from "prop-types";
import { Nav } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { filterChildElement, findChildElement } from "../util.js";
import { getTernMenu } from "../menu.js";
import { Footer } from "./Footer.jsx";
import { useTernLayout } from "../hooks/useTernLayout.js";
import { DesktopAppHeader } from "./DesktopAppHeader.jsx";
import { TopBar } from "../TopBar.jsx";
import { MobileTopBar } from "./MobileTopBar.jsx";
import { MobileAppHeader } from "./MobileAppHeader.jsx";
import { MobileNavbar } from "./MobileNavbar.jsx";

import "./TernLayout.scss";
import "../index.scss";

export function TernLayout({
  CONFIG,
  title,
  icon,
  versionString,
  children,
}) {
  const { isMobile } = useTernLayout();

  // Get Navbar component
  const navbar = findChildElement(Nav, children);
  const filteredChildren = filterChildElement(Nav, children);

  const version = versionString || CONFIG?.VERSION || "0.0.1";
  const menu = getTernMenu(CONFIG?.MENU);
  const showAffiliation = CONFIG?.SHOW_AFFILIATION || true;

  const appIcon = <FontAwesomeIcon icon={faMagnifyingGlass} />;

  // Return mobile layout
  if (isMobile) {
    return (
      <div className="tern-application-layout mobile">
        <MobileTopBar menuConfig={menu} />
        <MobileAppHeader appTitle={title} appIcon={icon || appIcon} />
        <div className="mobile-app-content">
          {filteredChildren}
        </div>
        <MobileNavbar>
          {navbar}
        </MobileNavbar>
      </div>
    );
  }

  // Return desktop view
  return (
    <div className="tern-application-layout desktop">
      <TopBar menuConfig={menu} />
      <DesktopAppHeader appTitle={title} appIcon={icon || appIcon}>
        {navbar}
      </DesktopAppHeader>
      <div className="desktop-app-content">
        {filteredChildren}
      </div>
      <Footer resources={menu.resources} version={version} showAffiliation={showAffiliation} />
    </div>
  );
}

TernLayout.propTypes = {
  CONFIG: PropTypes.shape().isRequired,
  title: PropTypes.string.isRequired,
  icon: PropTypes.node.isRequired,
  versionString: PropTypes.string,
  children: PropTypes.node.isRequired,
};

TernLayout.defaultProps = {
  versionString: null,
};
