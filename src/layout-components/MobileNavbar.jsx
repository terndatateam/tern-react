import PropTypes from "prop-types";

import "./MobileNavbar.scss";

export function MobileNavbar({ children }) {
  return (
    <div className="mobile-navbar">
      {children}
    </div>
  );
}

MobileNavbar.propTypes = {
  // resources menu items
  children: PropTypes.node.isRequired,
};

MobileNavbar.defaultProps = {
};
