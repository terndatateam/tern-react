import { useState } from "react";
import PropTypes from "prop-types";
import {
  Container, Dropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from "reactstrap";
import orderBy from "lodash/orderBy.js";
import { faInfoCircle, faSearch } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { getTernMenu, menuConfigType } from "../menu.js";

import "./MobileTopBar.scss";
import { TernIcon } from "../icons/TernIcon.jsx";
import { Sidebar } from "../modal/Sidebar.jsx";
import { Footer } from "./Footer.jsx";

export function MobileTopBar({ menuConfig, home }) {
  const [footerOpen, setFooterOpen] = useState(false);
  const toggle = () => {
    setFooterOpen(!footerOpen);
  };
  return (
    <>
      <Container className="mobile-above-header" fluid>
        <div className="above-header-menu">
          {orderBy(Object.values(menuConfig).filter((x) => x), "order").map(
            (item) => (
              <div key={item.label}><TopBarDropdown label={item.label} items={item.items} /></div>
            ),
          )}
        </div>
        <div className="above-header-links">
          <div className="link-tern-home">
            <a href={home} target="_blank" rel="noreferrer">
              <TernIcon />
            </a>
          </div>
          <div className="link-data-portal">
            <a href={menuConfig.tools.items.tddp.href} target="_blank" rel="noreferrer">
              <FontAwesomeIcon icon={faSearch} />
            </a>
          </div>
          <div className="link-data-info">
            <button type="button" onClick={toggle}>
              <FontAwesomeIcon icon={faInfoCircle} />
            </button>
          </div>
        </div>
      </Container>
      <Sidebar isOpen={footerOpen} toggle={toggle}>
        <Footer />
      </Sidebar>
    </>

  );
}

MobileTopBar.propTypes = {
  menuConfig: menuConfigType,
  home: PropTypes.string,
};

MobileTopBar.defaultProps = {
  menuConfig: getTernMenu(),
  home: "https://www.tern.org.au",
};

function TopBarDropdown({ label, items }) {
  const [open, setOpen] = useState(false);

  const toggle = () => setOpen(!open);

  return (
    <Dropdown
      isOpen={open}
      toggle={toggle}
    >
      <DropdownToggle caret tag="span">{label}</DropdownToggle>
      <DropdownMenu>
        {orderBy(Object.values(items).filter((x) => x), ["order"]).map(
          (item) => (
            <DropdownItem key={item.label} tag="a" href={item.href} target="_blank" rel="noopener noreferrer">
              {item.label}
            </DropdownItem>
          ),
        )}
      </DropdownMenu>
    </Dropdown>
  );
}

TopBarDropdown.propTypes = {
  label: PropTypes.string.isRequired,
  items: PropTypes.PropTypes.shape({}).isRequired,
};
