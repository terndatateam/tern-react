import PropTypes from "prop-types";

import { Container, Row } from "reactstrap";

import "./Banner.scss";

const colorClassMap = {
  "orange-mid": "content-title-orange-mid",
  "green-mid": "content-title-green-mid",
  "green-light": "content-title-green-light",
};

export function Banner({ color, title, children }) {
  return (
    <Container fluid>
      <Row className={`content-title ${colorClassMap[color]}`}>
        <Container className="content-title-container">
          {children || <h1>{title}</h1>}
        </Container>
      </Row>
    </Container>
  );
}

Banner.propTypes = {
  children: PropTypes.node,
  color: PropTypes.oneOf(
    ["orange-mid", "green-mid", "green-light"],
  ),
  title: PropTypes.string,
};

Banner.defaultProps = {
  title: null,
  children: null,
  color: "orange-mid",
};
