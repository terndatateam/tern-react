import { UncontrolledAlert } from "reactstrap";
import PropTypes from "prop-types";
import "./WarningBanner.scss";

/**
 * A warning banner component that displays warning or alert messages conditionally.
 *
 * @component
 * @param {Object} props
 * @param {boolean} props.visible - Controls whether the banner is shown or hidden
 * @param {string} [props.message="Warning: This site is experimental..."] - The message to display in the banner
 * @param {string} [props.color="warning"] - The color theme of the banner (uses reactstrap colors)
 * @param {string} [props.className=""] - Additional CSS classes to apply to the banner
 *
 */
export function WarningBanner({
  visible,
  message,
  color = "warning",
  className = "",
}) {
  if (!visible) return null;

  return (
    <UncontrolledAlert color={color} className={`bar-alert ${className}`}>
      <p>{message}</p>
    </UncontrolledAlert>
  );
}

WarningBanner.propTypes = {
  visible: PropTypes.bool.isRequired,
  message: PropTypes.string,
  color: PropTypes.string,
  className: PropTypes.string,
};

WarningBanner.defaultProps = {
  color: "warning",
  message: "Warning: This site is experimental, please do not use data for research purposes.",
  className: "",
};
