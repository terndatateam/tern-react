import PropTypes from "prop-types";
import { TernLogo } from "../icons/TernLogo.jsx";

import "./MobileAppHeader.scss";

export function MobileAppHeader({
  appTitle, appIcon,
}) {
  return (
    <div className="mobile-app-header">
      <div className="icon">
        <TernLogo />
      </div>
      <h6 className="app-title">
        {appIcon}
        {appTitle}
      </h6>
    </div>
  );
}

MobileAppHeader.propTypes = {
  appTitle: PropTypes.string,
  appIcon: PropTypes.node,
  // children: PropTypes.node,
};

MobileAppHeader.defaultProps = {
  appTitle: null,
  appIcon: null,
  // children: null,
};
