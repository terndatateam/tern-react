const path = require("path");

module.exports = {
  core: {
    builder: "webpack5",
  },
  stories: ["../stories/**/*.jsx"],
  // Add any Storybook addons you want here: https://storybook.js.org/addons/
  addons: [
    "@storybook/addon-essentials",
    "storybook-addon-jsx",
    "@whitespace/storybook-addon-html",
  ],
  webpackFinal: async (config) => {
    config.module.rules.push({
      test: /\.scss$/,
      use: ["style-loader", "css-loader", "sass-loader"],
      include: path.resolve(__dirname, "../"),
    });

    return config;
  },
};
