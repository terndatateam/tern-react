// eslint rule overrides
module.exports = {
  root: true,
  env: {
    es6: true,
    es2020: true,
  },
  parserOptions: {
    sourceType: "module",
    ecmaFeatures: {
      jsx: true,
    },
  },
  extends: [
    "eslint:recommended",
    "plugin:react/recommended",
    "react-app",
    "airbnb",
    "airbnb/hooks",
    "plugin:jsx-a11y/recommended",
    "plugin:storybook/recommended",
  ],
  rules: {
    "max-len": ["error", { code: 120 }],
    quotes: ["warn", "double"],
    "import/prefer-default-export": "off",
    "import/no-default-export": "error",
    "import/extensions": ["error", "ignorePackages"],
    // no longer needed with react >= 17
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "react/jsx-props-no-spreading": "off",
    "import/no-extraneous-dependencies": [
      "off",
      {
        devDependencies: [
          "**/*.test.ts",
          "**/*.test.tsx",
        ],
      },
    ],
  },
};
