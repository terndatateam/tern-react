import { render, fireEvent } from "@testing-library/react";
import { expect, vitest } from "vitest";
import { DateRangeInput } from "../../src/inputs/DateRangeInput.jsx";

/**
 * TODO: Requires further testing in tern-react
 */
describe("DateInput Component", () => {
  it("renders with basic props", () => {
    const { queryByText } = render(
      <DateRangeInput
        name="test"
      />,
    );

    // check for label and placeholder
    const startLabel = queryByText("Start Date");
    const endLabel = queryByText("End Date");

    expect(startLabel).not.toBeNull();
    expect(endLabel).not.toBeNull();
  });

  it("calls exposed onEvent handlers when slider is dragged", async () => {
    const onChangeMock = vitest.fn();

    const { container } = render(
      <DateRangeInput
        name="select"
        onChange={onChangeMock}
      />,
    );

    // get the first slider input
    const dateFromSlider = container.querySelector(".rc-slider-handle-1");
    const initialPos = dateFromSlider.getBoundingClientRect();

    // Handle is pressed, dragged to the right, and released at the new postion
    fireEvent.mouseDown(dateFromSlider, { clientX: initialPos.x });
    fireEvent.mouseMove(dateFromSlider, { clientX: initialPos.x + 50 });
    fireEvent.mouseUp(dateFromSlider, { clientX: initialPos.x + 50 });

    expect(onChangeMock).toHaveBeenCalledTimes(1);
  });
});
