import { render, fireEvent, screen } from "@testing-library/react";
import { expect, vitest } from "vitest";
import { DropdownInput } from "../../src/inputs/DropdownInput.jsx";

describe("DropdownInput", () => {
  it("renders with basic props", () => {
    render(
      <DropdownInput
        name="test-dropdown"
        label="Test Dropdown"
        description="Dropdown description"
        inputKey="dropdown-1"
      >
        <option value="option1">Option 1</option>
        <option value="option2">Option 2</option>
        <option value="option3">Option 3</option>
      </DropdownInput>,
    );

    // Check if the label is rendered
    const labelElement = screen.getByLabelText("Test Dropdown");
    expect(labelElement).toBeInTheDocument();

    // Check if dropdown options are rendered
    expect(screen.getByText("Option 1")).toBeInTheDocument();
    expect(screen.getByText("Option 2")).toBeInTheDocument();
    expect(screen.getByText("Option 3")).toBeInTheDocument();
  });

  it("calls onChange when an option is selected", () => {
    const onChangeMock = vitest.fn();

    render(
      <DropdownInput
        name="test-dropdown"
        label="Test Dropdown"
        inputKey="dropdown-1"
        onChange={onChangeMock}
      >
        <option value="option1">Option 1</option>
        <option value="option2">Option 2</option>
        <option value="option3">Option 3</option>
      </DropdownInput>,
    );

    const selectElement = screen.getByLabelText("Test Dropdown");

    fireEvent.change(selectElement, { target: { value: "option2" } });

    expect(onChangeMock).toHaveBeenCalledTimes(1);
    expect(onChangeMock).toHaveBeenCalledWith(expect.any(Object)); // Or with specific value if passed directly
  });

  it("renders tooltip on hover", async () => {
    const tooltipText = "Select an option";

    render(
      <DropdownInput
        name="test-dropdown"
        label="Test Dropdown"
        tooltipText={tooltipText}
        tooltipPlacement="top"
      >
        <option value="option1">Option 1</option>
      </DropdownInput>,
    );

    const icon = screen.getByRole("img", { hidden: true });
    expect(icon).toBeInTheDocument();

    expect(screen.queryByText(tooltipText)).not.toBeInTheDocument();
    fireEvent.mouseEnter(icon);

    const tooltip = await screen.findByText(tooltipText);
    expect(tooltip).toBeInTheDocument();

    fireEvent.mouseLeave(icon);

    await screen.findByText(tooltipText, { hidden: true });
  });
});
