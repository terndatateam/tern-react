import { render, fireEvent, waitFor } from "@testing-library/react";
import { expect, vitest } from "vitest";
import { TextInput } from "../../src/inputs/TextInput.jsx";

describe("TextInput Component", () => {
  it("renders with basic props", () => {
    const { getByLabelText, getByPlaceholderText } = render(
      <TextInput
        name="test"
        label="Test Input"
        value=""
        placeholder="Enter your answer"
      />,
    );

    // check for label and placeholder
    const inputElement = getByLabelText("Test Input");
    expect(inputElement).toBeInTheDocument();

    const placeholder = getByPlaceholderText("Enter your answer");
    expect(placeholder).toBeInTheDocument();
  });

  it("calls exposed onEvent handlers when text is entered", () => {
    const onChangeMock = vitest.fn();
    const onBlurMock = vitest.fn();

    const { getByLabelText } = render(
      <TextInput
        name="username"
        label="Username"
        value=""
        onChange={onChangeMock}
        onBlur={onBlurMock}
        placeholder="Enter your username"
      />,
    );

    const value = "testuser";
    const inputElement = getByLabelText("Username");

    // Simulate typing each character individually
    for (let i = 0; i < value.length; i += 1) {
      fireEvent.input(inputElement, { target: { value: value.slice(0, i + 1) } });
    }

    fireEvent.blur(inputElement);

    // check blur is called on blur
    expect(onBlurMock).toHaveBeenCalledTimes(1);

    // check onChange has been called the same number of times and the final value is correct
    expect(onChangeMock).toHaveBeenCalledTimes(value.length);

    expect(onChangeMock.mock.calls[value.length - 1][0].selection.value).toBe("testuser");
  });

  it("returns value of input on submit", () => {
    const submitElement = <span data-testid="submit-test"> Go </span>;

    const onSubmitMock = vitest.fn();

    const { getByLabelText, getByTestId } = render(
      <TextInput
        name="form-value"
        label="Testing"
        value=""
        placeholder="Enter your username"
        onSubmit={onSubmitMock}
        submitElement={submitElement}
      />,
    );

    const inputElement = getByLabelText("Testing");
    const submit = getByTestId("submit-test");

    fireEvent.input(inputElement, { target: { value: "value here" } });
    fireEvent.click(submit);

    // check onSubmit has been called the same number of times and the final value is correct
    expect(onSubmitMock).toHaveBeenCalledTimes(1);
    expect(onSubmitMock.mock.calls[0][0].selection.value).toBe("value here");
  });

  it("returns tooltip", async () => {
    const tooltipText = "Tooltip text here";

    const { getByRole, queryByText, findByText } = render(
      <TextInput
        name="form-value"
        label="Test"
        value=""
        tooltipText={tooltipText}
      />,
    );

    const icon = getByRole("img", { hidden: true });
    expect(icon).toBeInTheDocument();

    expect(queryByText(tooltipText)).not.toBeInTheDocument();
    fireEvent.mouseEnter(icon);

    const tooltip = await findByText(tooltipText);
    expect(tooltip).toBeInTheDocument();

    fireEvent.mouseLeave(icon);

    await waitFor(() => {
      expect(queryByText(tooltipText)).not.toBeInTheDocument();
    });
  });
});
