import { render, fireEvent, waitFor } from "@testing-library/react";
import { expect, vitest } from "vitest";
import { SwitchInput } from "../../src/inputs/SwitchInput.jsx";

describe("SwitchInput", () => {
  it("renders with basic props", () => {
    const { getByLabelText } = render(
      <SwitchInput
        name="test-switch"
        label="Test Switch"
        description="Test switch description"
        inputKey="switch-1"
      />,
    );

    // Check if the label is rendered
    const labelElement = getByLabelText("Test Switch");
    expect(labelElement).toBeInTheDocument();
  });

  it("renders with defaultValue", () => {
    const { getByLabelText } = render(
      <SwitchInput
        name="test-switch"
        label="Test Switch"
        defaultValue
      />,
    );

    const switchElement = getByLabelText("Test Switch");
    expect(switchElement).toHaveClass("switch-input active");
  });

  it("toggles switch state and calls onChange", () => {
    const onChangeMock = vitest.fn();

    const { getByLabelText } = render(
      <SwitchInput
        name="test-switch"
        label="Test Switch"
        defaultValue={false}
        onChange={onChangeMock}
      />,
    );

    const switchElement = getByLabelText("Test Switch");

    // Initially, the switch should be "unactive"
    expect(switchElement).toHaveClass("switch-input unactive");

    // Simulate a toggle (click) event
    fireEvent.click(switchElement);

    // Switch should now be active
    expect(switchElement).toHaveClass("switch-input active");

    // onChange should be called
    expect(onChangeMock).toHaveBeenCalledTimes(1);
    expect(onChangeMock).toHaveBeenCalledWith({ name: "test-switch", selection: true });
  });

  it("renders tooltip on hover", async () => {
    const tooltipText = "Toggle the switch";

    const { getByRole, queryByText, findByText } = render(
      <SwitchInput
        name="test-switch"
        label="Test Switch"
        tooltipText={tooltipText}
        tooltipPlacement="right"
      />,
    );

    const icon = getByRole("img", { hidden: true });
    expect(icon).toBeInTheDocument();

    expect(queryByText(tooltipText)).not.toBeInTheDocument();
    fireEvent.mouseEnter(icon);

    const tooltip = await findByText(tooltipText);
    expect(tooltip).toBeInTheDocument();

    fireEvent.mouseLeave(icon);

    await waitFor(() => {
      expect(queryByText(tooltipText)).not.toBeInTheDocument();
    });
  });

  // TODO: Not really tested, hard to control from component as well, not critical
  // it("renders icons based on switch state", () => {
  //   const { getByLabelText, rerender } = render(
  //     <SwitchInput
  //       name="test-switch"
  //       label="Test Switch"
  //       iconOn={<span>IconOn</span>}
  //       iconOff={<span>IconOff</span>}
  //       defaultValue={false}
  //     />,
  //   );

  //   // Initially, it should render the off icon
  //   let iconOff = getByLabelText("Test Switch").querySelector("span");
  //   expect(iconOff).toHaveTextContent("IconOff");

  //   // Toggle the switch
  //   fireEvent.click(getByLabelText("Test Switch"));

  //   // Re-render the component with the switch toggled on
  //   rerender(
  //     <SwitchInput
  //       name="test-switch"
  //       label="Test Switch"
  //       iconOn={<span>IconOn</span>}
  //       iconOff={<span>IconOff</span>}
  //       defaultValue={true}
  //     />,
  //   );

  //   // Now, it should render the on icon
  //   const iconOn = getByLabelText("Test Switch").querySelector("span");
  //   expect(iconOn).toHaveTextContent("IconOn");
  // });
});
