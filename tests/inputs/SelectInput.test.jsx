import { render, fireEvent, waitFor } from "@testing-library/react";
import { expect, vitest } from "vitest";
import { SelectInput } from "../../src/inputs/SelectInput.jsx";

describe("SelectDropdown Component", () => {
  it("renders with basic props", () => {
    const { getByLabelText } = render(
      <SelectInput
        name="test-select"
        label="Test Select"
        options={[{ value: "1", label: "Option 1" }, { value: "2", label: "Option 2" }]}
        inputKey="dropdown-1"
        description="Test dropdown description"
      />,
    );

    // Check that the label is rendered
    const labelElement = getByLabelText("Test Select");
    expect(labelElement).toBeInTheDocument();
  });

  it("calls onChange when an option is selected", () => {
    const onChangeMock = vitest.fn();

    const { getByLabelText, getByText } = render(
      <SelectInput
        name="test-select"
        label="Test Select"
        placeholder="Select an option"
        options={[{ value: "1", label: "Option 1" }, { value: "2", label: "Option 2" }]}
        onChange={onChangeMock}
      />,
    );

    // Simulate selecting an option
    const selectElement = getByLabelText("Test Select");
    fireEvent.focus(selectElement);
    fireEvent.keyDown(selectElement, { key: "ArrowDown", code: 40 });
    fireEvent.click(getByText("Option 1"));

    // Expect onChange to be called
    expect(onChangeMock).toHaveBeenCalledTimes(1);
    expect(onChangeMock).toHaveBeenCalledWith({
      name: "test-select", selection: { value: "1", label: "Option 1" },
    });
  });

  it("calls onBlur when the dropdown is blurred", () => {
    const onBlurMock = vitest.fn();

    const { getByLabelText } = render(
      <SelectInput
        name="test-select"
        label="Test Select"
        placeholder="Select an option"
        options={[{ value: "1", label: "Option 1" }, { value: "2", label: "Option 2" }]}
        onBlur={onBlurMock}
      />,
    );

    const selectElement = getByLabelText("Test Select");
    fireEvent.blur(selectElement);

    // Expect onBlur to be called once
    expect(onBlurMock).toHaveBeenCalledTimes(1);
  });

  it("renders a tooltip when hovering over the info icon", async () => {
    const tooltipText = "Tooltip for select";

    const { getByRole, queryByText, findByText } = render(
      <SelectInput
        name="test-select"
        label="Test Select"
        tooltipText={tooltipText}
        tooltipPlacement="right"
        options={[{ value: "1", label: "Option 1" }, { value: "2", label: "Option 2" }]}
      />,
    );

    const icon = getByRole("img", { hidden: true });
    expect(icon).toBeInTheDocument();

    expect(queryByText(tooltipText)).not.toBeInTheDocument();
    fireEvent.mouseEnter(icon);

    const tooltip = await findByText(tooltipText);
    expect(tooltip).toBeInTheDocument();

    fireEvent.mouseLeave(icon);

    await waitFor(() => {
      expect(queryByText(tooltipText)).not.toBeInTheDocument();
    });
  });
});
