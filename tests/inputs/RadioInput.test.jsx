import {
  render,
  fireEvent,
  waitFor,
  screen,
} from "@testing-library/react";
import { expect, vitest } from "vitest";
import { RadioInput } from "../../src/inputs/RadioInput.jsx";

describe("RadioInput Component", () => {
  it("renders with basic props", () => {
    const options = [
      { label: "Yes", value: "yes" },
      { label: "No", value: "no" },
    ];

    const { queryByText } = render(
      <RadioInput
        name="test"
        label="Test Input"
        description="This is a description"
        options={options}
      />,
    );

    // check for label and placeholder
    const label = queryByText("Test Input");
    expect(label).not.toBeNull();

    const description = queryByText("This is a description");
    expect(description).not.toBeNull();

    const yesOption = queryByText("Yes");
    expect(yesOption).not.toBeNull();
  });

  it("calls exposed onEvent handlers when input is entered", async () => {
    const onChangeMock = vitest.fn();

    const options = [
      { label: "Yes", value: "yes" },
      { label: "No", value: "no" },
    ];

    const { container } = render(
      <RadioInput
        name="select"
        label="Select options"
        onChange={onChangeMock}
        options={options}
      />,
    );

    // get the first input which will be Yes
    const inputElement = container.querySelector(".form-check-input");
    fireEvent.click(inputElement);

    await waitFor(() => {
      // Check if the onChange handler was called with the first option
      expect(onChangeMock).toHaveBeenCalledTimes(1);
      expect(onChangeMock.mock.calls[0][0].selection.value).toBe("yes");
    });
  });
  it("returns an array of values for checkbox", async () => {
    const onChangeMock = vitest.fn();

    const options = [
      { label: "Yes", value: "yes" },
      { label: "No", value: "no" },
    ];

    render(
      <RadioInput
        name="select"
        label="Select options"
        type="checkbox"
        onChange={onChangeMock}
        options={options}
      />,
    );

    // Get all checkboxes
    const yesCheckbox = screen.getByLabelText("Yes");
    const noCheckbox = screen.getByLabelText("No");

    // click the first checkbox (Yes)
    fireEvent.click(yesCheckbox);

    await waitFor(() => {
      // Verify that onChange was called once
      expect(onChangeMock).toHaveBeenCalledTimes(1);

      // expect an array with the value selected
      expect(onChangeMock.mock.calls[0][0].selection).toEqual([
        { label: "Yes", value: "yes" },
      ]);
    });

    // click the second check box with no
    fireEvent.click(noCheckbox);

    await waitFor(() => {
      // should be called twice
      expect(onChangeMock).toHaveBeenCalledTimes(2);

      // expect the array to have multiple items
      expect(onChangeMock.mock.calls[1][0].selection).toEqual([
        { label: "Yes", value: "yes" },
        { label: "No", value: "no" },
      ]);
    });

    // click first option to deselect
    fireEvent.click(yesCheckbox);

    await waitFor(() => {
      // should be called again
      expect(onChangeMock).toHaveBeenCalledTimes(3);

      // expect only one item in the array
      expect(onChangeMock.mock.calls[2][0].selection).toEqual([
        { label: "No", value: "no" },
      ]);
    });
  });
});
