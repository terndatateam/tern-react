import { vi } from "vitest";
import { render, fireEvent } from "@testing-library/react";
import { renderHook } from "@testing-library/react-hooks";
import * as ResizeObserverModule from "resize-observer-polyfill";
import { Map } from "../src/map/Map.jsx";

// needed for map component
import { useTernMap } from "../src/map/useTernMaps.js";
import { UsageGuide, UsageGuideBody, UsageGuideHeader } from "../src/map/UsageGuide.jsx";

// TODO: Mocking problematic react-leaflet-cluster to prevent syntax error
vi.mock("../node_modules/react-leaflet-cluster/lib/index.js", () => ({
  // Return empty obj to prevent SyntaxError
}));

(global).ResizeObserver = ResizeObserverModule.default;

// Returns a component that already contain all decorators from story level, meta level and global level.
test("map render success", () => {
  // default hook init
  const { result } = renderHook(() => useTernMap());
  render(<Map initMap={result.current} />);

  // check for map container
  const mapContainer = document.getElementsByClassName("map-container");
  expect(mapContainer).not.toBeNull();

  // check usage guide is not present (default)
  const usageGuide = document.getElementsByClassName("usage-guide")[0];
  expect(usageGuide).toBeFalsy();

  // menu button should render
  const menu = document.getElementsByClassName("menu-button")[0];
  expect(menu).not.toBeNull();
});

test("map with drawing disabled", () => {
  const customConfig = { ENABLE_DRAWING: false };
  const { result } = renderHook(() => useTernMap(customConfig));

  render(<Map initMap={result.current} />);

  // test hook is false
  const { enableDraw } = result.current;
  expect(enableDraw).toBeFalsy();

  // check draw tools is not present
  const drawTools = document.getElementsByClassName("draw-tools")[0];
  expect(drawTools).toBeFalsy();
});

test("map with search disabled but drawing enabled with only square tool", () => {
  const customConfig = { ENABLE_DRAWING: ["rectangle"], ENABLE_GEOSEARCH: false };

  const { result } = renderHook(() => useTernMap(customConfig));
  render(<Map initMap={result.current} />);

  // check search tool is not present
  const searchTools = document.getElementsByClassName("search-tool")[0];
  expect(searchTools).toBeFalsy();

  // check only square tool is the only other button there
  // toggle draw tools first
  const drawToolsButton = document.getElementsByClassName("map-tools-draw")[0];
  fireEvent.click(drawToolsButton);
  const squareDrawTool = document.getElementsByClassName("draw-rectangle")[0];
  expect(squareDrawTool).not.toBeFalsy();
});

// TODO: Unit test does not work, child elements seems to

test("map with usage guide", async () => {
  const { result } = renderHook(() => useTernMap());

  render(
    <Map
      initMap={result.current}
      data-testid="map-container"
      width="1000px"
      height="500px"
    >
      <UsageGuide modalSize="md">
        <UsageGuideHeader>EcoPlots Usage Guide</UsageGuideHeader>
        <UsageGuideBody data-testid="custom-usage-body">
          Body text
        </UsageGuideBody>
      </UsageGuide>
    </Map>,
  );

  // check usage guide button is present
  const usageGuide = document.getElementsByClassName("usage-guide-tool")[0];
  expect(usageGuide).not.toBeFalsy();

  const childElements = usageGuide.querySelectorAll("button");
  expect(childElements.length).toBe(1);

  // open usage
  fireEvent.click(childElements[0]);

  // check text of body
  const usageGuideBody = document.getElementsByClassName("usage-modal-body")[0];
  expect(usageGuideBody).not.toBeFalsy();

  const bodyText = usageGuideBody.textContent;
  expect(bodyText).toContain("Body text");
});
