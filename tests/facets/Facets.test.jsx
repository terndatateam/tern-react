import { render, fireEvent, screen } from "@testing-library/react";
import { renderHook, act } from "@testing-library/react-hooks";
import { expect } from "vitest";
import { useState } from "react";
import { Facet } from "../../src/facets/Facet.jsx";
import { Facets } from "../../src/facets/Facets.jsx";
import { TextInput } from "../../src/inputs/TextInput.jsx";
import { SelectInput } from "../../src/inputs/SelectInput.jsx";

function useFacetsTestWrapper(initialValues = {}) {
  const [values, setValues] = useState(initialValues);
  return { values, setValues };
}

describe("Facets Component", () => {
  it("updates values when input changes", () => {
    const { result } = renderHook(() => useFacetsTestWrapper());

    render(
      <Facets values={result.current.values} update={result.current.setValues} updateOn="onChange">
        <Facet name="Text Facet">
          <TextInput name="question" placeholder="Type something" />
        </Facet>
      </Facets>,
    );

    const input = screen.getByPlaceholderText("Type something");

    act(() => {
      fireEvent.change(input, { target: { value: "New Value" } });
    });

    expect(result.current.values.question.value).toBe("New Value");
  });

  it("handles multi-select input changes", () => {
    const { result } = renderHook(() => useFacetsTestWrapper());

    render(
      <Facets values={result.current.values} update={result.current.setValues} updateOn="onChange">
        <Facet name="Select Facet">
          <SelectInput
            name="questiontwo"
            options={[
              { label: "Yes", value: "1" },
              { label: "No", value: "0" },
            ]}
            placeholder="Select an option"
            selectProps={{ isMulti: true }}
          />
        </Facet>
      </Facets>,
    );

    // Open the dropdown
    const placeholder = screen.getByText("Select an option");
    fireEvent.mouseDown(placeholder);

    // Select an option
    const option = screen.getByText("Yes");
    act(() => {
      fireEvent.click(option);
    });

    // Assert the selected value
    expect(result.current.values.questiontwo[0].value).toEqual("1");
  });
});
