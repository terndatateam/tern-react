import { render } from "@testing-library/react";
// import userEvent from "@testing-library/user-event";
import { BlockUi } from "../src/loading/BlockUi.jsx";

describe("Component: BlockUi", () => {
  it("should be rendered", () => {
    const { container } = render(
      <BlockUi blocking={false}>
        <p>child</p>
      </BlockUi>,
    );

    const blockui = container.querySelector("#block-ui-container");
    expect(blockui).toBeInTheDocument();
  });

  it("should block and render children", async () => {
    const { container, getByRole } = render(

      <BlockUi keepInView blocking>
        <button type="button">child</button>
      </BlockUi>,

    );

    // Children should be rendered
    expect(getByRole("button")).toBeInTheDocument();

    // overlay should be in the dom
    const overlay = container.querySelector("#block-ui-overlay");
    expect(overlay).toBeInTheDocument();
    expect(overlay).toHaveStyle({ position: "absolute" });
    expect(overlay).toHaveStyle({ zIndex: 1040 });
    expect(overlay).toHaveStyle({ cursor: "wait" });

    // spinner should be in the dom
    expect(container.querySelector("#block-ui-spinner")).toBeInTheDocument();
  });

  it("should not block and should render children", async () => {
    const { container, getByRole } = render(
      <BlockUi keepInView blocking={false} tag="div">
        <button type="button">child</button>
      </BlockUi>,

    );

    // Children should be rendered
    expect(getByRole("button")).toBeInTheDocument();

    // overlay should not be in the dom
    expect(container.querySelector("#block-ui-overlay")).not.toBeInTheDocument();

    // spinner should not be in the dom
    expect(container.querySelector("#block-ui-spinner")).not.toBeInTheDocument();
  });
});
