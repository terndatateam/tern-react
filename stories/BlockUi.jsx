import "../src/loading/BlockUi.scss";
import { useState } from "react";
import { BlockUi } from "../src/loading/BlockUi.jsx";

export default {
  title: "components/BlockUi",
  component: BlockUi,
  args: {
    keepInView: true,
    tag: "div",
    children: null,
  },
};

export function Default({ tag, ...args }) {
  const [blocking, setBlocking] = useState(true);
  const [blockedCount, setBlockedCount] = useState(0);
  const [unblockedCount, setUnblockedCount] = useState(0);

  const btnStyles = {
    display: "inline-block",
    padding: "12px 25px",
    "font-size": "16px",
    cursor: "pointer",
    "text-align": "center",
    "text-decoration": "none",
    outline: "none",
    color: "#fff",
    "background-color": "#006381",
    border: "none",
    "border-radius": "10px",
    "margin-bottom": "5px",
  };
  return (
    <>
      <button
        style={btnStyles}
        type="button"
        onClick={() => setBlocking((prev) => !prev)}
      >
        {blocking ? "Unblock" : "Block"}
      </button>
      <div style={{
        padding: "2rem",
        backgroundColor: "steelblue",
        color: "white",
      }}
      >
        <BlockUi {...args} blocking={blocking} tag={tag || undefined}>
          <div>
            <h2>
              {" "}
              { blocking ? "Section is Blocked" : "Section can be blocked"}
            </h2>
            <p>
              This content is a child of BlockUi component and
              {blocking ? " is now blocked" : " can be blocked" }
              .
            </p>
            <p>Buttons and other HTML elements are not accessible when the component is blocking.</p>
            <button
              type="button"
              onClick={() => setBlockedCount((prev) => prev + 1)}
              style={{
                backgroundColor: "#006381", borderRadius: "10px", color: "#fff", marginRight: "5px",
              }}
            >
              Click Me
              {" "}
              <span>{blockedCount}</span>
            </button>
            <button
              type="button"
              onClick={() => setBlockedCount(0)}
              style={{ backgroundColor: "#ed694b", borderRadius: "10px", color: "#fff" }}
            >
              Reset
            </button>
          </div>
        </BlockUi>
        <div style={{ paddingTop: "1rem" }}>
          <h2>Section Not Blocked</h2>
          <p>This content is not a child of BlockUi component and will not be blocked.</p>
          <button
            type="button"
            onClick={() => setUnblockedCount((prev) => prev + 1)}
            style={{
              backgroundColor: "#006381", borderRadius: "10px", color: "#fff", marginRight: "5px",
            }}
          >
            Click Me
            {" "}
            <span>{unblockedCount}</span>
          </button>
          <button
            type="button"
            onClick={() => setUnblockedCount(0)}
            style={{ backgroundColor: "#ed694b", borderRadius: "10px", color: "#fff" }}
          >
            Reset
          </button>
        </div>
      </div>
    </>
  );
}
