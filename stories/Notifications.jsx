import { Container, Button, ButtonGroup } from "reactstrap";
import { Notifications } from "../src/notification/Notifications.jsx";
import { useNotifications } from "../src/hooks/useNotification.js";

export default {
  title: "components/Notifications",
  component: Notifications,
  argTypes: {
    isDismissable: { control: { type: "boolean", defaultValue: false } },
    timer: { control: { type: "boolean", defaultValue: true } },
    duration: { control: { type: "number", defaultValue: 500 } },
  },
};

export function Default(args) {
  const { notifications, pushNotification, closeNotification } = useNotifications();

  // define some push notifications functions
  const pushSuccessMessage = () => {
    pushNotification("Success", "Good job on pressing that button.", "success");
  };

  const pushErrorMessage = () => {
    pushNotification("Whoops", "You're not meant to press this.", "error");
  };

  const pushMessage = () => {
    pushNotification("Message", "This is a very informative message");
  };

  return (
    <Container>
      <h4>
        View the JSX to see the usage of the notifcations hook.
      </h4>
      <ButtonGroup>
        <Button color="success" onClick={pushSuccessMessage}>
          Success
        </Button>
        <Button color="danger" onClick={pushErrorMessage}>
          Don&apos;t press this button.
        </Button>
        <Button onClick={pushMessage}>
          Message
        </Button>
      </ButtonGroup>

      <Notifications
        notifications={notifications}
        isDismissable={args.isDismissable}
        closeNotification={closeNotification}
        timer={args.timer}
        duration={args.duration}
      />
    </Container>
  );
}
