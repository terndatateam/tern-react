import { useState } from "react";
import { Button, Container, Row } from "reactstrap";
import { AccessLink } from "../src/AccessLink.jsx";

export default {
  title: "components/AccessLink",
  component: AccessLink,
  args: {
    title: "Default Link Title",
    url: "https://example.com",
    isInternal: false,
    hideType: false,
  },
};

export function Default(args) {
  const [userInput, setUserInput] = useState(null);

  const handleLinkClick = () => {
    setUserInput("Link clicked!");
  };

  return (
    <Container>
      <AccessLink
        {...args}
        onClick={handleLinkClick}
      />
      <Row>
        {userInput && `User Input: ${userInput}`}
      </Row>
    </Container>
  );
}

// Custom External Link Example
export function ExternalLinkExample() {
  const [userInput, setUserInput] = useState(null);

  const handleLinkClick = () => {
    setUserInput("External Link Clicked!");
  };

  return (
    <Container>
      <AccessLink
        url="https://externalwebsite.com"
        title="Go to External Website"
        isInternal={false}
        onClick={handleLinkClick}
      />
      {userInput}
    </Container>
  );
}

// Custom Internal Link Example
export function InternalLinkExample() {
  const [userInput, setUserInput] = useState(null);

  const handleLinkClick = () => {
    setUserInput("Internal Link Clicked!");
  };

  return (
    <Container>
      <AccessLink
        url="home/about"
        title="Internal About Page"
        isInternal
        onClick={handleLinkClick}
      />
      <Row>
        <Button onClick={handleLinkClick}>Action</Button>
        {userInput && `User Input: ${userInput}`}
      </Row>
    </Container>
  );
}

export function MultipleFileTypes() {
  return (
    <div>
      <AccessLink
        url="https://example.com/sample.pdf"
        title="/example.com/sample.pdf"
        isInternal={false}
      />
      <AccessLink
        url="/example.com/sample.docx"
        title="Open Word Document"
        isInternal={false}
      />
      <AccessLink
        url="https://example.com/sample.txt"
        title="Text File"
        isInternal={false}
      />
    </div>
  );
}
