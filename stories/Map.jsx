import { Container, Button } from "reactstrap";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faMapPin } from "@fortawesome/free-solid-svg-icons";
import MarkerClusterGroup from "react-leaflet-cluster";
import { Map } from "../src/map/Map.jsx";
import { ClusterMarker } from "../src/map/markers/ClusterMarker.jsx";
import { LocationMarker } from "../src/map/markers/LocationMarker.jsx";
import { useTernMap } from "../src/map/useTernMaps.js";
import { Legend, Legends } from "../src/map/legend/Legends.jsx";
import { MarkerPopup } from "../src/map/markers/MarkerPopup.jsx";
import { Polygon } from "../src/map/markers/Polygon.jsx";
import { UsageGuide, UsageGuideBody, UsageGuideHeader } from "../src/map/UsageGuide.jsx";
import mulitpolygonData from "./mulipolygonData.json";

export default {
  title: "map/Map",
  component: Map,
  argTypes: {
  },
};

export function Default(args) {
  const map = useTernMap();
  return (
    <Container>
      <Map initMap={map} toolbarSize="md" />
    </Container>
  );
}

export function ClusterMap() {
  // disable drawing
  const map = useTernMap({ ENABLE_DRAWING: ["rectangle"], ENABLE_GEOSEARCH: false });

  const clusters = {
    r1: {
      location: {
        lat: -36.19690003339201,
        lon: 140.44742996804416,
      },
      num_sites: 23992,
    },
    r4: {
      location: {
        lat: -36.19690003339201,
        lon: 140.4444299683442,
      },
      num_sites: 999,
    },
    r7: {
      location: {
        lat: -25.31170003116131,
        lon: 149.89449994638562,
      },
      num_sites: 12,
    },
    rh: {
      location: {
        lat: -19.688400006853044,
        lon: 141.53649995103478,
      },
      num_sites: 4361,
    },
    qf: {
      location: {
        lat: -30.945685033220798,
        lon: 129.4818099634722,
      },
      num_sites: 6,
    },
    r5: {
      location: {
        lat: -25.314875012263656,
        lon: 140.62313498463482,
      },
      num_sites: 100,
    },
  };

  const clusterKeys = Object.keys(clusters);

  const getClassBySitesNumber = (num) => {
    if (num !== undefined) {
      switch (true) {
        case num <= 10:
          return "green";
        case num <= 100:
          return "yellow";
        case num <= 1000:
          return "orange";
        default:
          return "red";
      }
    }
    return "green";
  };

  // queensland polygon coordinates
  const polygonCoordinates = [
    [-26.005, 140.995],
    [-26.005, 137.995],
    [-10, 137.995],
    [-10, 154],
    [-28.18, 154],
    [-28.42423, 152.93848],
    [-28.34843, 152.4824],
    [-28.5503, 152.07915],
    [-28.93346, 152.11644],
    [-29.23319, 151.3544],
    [-28.76766, 150.9082],
    [-28.67633, 149.55433],
    [-29.04144, 148.97512],
    [-29, 140.995],
  ];

  // create function for cluster marker group with font awesome icon
  const createClusterIcon = () => LocationMarker({
    customIconClass: "cluster-icon",
    options: { iconSize: 30 },
    icon: <FontAwesomeIcon icon={faMapPin} />,
  });

  const mulitpolygon = mulitpolygonData.features[0].geometry.coordinates;
  const mulitpolygonCoordinates = mulitpolygon.map((polygon) => polygon[0].map((coord) => [coord[1], coord[0]]));

  return (
    <Container>
      <Map initMap={map} scaleControlPosition="bottomleft" hasLayerSwitch>

        {/** Legends component */}
        <Legends>
          <Legend shape="square" color="#e36a51"> TERN EcoSystem Processes site</Legend>
          <Legend shape="circle" color="#065f65"> Ecological site</Legend>
          <Legend shape="circle" color="#ff2cab"> Revisited site</Legend>
          <Legend shape="circle" color="#46ff65"> Cluster (&gt;10 sites)</Legend>
          <Legend shape="circle" color="#fffc5c"> Cluster (&gt;100 sites)</Legend>
          <Legend shape="circle" color="#ff8066"> Cluster (&gt;1000 sites)</Legend>
          <Legend shape="circle" color="#ff4b4b"> Cluster (&gt;=1000 sites)</Legend>
        </Legends>
        <MarkerClusterGroup
          iconCreateFunction={createClusterIcon}
        >
          {clusterKeys.map((clusterKey) => {
            const cluster = clusters[clusterKey];
            return (
              <ClusterMarker
                key={clusterKey}
                number={cluster.num_sites}
                color={getClassBySitesNumber(cluster.num_sites)}
                location={cluster.location}
                events={{
                  mouseover: (e) => {
                    e.target.openPopup();
                  },
                }}
              >
                <MarkerPopup>
                  <div>Adding popup</div>
                </MarkerPopup>
              </ClusterMarker>
            );
          })}

        </MarkerClusterGroup>

        <Polygon positions={polygonCoordinates} />

        {/* mulitpolygon */}
        <Polygon positions={mulitpolygonCoordinates} />

      </Map>
    </Container>
  );
}

export function MarkerMap() {
  const map = useTernMap();

  const sites = [
    {
      dataset: "http://linked.data.gov.au/dataset/bdbsa",
      date_commissioned: null,
      feature_type: "http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",
      latitude: "-28.98042",
      longitude: "135.16556",
      site_id: "http://linked.data.gov.au/dataset/bdbsa/site-rr11801-12388",
      site_visit_count: "1",
      site_visits: [
        {
          site_visit_id: "http://linked.data.gov.au/dataset/bdbsa/sv-12297",
          visit_end_date: {
            type: "dateTime",
            value: "1995-02-02T23:00:00",
          },
          visit_start_date: {
            type: "dateTime",
            value: "1995-02-02T23:00:00",
          },
        },
      ],
    },
    {
      dataset: "http://linked.data.gov.au/dataset/bdbsa",
      date_commissioned: null,
      feature_type: "http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",
      latitude: "-28.84645",
      longitude: "132.53812",
      site_id: "http://linked.data.gov.au/dataset/bdbsa/site-tal00101-19214",
      site_visit_count: "1",
      site_visits: [
        {
          site_visit_id: "http://linked.data.gov.au/dataset/bdbsa/sv-19970",
          visit_end_date: {
            type: "dateTime",
            value: "2003-04-08T00:00:00",
          },
          visit_start_date: {
            type: "dateTime",
            value: "2003-04-08T00:00:00",
          },
        },
      ],
    },
    {
      dataset: "http://linked.data.gov.au/dataset/bdbsa",
      date_commissioned: null,
      feature_type: "http://linked.data.gov.au/def/tern-cv/5bf7ae21-a454-440b-bdd7-f2fe982d8de4",
      latitude: "-0.0",
      longitude: "-0.0",
      site_id: "http://linked.data.gov.au/dataset/bdbsa/site-c20425601-35893",
      site_visit_count: "1",
      site_visits: [
        {
          site_visit_id: "http://linked.data.gov.au/dataset/bdbsa/sv-29596",
          visit_end_date: {
            type: "dateTime",
            value: "1976-09-20T00:00:00",
          },
          visit_start_date: {
            type: "dateTime",
            value: "1976-09-20T00:00:00",
          },

        },
        {
          site_visit_id: "http://linked.data.gov.au/dataset/bdbsa/sv-29596",
          visit_end_date: {
            type: "dateTime",
            value: "1976-09-20T00:00:00",
          },
          visit_start_date: {
            type: "dateTime",
            value: "1976-09-20T00:00:00",
          },

        },
      ],
    },
  ];

  return (
    <Container>
      <Map
        initMap={map}
        zoomToolsOnly
        scaleControlPosition="bottomleft"
      >
        {sites.map((site) => (
          <LocationMarker
            key={site.site_id}
            customIconClass={site.site_visits.length > 1 && "custom-icon"}
            location={{ lat: Number(site.latitude), lon: Number(site.longitude) }}
          />
        ))}
      </Map>
    </Container>
  );
}

export function ExternalInteraction() {
  const map = useTernMap();

  const { setBounds } = map;

  // const onDeleteShape = () => {
  //   console.log('shape deleted!');
  // }

  // const onCreateShape = (shape) => {
  //   console.log('shape created', shape);
  // }

  const exampleBounds = {
    type: "Polygon",
    coordinates: [
      [
        [
          96.3102611562355,
          -6.615864243073614,
        ],
        [
          158.45831809645944,
          -6.615864243073614,
        ],
        [
          158.45831809645944,
          -35.245264110058066,
        ],
        [
          96.3102611562355,
          -35.245264110058066,
        ],
        [
          96.3102611562355,
          -6.615864243073614,
        ],
      ],
    ],
  };

  const [region, setRegion] = useState(null);

  return (

    <Container>
      <Button onClick={() => setRegion(166)}>
        Set Feature ID
        {` ${166}`}
        {" "}
        for WMS Layer to highlight Tassie
      </Button>
      <Button className="ml-2" onClick={() => setBounds(exampleBounds)}>
        Set Bounds Externally
      </Button>
      <Map
        initMap={map}
        width="1000px"
        height="500px"
        featureId={region}
        scaleControlPosition="bottomleft"
        hasLayerSwitch
        // onCreateShape={onCreateShape}
        // onDeleteShape={onDeleteShape}
      >
        <UsageGuide modalSize="lg">
          <UsageGuideHeader>Custom Header</UsageGuideHeader>
          <UsageGuideBody>
            <p>
              The Maps search feature allows for location searches to be done visually. In
              the toolbox, click one for the selection tools then draw a selection area on the map. If
              needed, use the edit tool to change the selection area or delete the selection to start again.
              When edits are completed, click the save button to confirm the changes.
              <br />
              <br />
              <br />
              Further information is available in the Maps search feature
              <a
                href="https://ternaus.atlassian.net/wiki/spaces/TERNSup/pages/2677735428/Using+the+map+view+in+EcoPlots"
                target="_blank"
                rel="noopener noreferrer"
                style={{ color: "#5c9494" }}
              >
                <strong>{" eSupport article"}</strong>
                .
              </a>
            </p>
          </UsageGuideBody>
        </UsageGuide>
      </Map>

    </Container>

  );
}
