/** Override config specific for story books. */
module.exports = {
  root: false,
  rules: {
    "import/no-default-export": "off",
    "import/no-extraneous-dependencies": ["error", { devDependencies: true }],
    "no-unused-vars": ["error", { argsIgnorePattern: "args" }],
    "react/destructuring-assignment": ["off"],
    "react/jsx-props-no-spreading": ["error", { custom: "ignore" }],
    "react/prop-types": ["error", { skipUndeclared: true }],
  },
};
