import { Container, Button } from "reactstrap";
import { useState } from "react";
import { DateRangeInput } from "../../src/inputs/DateRangeInput.jsx";

export default {
  title: "Inputs/DateRangeInput",
  component: DateRangeInput,
  argTypes: {
    onChange: { action: "changed" },
    name: { control: "text" },
  },
};

function Template(args) {
  const [[max, min], setMaxMin] = useState(["2032-10-01", "1788-01-01"]);
  const [[defaultTo, defaultFrom], setDefault] = useState([null, null]);

  const onChange = () => {
    setMaxMin(["2012-02-01", "1970-01-01"]);
  };

  const onChangeDefault = () => {
    setDefault(["2010-04-01", "2000-01-01"]);
  };

  return (
    <Container>
      <Button onClick={onChange}>Change range</Button>
      <Button onClick={onChangeDefault}>Change default</Button>
      <DateRangeInput maxDate={max} minDate={min} defaultValue={{ to: defaultTo, from: defaultFrom }} />
    </Container>
  );
}

export const Default = Template.bind({});

Default.args = {
  name: "example",
};
