import { RadioInput } from "../../src/inputs/RadioInput.jsx";

export default {
  title: "Inputs/RadioInput",
  component: RadioInput,
  argTypes: {
    onChange: { action: "changed" },
    inputKey: { control: "text" },
    name: { control: "text" },
    options: {
      control: "object",
      defaultValue: [
        { label: "Option 1", value: "1" },
        { label: "Option 2", value: "2" },
      ],
    },
    description: { control: "text" },
    label: { control: "text" },
    value: {
      control: "object",
      defaultValue: { label: "Option 1", value: "1" },
    },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "left", "right", "bottom"],
      },
    },
    tooltipText: { control: "text" },
    type: {
      control: {
        type: "select",
        options: ["radio", "checkbox"],
      },
    },
  },
};

function Template(args) {
  return <RadioInput {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  name: "example",
  label: "Example Label",
  description: "This is a description",
  tooltipPlacement: "top",
  tooltipText: "Tooltip text",
  type: "radio",
  options: [
    { label: "Option 1", value: "1" },
    { label: "Option 2", value: "2" },
  ],
  value: { label: "Option 1", value: "1" },
};
