import { DropdownInput } from "../../src/inputs/DropdownInput.jsx";

export default {
  title: "Inputs/DropdownInput",
  component: DropdownInput,
  argTypes: {
    onChange: { action: "changed" },
    name: { control: "text" },
    label: { control: "text" },
    description: { control: "text" },
    tooltipText: { control: "text" },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "left", "right", "bottom"],
      },
    },
  },
};

function Template(args) {
  return (
    <DropdownInput {...args}>
      <option value="option1">Option 1</option>
      <option value="option2">Option 2</option>
      <option value="option3">Option 3</option>
    </DropdownInput>
  );
}

export const Default = Template.bind({});
Default.args = {
  name: "example-dropdown",
  label: "Example Dropdown",
  description: "Choose an option",
  tooltipText: "Tooltip text",
  tooltipPlacement: "top",
};
