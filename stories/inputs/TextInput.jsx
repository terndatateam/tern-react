import { faMagnifyingGlass } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { TextInput } from "../../src/inputs/TextInput.jsx";

export default {
  title: "Inputs/TextInput",
  component: TextInput,
  argTypes: {
    onChange: { action: "changed" },
    onBlur: { action: "blurred" },
    name: { control: "text" },
    description: { control: "text" },
    value: { control: "text" },
    inputKey: { control: "text" },
    label: { control: "text" },
    defaultValue: { control: "text" },
    icon: { control: "element" },
    placeholder: { control: "text" },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "left", "right", "bottom"],
      },
    },
    tooltipText: { control: "text" },
    inputProps: { control: "object" },
  },
};

function Template(args) {
  return <TextInput {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  name: "example",
  label: "Example Label",
  placeholder: "Enter text...",
  description: "This is a description",
  tooltipPlacement: "top",
  tooltipText: "Tooltip text",
};

export const TextInputWithSubmitElement = Template.bind({});
TextInputWithSubmitElement.args = {
  name: "example",
  label: "Example Label",
  placeholder: "Enter text...",
  description: "This is a description",
  tooltipPlacement: "top",
  tooltipText: "Tooltip text",
  submitElement: <FontAwesomeIcon icon={faMagnifyingGlass} />,
};
