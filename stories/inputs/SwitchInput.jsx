import { SwitchInput } from "../../src/inputs/SwitchInput.jsx";

export default {
  title: "Inputs/SwitchInput",
  component: SwitchInput,
  argTypes: {
    onChange: { action: "changed" },
    name: { control: "text" },
    description: { control: "text" },
    tooltipText: { control: "text" },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "left", "right", "bottom"],
      },
    },
    iconOn: { control: "text" },
    iconOff: { control: "text" },
    defaultValue: { control: "boolean" },
  },
};

function Template(args) {
  return <SwitchInput {...args} />;
}

export const Default = Template.bind({});
Default.args = {
  name: "example-switch",
  label: "Example Switch",
  description: "This is a toggle switch",
  defaultValue: false,
  tooltipText: "Toggle switch",
  tooltipPlacement: "top",
};
