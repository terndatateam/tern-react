import { useState } from "react";
import { SelectInput } from "../../src/inputs/SelectInput.jsx";

export default {
  title: "Inputs/SelectInput",
  component: SelectInput,
  argTypes: {
    onChange: { action: "changed" },
    onBlur: { action: "blurred" },
    name: { control: "text" },
    description: { control: "text" },
    tooltipText: { control: "text" },
    tooltipPlacement: {
      control: {
        type: "select",
        options: ["top", "left", "right", "bottom"],
      },
    },
    options: { control: "object" },
    values: { control: "array" },
    defaultValue: { control: "object" },
    placeholder: { control: "text" },
    selectProps: { control: "object" },
    italicised: { control: "boolean" },
    casing: { control: "text" },
  },
};

function Template(args) {
  const [value, setValue] = useState(null);

  const onChange = ({ selection }) => {
    setValue(selection);
  };

  return (
    <SelectInput {...args} values={value} onChange={onChange} casing="title" />
  );
}

export const Default = Template.bind({});
Default.args = {
  name: "example-select",
  label: "Example Select",
  description: "This is a select dropdown",
  options: [
    { value: "1", label: "Option 1" },
    { value: "2", label: "option one and really cool 2" },
    { value: "3", label: "Asteraceae Bercht. & J.Presl" },
  ],
  defaultValue: { value: "2", label: "Option 2" },
  casing: "title",
  italicised: false,
  speciesFormat: false,
  placeholder: "Select an option...",
  tooltipText: "Select an option from the dropdown",
  tooltipPlacement: "top",
};
