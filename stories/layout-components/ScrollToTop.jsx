import { Container, Row } from "reactstrap";
import { ScrollToTop } from "../../src/ScrollToTop.jsx";

export default {
  title: "layout components/ScrollToTop",
  component: ScrollToTop,
  argTypes: {
    message: { control: { type: "text", defaultValue: "Back To Top" } },
  },
};

export function Default(args) {
  return (
    <Container style={{ height: "110%" }}>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <Row>Row</Row>
      <ScrollToTop message={args.message} />
    </Container>
  );
}
