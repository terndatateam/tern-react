import { WarningBanner } from "../../src/layout-components/WarningBanner.jsx";
import "./AlertBanner.scss";

export default {
  title: "layout components/WarningBanner",
  component: WarningBanner,
  parameters: {
    docs: {
      description: {
        component: "The `WarningBanner` component displays important messages to the user."
        + "It can be used to show warnings, errors, or informational messages conditionally.",
      },
    },
  },
};

export function Default(args) {
  const content = (
    <div>
      <WarningBanner
        visible={args.CONFIG.MENU.env === "test"}
      />
    </div>
  );
  return content;
}

export function ResolutionWarning(args) {
  const content = (
    <div>
      <WarningBanner
        visible={args.unsupportedWidth}
        message={
          "Warning: Screen width below minimum supported width detected. "
          + "The minimum recommended width for this site is 720px. "
          + "Some features might not work as intended."
        }
        className="width-warning"
      />
    </div>
  );
  return content;
}

export function CustomMessage(args) {
  const content = (
    <div>
      <WarningBanner
        visible={args.otherCondition}
        message="Custom Message"
        className="other-warning"
      />
    </div>
  );
  return content;
}

Default.args = {
  CONFIG: {
    MENU: {
      env: "test",
    },
  },
};

ResolutionWarning.args = {
  unsupportedWidth: true,
};

CustomMessage.args = {
  otherCondition: true,
};
