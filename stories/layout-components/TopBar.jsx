import "../../src/index.scss";
import { TopBar } from "../../src/TopBar.jsx";

import { getTernMenu } from "../../src/menu.js";

export default {
  title: "layout components/TopBar",
  args: {
    env: "test",
    overrides: {},
  },
};

export function Dev(args) {
  return <TopBar menuConfig={getTernMenu({ ...args })} />;
}

export function Prod(args) {
  return <TopBar menuConfig={getTernMenu({ ...args })} />;
}
Prod.args = {
  env: "prod",
};

export function Custom(args) {
  return <TopBar menuConfig={getTernMenu({ ...args })} />;
}
Custom.args = {
  env: "prod",
  overrides: {
    data: {
      label: "Data",
      items: {
        // add menu item
        extra1: { label: "Item1", href: "", order: 1 },
        // change title
        maps: { label: "MapViz" },
        // remove shared
        shared: null,
      },
      // reorder
      order: 5,
    },
  },
};
