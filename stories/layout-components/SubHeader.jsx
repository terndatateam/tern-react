import { faRightToBracket } from "@fortawesome/free-solid-svg-icons";
import { Button } from "reactstrap";
import { SubHeader } from "../../src/SubHeader.jsx";

export default {
  title: "layout components/SubHeader",
  component: SubHeader,
  args: {
    title: "Title",
    tagline: "Tagline text here",
    buttonText: "Access Page/site",
    buttonIcon: faRightToBracket,
    buttonTag: "a",
    route: "www.google.com",
    backgroundImage: "url(https://www.tern.org.au/wp-content/uploads/trees.jpg)",

  },
};

export function Default(args) {
  return (
    <SubHeader
      {...args}
    />
  );
}

export function DefaultWithChildren(args) {
  return (
    <SubHeader
      {...args}
    >
      <Button> Override button with children</Button>
    </SubHeader>
  );
}
