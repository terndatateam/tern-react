import { Row, Col } from "reactstrap";

import "../../src/index.scss";
import { Banner } from "../../src/layout-components/Banner.jsx";

export default {
  title: "layout components/Banner",
  component: Banner,
  args: {
    // color: "orange-mid",
    title: "Heading",
  },
};

export function Default(args) {
  return <Banner {...args} />;
}

export function WithContent(args) {
  return (
    <Banner {...args}>
      <Row>
        <Col xs="auto"><h1>Content Title</h1></Col>
        <Col xs="auto"><p>Some Content here</p></Col>
      </Row>
    </Banner>
  );
}
