import { useState } from "react";
import { Button } from "reactstrap";
import { Sidebar } from "../../src/modal/Sidebar.jsx";

export default {
  title: "layout components/Sidebar",
  component: Sidebar,
  argTypes: {
    direction: {
      control: { type: "radio" },
      options: ["left", "right"],
    },
  },
};

export function Default(args) {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);

  return (
    <>
      <Button color="primary" onClick={toggle}>
        Toggle Sidebar
      </Button>
      <Sidebar {...args} isOpen={isOpen} toggle={toggle}>
        <p>This is the default sidebar content</p>
      </Sidebar>
    </>
  );
}
