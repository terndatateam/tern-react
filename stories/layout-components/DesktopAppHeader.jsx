import {
  Nav, NavItem, NavLink,
  UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
} from "reactstrap";

import "../../src/index.scss";
import { DesktopAppHeader } from "../../src/layout-components/DesktopAppHeader.jsx";
import { LoginNavItem } from "../../src/LoginNavItem.jsx";

export default {
  title: "layout components/Desktop App Header",
  component: DesktopAppHeader,
  args: {
    fluid: false,
    title: null,
  },
  argTypes: {
    title: {
      control: false,
    },
  },
};

export function Default(args) {
  return <DesktopAppHeader {...args} />;
}

export function WithMenuLoggedOut({ fluid, title, login }) {
  return (
    <DesktopAppHeader fluid={fluid} appTitle={title}>
      <Nav navbar>
        <NavItem>
          <NavLink to="/" active>Home</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/login">Sign&nbsp;In</NavLink>
        </NavItem>
        <UncontrolledDropdown nav>
          <DropdownToggle nav caret>
            Options
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>Option 1</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Option 2</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <LoginNavItem login={login}>
          <DropdownItem>
            User Option 1
          </DropdownItem>
        </LoginNavItem>
      </Nav>
    </DesktopAppHeader>
  );
}

WithMenuLoggedOut.args = {
  login: {
    user: null,
    checking: false,
  },
};

export function WithMenuLoggedIn({ fluid, title, login }) {
  return (
    <DesktopAppHeader fluid={fluid} appTitle={title}>
      <Nav navbar>
        <NavItem>
          <NavLink to="/" active>Home</NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/login">Sign&nbsp;In</NavLink>
        </NavItem>
        <UncontrolledDropdown nav>
          <DropdownToggle nav caret>
            Options
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>Option 1</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Option 2</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        {/* <LoginNavItem login={{ checking: false, user: null }} /> */}
        {/* <LoginNavItem login={{ checking: false, user: { name: "Gerhard Weis" } }} /> */}
        <LoginNavItem login={login}>
          <DropdownItem>
            User Option 1
          </DropdownItem>
        </LoginNavItem>
      </Nav>
    </DesktopAppHeader>
  );
}

WithMenuLoggedIn.args = {
  login: {
    user: { name: "User Name" },
  },
};

export function WithTitle(args) {
  return <DesktopAppHeader {...args} />;
}

WithTitle.args = {
  title: <h3>App Title</h3>,
};
