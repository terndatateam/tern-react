import { useState } from "react";
import { Container, Row, Col } from "reactstrap";

import "../../src/index.scss";
import { SplitPane } from "../../src/SplitPane.jsx";

export default {
  title: "layout components/SplitPane",
  component: SplitPane,
  arg: {},
};

export function Default() {
  // initialsizes in relation
  const [savedState, setSavedState] = useState({ isCollapsed: true, initialSize: 500 });

  return (
    <Container style={{ height: "50vh" }}>
      <Row style={{ height: "100%" }}>
        <Col>
          <SplitPane
            minSize={200}
            maxSize={500}
            initialSize={savedState.initialSize}
            isCollapsed={savedState.isCollapsed}
            onChange={setSavedState}
          >
            <Row>
              <Col>
                <h1>Left bar</h1>
              </Col>
            </Row>

            <Row>
              <Col>
                <h1>Right bar</h1>
              </Col>
            </Row>

          </SplitPane>
        </Col>
      </Row>
    </Container>
  );
}
