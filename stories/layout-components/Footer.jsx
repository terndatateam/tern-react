import "../../src/index.scss";
import { Footer } from "../../src/layout-components/Footer.jsx";

import { getTernMenu } from "../../src/menu.js";

export default {
  title: "layout components/Footer",
  component: Footer,
  args: {
    env: "dev",
    overrides: {},
  },
};

export function Dev() {
  return <Footer version="1.0.0" />;
}

export function Prod(args) {
  return <Footer resources={getTernMenu({ ...args }).resources} version="2.0.0" />;
}

export function Custom(args) {
  return <Footer resources={getTernMenu({ ...args }).resources} version="2.0.0" />;
}
Custom.args = {
  env: "prod",
  overrides: {
    resources: {
      items: {
        // add menu item
        extra1: { label: "Item1", href: "", order: 1 },
        // change title
        dataLicensing: { label: "License" },
        // remove acknowledgements
        shared: null,
        // re-orrder copyRight
        copyRight: { order: 10 },
      },
    },
  },
};
