import { useState, useRef } from "react";
import {
  Nav, NavItem, NavLink,
  UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
  Button,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";

import { BottomSheetModal } from "../src/modal/BottomSheetModal.jsx";
import { TernLayout } from "../src/layout-components/TernLayout.jsx";
import { LoginNavItem } from "../src/LoginNavItem.jsx";

export default {
  title: "layout components/BottomSheetModal",
  component: BottomSheetModal,
  args: {
    fluid: false,
    title: null,
  },
  argTypes: {
    title: {
      control: false,
    },
  },
};

export function Default(args) {
  const [isOpen, setIsOpen] = useState(false);
  const toggle = () => setIsOpen(!isOpen);
  const login = { user: null, checking: false };

  const ref = useRef(null);

  return (

    <TernLayout {...args}>
      <Nav navbar>
        <NavItem>
          <NavLink to="/" active>
            <FontAwesomeIcon icon={faHome} />
            Home
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/">
            <FontAwesomeIcon icon={faHome} />
            Home
          </NavLink>
        </NavItem>
        <UncontrolledDropdown nav>
          <DropdownToggle nav caret>
            Options
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>Option 1</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Option 2</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <LoginNavItem login={login}>
          <DropdownItem>
            User Option 1
          </DropdownItem>
        </LoginNavItem>
      </Nav>
      <div
        style={{
          height: "70vh",
          backgroundColor: "red",
          position: "relative",
          overflow: "hidden",
        }}
        ref={ref}
      >
        <Button color="primary" onClick={toggle}>
          Toggle Bottom Sheet
        </Button>
        <BottomSheetModal
          isOpen={isOpen}
          parentRef={ref}
          toggle={toggle}
          swipeDownToClose={false}
          limitBoundsToBreakpoints
        >
          <p>This is the content inside the modal!</p>
        </BottomSheetModal>
      </div>
    </TernLayout>
  );
}
