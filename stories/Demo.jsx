import "../src/index.scss";

import { App } from "./components/App.jsx";
import { Home } from "./components/Home.jsx";
import { SignIn } from "./components/SignIn.jsx";

export default {
  title: "demo/Pages",
};

export function Welcome() {
  return (
    <App>
      <Home />
    </App>
  );
}

export function SignInPage() {
  return (
    <App appHeader={false}>
      <SignIn />
    </App>
  );
}
