import { Container } from "reactstrap";
import { useState } from "react";
import { Facet } from "../src/facets/Facet.jsx";
import { Facets } from "../src/facets/Facets.jsx";
import { TextInput } from "../src/inputs/TextInput.jsx";
import { SelectInput } from "../src/inputs/SelectInput.jsx";
import { DateRangeInput } from "../src/inputs/DateRangeInput.jsx";

export default {
  title: "components/Facets",
  component: Facets,
  args: {
    title: "Title Of Facets",
    updateOn: "onChange", // Can be "onBlur", "onChange", or "onSubmit"
    loading: false,
  },
  argTypes: {
    title: { control: { type: "text" } },
    updateOn: { control: { type: "select", options: ["onBlur", "onChange", "onSubmit"] } },
    loading: { control: "boolean" },
  },
};

export function Default(args) {
  const [values, setValues] = useState({});

  const onFacetChange = (e) => {
    console.log("Facet changed", e);
  };

  return (
    <Container>
      <Facets {...args} values={values} update={setValues} updateOn="onChange" onChange={onFacetChange}>
        <Facet name="Text Facet">
          <TextInput name="question" placeholder="Type something" />
        </Facet>
        <Facet name="Select Facet" value={values.questiontwo}>
          <SelectInput
            name="questiontwo"
            options={[
              { label: "Yes", value: "1" },
              { label: "No", value: "0" },
            ]}
            values={values.questiontwo}
            placeholder="Select an option"
            selectProps={{ isMulti: true }}
          />
        </Facet>
        <Facet name="Date range">
          <DateRangeInput name="daterange" minDate="2000-01-01" maxDate="2012-12-3" />
        </Facet>
      </Facets>
    </Container>
  );
}
