import { useState } from "react";
import {
  Table,
  Card,
  CardBody,
  CardHeader,
  Button,
} from "reactstrap";
import { faBook } from "@fortawesome/free-solid-svg-icons";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { PaginationBar } from "../src/pagination/PaginationBar.jsx";
import { ResultsInfo } from "../src/pagination/ResultsInfo.jsx";
import { SortBy } from "../src/pagination/SortBy.jsx";
import { DisplayCount } from "../src/pagination/DisplayCount.jsx";
import { PageInfo } from "../src/pagination/PageInfo.jsx";
import { Pagination } from "../src/pagination/Pagination.jsx";
import divider from "../src/images/divider.png";
import "./PaginationBar.scss";

// some dummy data for the table
// in reality, the data would get from redux store or an API call
const testData = [
  { name: "JJJ", date: "2004-10-01" },
  { name: "KKK", date: "2010-01-01" },
  { name: "LLL", date: "2003-02-01" },
  { name: "MMM", date: "2011-03-01" },
  { name: "NNN", date: "2007-04-01" },
  { name: "OOO", date: "2019-05-01" },
  { name: "PPP", date: "2004-06-01" },
  { name: "QQQ", date: "2016-07-01" },
  { name: "RRR", date: "2004-08-01" },
  { name: "SSS", date: "2021-09-01" },
  { name: "TTT", date: "2021-10-01" },
  { name: "AAA", date: "2022-01-01" },
  { name: "BBB", date: "2022-02-01" },
  { name: "CCC", date: "2023-03-01" },
  { name: "DDD", date: "2024-04-01" },
  { name: "EEE", date: "2023-05-01" },
  { name: "FFF", date: "2023-06-01" },
  { name: "GGG", date: "2024-07-01" },
  { name: "HHH", date: "2020-08-01" },
  { name: "III", date: "2021-09-01" },
];

export default {
  title: "components/PaginationBar",
  component: PaginationBar,
  args: {
    onSortChange: () => {},
    onDisplayCountChange: () => {},
    onPageChange: () => {},
    displayCount: 5,
    currentPage: 1,
    sortBy: "name",
    totalRecords: testData.length,
    sortOptions: [
      { name: "Name", value: "name" },
      { name: "Date", value: "date" },
    ],
  },
};

// function to sort name or date
function sortData(dataToSort, order) {
  const data = [...dataToSort];

  switch (order) {
    case "name":
      // sort name property alphabetically
      return data.sort((a, b) => (a.name > b.name ? 1 : -1));
    case "date":
      return data.sort((a, b) => (new Date(a.date) > new Date(b.date) ? 1 : -1));
    default:
      return dataToSort;
  }
}

export function Default(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOrder, setSortOrder] = useState("name");
  const [displayCount, setDisplayCount] = useState(5);

  const {
    totalRecords,
    sortOptions,
  } = args;

  const start = (currentPage - 1) * displayCount;
  const end = Number(start) + Number(displayCount);
  const sortedData = sortData(testData, sortOrder);
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onSortChange = (value) => {
    setSortOrder(value);
    setCurrentPage(1);
  };

  const onDisplayCountChange = (value) => {
    setDisplayCount(value);
    setCurrentPage(1);
  };

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <ResultsInfo
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <SortBy
            sortBy={sortOrder}
            onSortChange={onSortChange}
            sortOptions={sortOptions}
          />
          <img src={divider} alt="vertical divider" />
          <DisplayCount
            displayCount={displayCount}
            onDisplayCountChange={onDisplayCountChange}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <PageInfo
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}

export function WithExportButton(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOrder, setSortOrder] = useState("name");
  const [displayCount, setDisplayCount] = useState(5);

  const {
    totalRecords,
    sortOptions,
  } = args;

  const start = (currentPage - 1) * displayCount;
  const end = Number(start) + Number(displayCount);
  const sortedData = sortData(testData, sortOrder);
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onSortChange = (value) => {
    setSortOrder(value);
    setCurrentPage(1);
  };

  const onDisplayCountChange = (value) => {
    setDisplayCount(value);
    setCurrentPage(1);
  };

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3 with-export-button">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <ResultsInfo
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <SortBy
            sortBy={sortOrder}
            onSortChange={onSortChange}
            sortOptions={sortOptions}
          />
          <img src={divider} alt="vertical divider" />
          <DisplayCount
            displayCount={displayCount}
            onDisplayCountChange={onDisplayCountChange}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <Button
            // eslint-disable-next-line no-alert
            onClick={() => alert("Export button clicked")}
          >
            <FontAwesomeIcon icon={faBook} />
            {" "}
            Export
          </Button>
          <img src={divider} alt="vertical divider" />
          <PageInfo
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}

export function WithoutDividers(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOrder, setSortOrder] = useState("name");
  const [displayCount, setDisplayCount] = useState(5);

  const {
    totalRecords,
    sortOptions,
  } = args;

  const start = (currentPage - 1) * displayCount;
  const end = Number(start) + Number(displayCount);
  const sortedData = sortData(testData, sortOrder);
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onSortChange = (value) => {
    setSortOrder(value);
    setCurrentPage(1);
  };

  const onDisplayCountChange = (value) => {
    setDisplayCount(value);
    setCurrentPage(1);
  };

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <ResultsInfo
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <SortBy
            sortBy={sortOrder}
            onSortChange={onSortChange}
            sortOptions={sortOptions}
          />
          <DisplayCount
            displayCount={displayCount}
            onDisplayCountChange={onDisplayCountChange}
            totalRecords={totalRecords}
          />
          <PageInfo
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}

export function WithoutResultInfo(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOrder, setSortOrder] = useState("name");
  const [displayCount, setDisplayCount] = useState(5);

  const {
    totalRecords,
    sortOptions,
  } = args;

  const start = (currentPage - 1) * displayCount;
  const end = Number(start) + Number(displayCount);
  const sortedData = sortData(testData, sortOrder);
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onSortChange = (value) => {
    setSortOrder(value);
    setCurrentPage(1);
  };

  const onDisplayCountChange = (value) => {
    setDisplayCount(value);
    setCurrentPage(1);
  };

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <SortBy
            sortBy={sortOrder}
            onSortChange={onSortChange}
            sortOptions={sortOptions}
          />
          <img src={divider} alt="vertical divider" />
          <DisplayCount
            displayCount={displayCount}
            onDisplayCountChange={onDisplayCountChange}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <PageInfo
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}

export function WithoutResultAndPageInfo(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const [sortOrder, setSortOrder] = useState("name");
  const [displayCount, setDisplayCount] = useState(5);

  const {
    totalRecords,
    sortOptions,
  } = args;

  const start = (currentPage - 1) * displayCount;
  const end = Number(start) + Number(displayCount);
  const sortedData = sortData(testData, sortOrder);
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onSortChange = (value) => {
    setSortOrder(value);
    setCurrentPage(1);
  };

  const onDisplayCountChange = (value) => {
    setDisplayCount(value);
    setCurrentPage(1);
  };

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <SortBy
            sortBy={sortOrder}
            onSortChange={onSortChange}
            sortOptions={sortOptions}
          />
          <img src={divider} alt="vertical divider" />
          <DisplayCount
            displayCount={displayCount}
            onDisplayCountChange={onDisplayCountChange}
            totalRecords={totalRecords}
          />
          <img src={divider} alt="vertical divider" />
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={displayCount}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}

export function WithOnlyPagination(args) {
  const [currentPage, setCurrentPage] = useState(1);
  const {
    totalRecords,
  } = args;

  const start = (currentPage - 1) * 5;
  const end = Number(start) + 5;
  const sortedData = sortData(testData, "name");
  // initial paginated data with default settings
  const paginatedData = sortedData.slice(start, end);

  const onPageChange = (page) => {
    setCurrentPage(page);
  };

  return (
    <Card className="mb-3">
      <CardHeader>
        <h5>PaginationBar</h5>
      </CardHeader>
      <CardBody>
        <Table>
          <thead>
            <tr>
              <th>Name</th>
              <th>Date</th>
            </tr>
          </thead>
          <tbody>
            {paginatedData.map((data) => (
              <tr key={data.name}>
                <td>{data.name}</td>
                <td>{data.date}</td>
              </tr>
            ))}
          </tbody>
        </Table>
        <PaginationBar>
          <Pagination
            onPageChange={onPageChange}
            currentPage={currentPage}
            displayCount={5}
            totalRecords={totalRecords}
          />
        </PaginationBar>
      </CardBody>
    </Card>
  );
}
