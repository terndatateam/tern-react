import "../src/index.scss";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCheckCircle, faIcons } from "@fortawesome/free-solid-svg-icons";
import { Container } from "reactstrap";
import { HelpCard } from "../src/HelpCard.jsx";

export default {
  title: "components/HelpCard",
  component: HelpCard,
  // set defaults
  args: {
    code: "404",
    title: "Title of Card",
    message: <small>This is a React Element being passed in as a message.</small>,
    icon: <FontAwesomeIcon icon={faIcons} />,
  },
  // // configure controls
  argTypes: {
    code: { control: { defaultValue: "404" } },
    title: { control: { type: "text", defaultValue: "Enter Title" } },
    message: { control: { disable: true } },
    icon: { control: { disable: true } },
  },
};

export function Default(args) {
  return (
    <Container>
      <HelpCard
        title={args.title}
        message={args.message}
        icon={args.icon}
        code={args.code}
      />
    </Container>
  );
}

export function Success() {
  return (
    <Container>
      <HelpCard
        title="Success!"
        message={(
          <span>
            This is a message inside a
            <b> React Element</b>
            {" "}
            that details why the user was successful
          </span>
        )}
        icon={<FontAwesomeIcon color="green" icon={faCheckCircle} />}
      />
    </Container>
  );
}
