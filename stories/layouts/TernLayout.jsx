import {
  Nav, NavItem, NavLink,
  UncontrolledDropdown, DropdownToggle, DropdownMenu, DropdownItem,
  CardHeader,
  Container,
  Card,
  CardBody,
} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faHome } from "@fortawesome/free-solid-svg-icons";
import { TernLayout } from "../../src/layout-components/TernLayout.jsx";
import { LoginNavItem } from "../../src/LoginNavItem.jsx";

export default {
  title: "layout components/TERN Layout",
  component: TernLayout,
  args: {
    fluid: false,
    title: null,
  },
  argTypes: {
    title: {
      control: false,
    },
  },
};

export function Default(args) {
  const login = { user: null, checking: false };

  return (
    <TernLayout {...args}>
      <Nav navbar>
        <NavItem>
          <NavLink to="/" active>
            <FontAwesomeIcon icon={faHome} />
            Home
          </NavLink>
        </NavItem>
        <NavItem>
          <NavLink to="/">
            <FontAwesomeIcon icon={faHome} />
            Home
          </NavLink>
        </NavItem>
        <UncontrolledDropdown nav>
          <DropdownToggle nav caret>
            Options
          </DropdownToggle>
          <DropdownMenu>
            <DropdownItem>Option 1</DropdownItem>
            <DropdownItem divider />
            <DropdownItem>Option 2</DropdownItem>
          </DropdownMenu>
        </UncontrolledDropdown>
        <LoginNavItem login={login}>
          <DropdownItem>
            User Option 1
          </DropdownItem>
        </LoginNavItem>
      </Nav>
      <Container>
        <Card>
          <CardHeader>
            Header
          </CardHeader>
          <CardBody>
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla vitae orci vel
            ligula vulputate fringilla. Morbi vel dolor elit. Donec eu felis nec nulla
            vestibulum gravida. Nulla facilisi. Phasellus ultricies nulla sit amet ligula
            sagittis, nec cursus mi faucibus. Maecenas eget ex felis. Vestibulum volutpat
            augue ac auctor interdum.

          </CardBody>
        </Card>
      </Container>
    </TernLayout>
  );
}
