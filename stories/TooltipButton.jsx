import "../src/index.scss";
import { action } from "@storybook/addon-actions";
import { faUndoAlt } from "@fortawesome/free-solid-svg-icons";
import { TooltipButton } from "../src/TooltipButton.jsx";

export default {
  title: "components/TooltipButton",
  component: TooltipButton,
  argTypes: { onClick: { action: "clicked" } },
};

export function Go(args) {
  return <TooltipButton color="teal" onClick={action("Go")}>Go</TooltipButton>;
}

/**
 * Clearfilter button example
 *
 */
export function ClearFilter() {
  return (
    <TooltipButton color="link" onClick={action("ClearFilter")} icon={faUndoAlt}>Clear All Filter</TooltipButton>
  );
}

/**
 * Search has a tooltip
 *
 */
export function Search() {
  return (
    <TooltipButton color="danger" onClick={action("Search")} tooltip="Click to Search"> Search </TooltipButton>
  );
}

export function ViewMetaData() {
  return (
    <TooltipButton color="teal" onClick={action("ViewMetaData")}> View Metadata</TooltipButton>
  );
}
export function MapSearch() {
  return (
    <TooltipButton color="teal" onClick={action("MapSearch")}> Map Search</TooltipButton>
  );
}
export function AddToFavourites() {
  return (
    <TooltipButton color="danger" onClick={action("AddToFavourites")}> Add to Favourites </TooltipButton>
  );
}
