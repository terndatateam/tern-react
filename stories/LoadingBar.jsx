import { Container } from "reactstrap";
import { LoadingBar } from "../src/loading/LoadingBar.jsx";

export default {
  title: "components/LoadingBar",
  component: LoadingBar,
};

export function Default() {
  return (
    <Container>
      <LoadingBar />
    </Container>
  );
}
