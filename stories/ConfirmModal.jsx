import { useState } from "react";
import { Button, Container, Row } from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faPlay, faStop } from "@fortawesome/free-solid-svg-icons";
import { ConfirmModal } from "../src/ConfirmModal.jsx";

export default {
  title: "components/ConfirmModal",
  component: ConfirmModal,
  args: {
    title: "Title Of Modal",
  },
  // // configure controls
  argTypes: {
    title: { control: { type: "string" } },
  },
};

export function Default(args) {
  const [confirmModal, setConfirmModal] = useState(false);
  const toggleModal = () => setConfirmModal(!confirmModal);

  const [userInput, setUserInput] = useState(null);

  const handleConfirm = () => {
    setUserInput("Confirmed!");
    toggleModal();
  };

  const handleCancel = () => {
    setUserInput("Cancelled.");
    toggleModal();
  };

  return (
    <Container>
      <ConfirmModal
        title={args.title}
        showConfirmModal={confirmModal}
        toggleModal={toggleModal}
        handleCancel={handleCancel}
        handleConfirm={handleConfirm}
      >
        This is a confirmation modal with default buttons.
      </ConfirmModal>
      <Row>
        <Button onClick={toggleModal}>Action</Button>
        {userInput && `User Input: ${userInput}`}
      </Row>
    </Container>
  );
}

// Custom Confirm
function ConfirmElement() {
  return (
    <>
      <FontAwesomeIcon size="sm" className="mx-1" icon={faPlay} />
      {" "}
      Custom Confirm
    </>
  );
}

// Custom Cancel Element
function CancelElement() {
  return (
    <>
      <FontAwesomeIcon size="sm" className="mx-1" icon={faStop} />
      Custom Cancel
    </>
  );
}

export function PassCustomButtons(args) {
  const [confirmModal, setConfirmModal] = useState(false);
  const toggleModal = () => setConfirmModal(!confirmModal);

  const [userInput, setUserInput] = useState(null);

  const handleConfirm = () => {
    setUserInput("Confirmed!");
    toggleModal();
  };

  const handleCancel = () => {
    setUserInput("Cancelled.");
    toggleModal();
  };

  return (
    <Container>
      <ConfirmModal
        title="Title of Modal"
        showConfirmModal={confirmModal}
        toggleModal={toggleModal}
        handleCancel={handleCancel}
        handleConfirm={handleConfirm}
        confirmButtonProps={{ styles: { backgroundColor: "green", borderColor: "green" } }}
        cancelButtonProps={{ styles: { backgroundColor: "red", borderColor: "red" } }}
        cancelElement={<CancelElement />}
        confirmElement={<ConfirmElement />}

      >
        This is a confirmation modal with custom buttons.
      </ConfirmModal>
      <Row>
        <Button onClick={toggleModal}>Action</Button>
        {userInput && `User Input: ${userInput}`}
      </Row>
    </Container>
  );
}

export function PassModalProps() {
  const [confirmModal, setConfirmModal] = useState(false);
  const toggleModal = () => setConfirmModal(!confirmModal);

  const [userInput, setUserInput] = useState(null);

  const handleConfirm = () => {
    setUserInput("Confirmed!");
    toggleModal();
  };

  const handleCancel = () => {
    setUserInput("Cancelled.");
    toggleModal();
  };

  const modalProps = {
    fade: false,
    size: "lg",
  };

  return (
    <Container>
      <ConfirmModal
        title="Title of Modal"
        showConfirmModal={confirmModal}
        toggleModal={toggleModal}
        handleCancel={handleCancel}
        handleConfirm={handleConfirm}
        modalProps={modalProps}
      >
        This is a confirmation modal with additional props passed directly into modal.
      </ConfirmModal>
      <Row>
        <Button onClick={toggleModal}>Action</Button>
        {userInput && `User Input: ${userInput}`}
      </Row>
    </Container>
  );
}
