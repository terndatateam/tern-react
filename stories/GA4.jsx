import "../src/index.scss";
// include AppHeader scss to set nav item colors
import "./LginNavItem.scss";
import { GA4 } from "../src/GA4.jsx";

export default {
  title: "components/GA4",
  component: GA4,
  args: {
    trackingId: null,
  },
  // argTypes: {},
};

export function Default(args) {
  return (
    <GA4
      trackingId={args.trackingId}
    />
  );
}
