import React from "react";
import "../src/index.scss";
import { RadioGroupButtons } from "../src/RadioGroupButtons.jsx";

export default {
  title: "components/RadioGroupButtons",
  component: RadioGroupButtons,
};

export function Default() {
  const radios = [
    { name: "Include (AND)", value: "include" },
    { name: "Expand (OR)", value: "expand" },
    { name: "Exclude (NOT)", value: "exclude" },
  ];
  const [radioValue, setRadioValue] = React.useState("Include");

  return (
    <>
      {radios.map((radio) => (
        <RadioGroupButtons
          key={radio.value}
          label={radio.name}
          value={radio.value}
          checked={radioValue === radio.value}
          onChange={(e) => setRadioValue(e.currentTarget.value)}
        />
      ))}
    </>
  );
}
