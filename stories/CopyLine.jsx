import colors from "../src/scss/colors.module.scss";
import { CopyLine } from "../src/CopyLine.jsx";

// list of hex values of colors
const colorOptions = Object.values(colors);

// create a map of color labels with hex values as keys and color names as values
const colorLabels = Object.entries(colors).reduce((acc, [key, value]) => {
  acc[value] = key;
  return acc;
}, {});

const colorControl = {
  options: colorOptions,
  control: {
    type: "select",
    labels: colorLabels,
  },
};

export default {
  title: "components/CopyLine",
  component: CopyLine,
  args: {
    children: "https://test.example.com/path",
    allowCopy: true,
    badgeText: "IRI",
    badgeColor: colors.orangeDark,
    buttonColor: colors.blueDark,
  },
  argTypes: {
    badgeColor: colorControl,
    buttonColor: colorControl,
    children: {
      control: "text",
    },
  },
};

export function Default({ children, ...args }) {
  return <CopyLine {...args}>{children}</CopyLine>;
}

export function WithoutBadge({ allowCopy }) {
  return (
    <CopyLine allowCopy={allowCopy}>
      http://example.com
    </CopyLine>
  );
}

export function WithBadgeDefaultColor() {
  return (
    <CopyLine badgeText="URI" allowCopy>
      http://linked.data.gov.au/dataset/states-territories/2-geometry
    </CopyLine>
  );
}

export function WithOtherBadgeColor() {
  return (
    <CopyLine badgeText="URI" badgeColor={colors.greenMid} allowCopy>
      http://example.com
    </CopyLine>
  );
}

export function WithOtherButtonColor() {
  return (
    <CopyLine badgeText="URI" badgeColor={colors.greenMid} buttonColor={colors.orangeDark} allowCopy>
      http://example.com
    </CopyLine>
  );
}
