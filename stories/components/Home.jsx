import { Banner } from "../../src/layout-components/Banner.jsx";

export function Home() {
  return (
    <>
      <Banner title="Welcome" />
      <div className="container">
        <div className="row main-content" />
      </div>
    </>
  );
}
