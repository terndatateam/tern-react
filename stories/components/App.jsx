import PropTypes from "prop-types";
import {
  Nav, NavItem, NavLink,
} from "reactstrap";

import { TopBar } from "../../src/TopBar.jsx";
import { DesktopAppHeader } from "../../src/layout-components/DesktopAppHeader.jsx";
import { Footer } from "../../src/layout-components/Footer.jsx";

export function App({ appHeader, children }) {
  return (
    <>
      <TopBar />
      <DesktopAppHeader fluid title={appHeader && (<h3>Demo Title</h3>)}>
        {appHeader && (
        <Nav navbar>
          <NavItem>
            <NavLink to="/" active>Home</NavLink>
          </NavItem>
          <NavItem>
            <NavLink to="/login">Sign&nbsp;In</NavLink>
          </NavItem>
        </Nav>
        )}
      </DesktopAppHeader>
      { children}
      <Footer />
    </>
  );
}

App.propTypes = {
  appHeader: PropTypes.bool,
  children: PropTypes.node.isRequired,
};
App.defaultProps = {
  appHeader: true,
};
