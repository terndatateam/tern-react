/* eslint-disable jsx-a11y/label-has-associated-control */
import { Col, Container, Row } from "reactstrap";

import "./SignIn.scss";

export function SignIn() {
  return (
    <Container fluid>
      <Row>
        <Container>
          <Row className="justify-content-center">
            <Col lg="6">
              <Row className="align-items-center" style={{ height: "100%" }}>
                <Col className="login-form-container">
                  <Row>
                    <Col>
                      <h2 className="text-center">Sign in with</h2>
                    </Col>
                  </Row>
                  <Row>
                    <KCForm />
                  </Row>
                  <Row>
                    <Col style={{ linHeight: "1" }}>
                      <small>
                        By continuing to login, you agree to our Terms and that you have read our
                        Data Use Policy, including our Cookie Use which can also be found
                        {" "}
                        <a href="https://www.tern.org.au/access-policy/">here</a>
                      </small>
                    </Col>
                  </Row>
                </Col>
              </Row>
            </Col>
            <Col lg="4" className="login-text-section">
              <Row className="login-text-container">
                <h3>
                  Have you signed up for our
                  {" "}
                  <a href="https://www.tern.org.au/" rel="noopener noreferrer" target="_blank">newsletter</a>
                  {" "}
                  ?
                </h3>
                <p>
                  Stay connected and see how other researchers are utilising
                  TERN&apos;s infrastructure and data.
                </p>
                <p>
                  We will send you project updates, data releases, research findings,
                  and user stories direct to your inbox.
                </p>
              </Row>
            </Col>
          </Row>
        </Container>
      </Row>
    </Container>
  );
}

function KCForm() {
  const posturl = (
    "https://auth-test.tern.org.au/auth/realms/tern/login-actions/authenticate?"
    + "session_code=r6fRIaN-qOxmwI--slUvLfx4USbZLm0ImiJdVUXesuo"
    + "&amp;execution=6fa357a1-6037-4d67-8330-96901139dbc4"
    + "&amp;client_id=account"
    + "&amp;tab_id=MoLoGq4ZCuc"
  );
  const forgotpw = "/auth/realms/tern/login-actions/reset-credentials?client_id=account&amp;tab_id=MoLoGq4ZCuc";
  const aaf = (
    "/auth/realms/tern/broker/aaf/login?"
    + "client_id=account&amp;tab_id=MoLoGq4ZCuc&amp;session_code=r6fRIaN-qOxmwI--slUvLfx4USbZLm0ImiJdVUXesuo"
  );
  const google = (
    "/auth/realms/tern/broker/google/login?"
    + "client_id=account&amp;tab_id=MoLoGq4ZCuc&amp;session_code=r6fRIaN-qOxmwI--slUvLfx4USbZLm0ImiJdVUXesuo"
  );
  return (
    <div id="kc-form" className="col">
      <div id="kc-form-wrapper" className="row justify-content-center">
        <form id="kc-form-login" onSubmit={() => true} action={posturl} method="post">
          <div>
            <label htmlFor="username">Username or email</label>
            <input tabIndex="0" id="username" name="username" defaultValue="" type="text" autoComplete="off" />
          </div>
          <div>
            <label htmlFor="password">Password</label>
            <input tabIndex="0" id="password" name="password" type="password" autoComplete="off" />
          </div>
          <div>
            <div id="kc-form-options" />
            <div>
              <span><a tabIndex="0" href={forgotpw}>Forgot Password?</a></span>
            </div>
          </div>
          <div id="kc-form-buttons">
            <input type="hidden" id="id-hidden-input" name="credentialId" />
            <input tabIndex="0" className="   " name="login" id="kc-login" type="submit" value="Sign in" />
          </div>
        </form>
      </div>
      <div id="kc-social-providers" className="row justify-content-center">
        <ul className="login-pf-social list-unstyled login-pf-social-all ">
          <li className="login-pf-social-link">
            <a href={aaf} id="zocial-aaf" className="zocial oidc">
              {" "}
              <span>AAF</span>
            </a>
          </li>
          <li className="login-pf-social-link">
            <a href={google} id="zocial-google" className="zocial oidc">
              {" "}
              <span>Google</span>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
}
