import { Container } from "reactstrap";
import { PageNotFound } from "../src/PageNotFound.jsx";

export default {
  title: "components/PageNotFound",
  component: PageNotFound,
};

export function Default() {
  return (
    <Container>
      <PageNotFound supportEmail="esupport@tern.org.au" />
    </Container>
  );
}
