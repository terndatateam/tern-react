import { Nav, DropdownItem } from "reactstrap";

import "../src/index.scss";
// include AppHeader scss to set navitem colors
import "./LginNavItem.scss";
import { LoginNavItem } from "../src/LoginNavItem.jsx";

export default {
  title: "components/LoginNavItem",
  component: LoginNavItem,
  args: {
    login: {
      user: null,
      checking: false,
    },
  },
  // argTypes: {},
};

export function Default(args) {
  return (
    <Nav navbar>
      <LoginNavItem {...args} />
    </Nav>
  );
}

export const Checking = Default.bind({});

Checking.args = {
  login: {
    checking: true,
  },
};

export const WithUser = Default.bind({});

WithUser.args = {
  login: {
    user: { name: "User Name" },
  },
};

export function WithItems(args) {
  return (
    <Nav navbar>
      <LoginNavItem {...args}>
        <DropdownItem>
          User Option 1
        </DropdownItem>
      </LoginNavItem>
    </Nav>
  );
}

WithItems.args = {
  login: {
    user: { name: "User Name" },
  },
};
