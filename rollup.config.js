import path from "path";

import autoprefixer from "autoprefixer";
import babel from "@rollup/plugin-babel";
import commonjs from "@rollup/plugin-commonjs";
import image from "@rollup/plugin-image";
import copy from "rollup-plugin-copy";
import postcss from "rollup-plugin-postcss";
import atImport from "postcss-import";

// import postcssurl from "postcss-url";
import resolve from "@rollup/plugin-node-resolve";

import pkg from "./package.json";
// enable import of json files
// import json from '@rollup/plugin-json';

const outdir = path.dirname(pkg.main);

export default [
  // CommonJS (for Node) and ES module (for bundlers) build.
  // (We could have three entries in the configuration array
  // instead of two, but it's quicker to generate multiple
  // builds from a single configuration where possible, using
  // an array for the `output` option, where we can specify
  // `file` and `format` for each target)
  {
    input: pkg.source,
    output: [
      {
        dir: outdir,
        format: "es",
        preserveModules: true,
        preserveModulesRoot: "src",
        sourcemap: true,
        paths: (id) => {
           // babel generates some imports, but doesn't add file extensions as required by ESM import spec.
           // react/jsx-runtime import requires js extenion
           if ("react/jsx-runtime" === id) { return "react/jsx-runtime.js" };
           // append .js to babel runtime import
           // if (/@babel\/runtime\/helpers\/esm\/[^\.]+/.test(id)) { return `${id}.js` };
        },
      },
    ],
    external: (id) => {
      // console.log(`Check external ${id}`);
      // use https://www.npmjs.com/package/rollup-plugin-peer-deps-external ?
      if (pkg.peerDependencies[id]) { return true; }
      // date-fns hack because we import things as "date-fns/*"
      if (/date-fns\/.*/.test(id)) { return true; }
      // lodash hack because we import things as "lodash/*"
      if (/lodash\/.*/.test(id)) { return true; }
      // don't bundle babel runtime
      if (/@babel\/runtime/.test(id)) { return true; }
      // don't bundle react jsx runtime
      if (/react\/jsx-runtime/.test(id)) { return true; }
      // images are external modules as well ... other rollup plugins take car of them.
      // if (path.extname(id).match(/\.(png|jpg|svg|woff2|woff)/)) { return true; }
      return false;
    },
    plugins: [
      resolve({
        mainFields: ["browser", "module", "jsnext", "main"],
        extensions: [".mjs", ".js", ".jsx", ".json", ".node"],
      }),
      commonjs({
        include: /\/node_modules\//,
      }),
      // json(),
      babel({
        // should be useable without babel
        exclude: ["node_modules/**"],
        // extensions
        extensions: [".ts", ".tsx", ".js", ".jsx", ".es6", ".es", ".mjs"],
        // recommended for libraries
        babelHelpers: "runtime",
        // babel configuration
        presets: [
          // rollup is takeing care of common js modules, don't let babel do it as well
          "@babel/preset-env",
          [
            "@babel/preset-react", {
              runtime: "automatic",
            },
          ],
        ],
        plugins: [
          // needed for babelHelpers runtime
          // "@babel/plugin-transform-runtime",
          ["@babel/plugin-transform-runtime", { useESModules: true }],
        ],
      }),
      image({
      }),
      // Asset handling
      postcss({
        plugins: [
          atImport(),
          // add vendor prefixes (TODO: do I really need that in library?)
          autoprefixer(),
          // copy assets referenced in css . files
          // postcssurl([
          // //  { filter: "**/*.svg", url: "rebase"},
          // // rewrite font urls in css files
          // //   { filter: "**/*.woff",
          // //     url: (asset) => {
          // //       const dest = path.join("./", "font", asset.pathname)
          // //       console.log("filter", asset, asset.pathname, " -> ", dest)
          // //       return dest
          // //     }
          // //   },
          // //   // { filter: "src/**/*.woff", basePath: '.', url: 'copy',
          // //        assetsPath: 'dist', to: "fonts2", useHash: false },
          // ]),
        ],
        autoModules: true,
        inject: false,
        extract: true, // false -> keep css in js file ?
        sourceMap: true,
        // modules: true, // treat all css as modules ... will render a js file
        //          with exports for all style names and variables
        // to: ... path that postcss can use to resolve, rebase and copy assets
      }),
      // copy all asset files
      copy({
        targets: [
          {
            // TODO: had to add src/images ...
            //       included in scss only ... but not copied by postcss ??
            src: ["src/fonts", "src/images", "src/scss", "src/index.scss"],
            dest: outdir,
          },
        ],
      }),
    ],
  },
];
