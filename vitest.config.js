/* eslint-disable import/no-default-export */
/* eslint-disable import/no-unresolved */
import react from "@vitejs/plugin-react";
import { defineConfig } from "vitest/config";

export default defineConfig({
  plugins: [react()],
  test: {
    environment: "jsdom",
    globals: true,
    setupFiles: "tests/setup.js",
    coverage: {
      reporter: ["text", "html"],
    },
  },
});
