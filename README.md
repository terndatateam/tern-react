# tern-react

> TERN common React components

[![NPM](https://img.shields.io/npm/v/tern-react.svg)](https://www.npmjs.com/package/tern-react) [![JavaScript Style Guide](https://img.shields.io/badge/code_style-standard-brightgreen.svg)](https://standardjs.com)

## Changelog

See [CHANGELOG.md](CHANGELOG.md).

## Development

Just clone and install the project as usually with npm.

```bash
npm install
```
To run watched unit tests with web ui, terminal and coverage (``recommended``).

```bash
npm run test:ui
```
To run watched unit tests with terminal and coverage.

```bash
npm run test:cli
```
To run headless unit tests with terminal (ci/cd).

```bash
npm run test:ci-cd
```

This project includes a full storybook setup to help with development, testing and demoing provided components.

```bash
npm run storybook
```

## Usage

Just install this pkg like any other npm pkg and start using it in your project as usual.

Here an example for a page including the full stylesheet provided by this pkg.

```jsx
import { Component } from 'react';
// import style first
import 'tern-react/index.css';

import { TopBar, AppHeader, Footer } from 'tern-react';

function Page() {
  return (
    <TopBar />
    <AppHeader />
    <Footer />
  );
}
```

## Testing Build

To test the build of tern-react before merging to develop, you can run:

```bash
npm pack
```
This will generate a .tgz file which you can reference the directory path inside another application by updating the package.json:

```js
"tern-react": "file:/pathto/tern-react/tern-react-2.0.0-dev.20.tgz",
```
The package must be installed to the UI with
```bash
npm install
```
Remember to revert the change in your package.json before committing inside the UI.
## License

MIT © [](https://github.com/)
