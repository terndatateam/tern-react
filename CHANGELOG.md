# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).


## [2.0.0] - 2024-05-06 (unreleased)

### Added

- Link to TERN Account app as Item in the LoginNavItem component
- Vite bundling
- Vitest framework.
- Initial unit tests (for BlockUi and Map components)

### Changed

- Update all dependencies
- Requires React 17
- Add version handling scripts to package.json
- Add Checkbox, DropDown, TexField, RadioButtonGroup facet components
- Convert all exports to named exports
- Import React no longer needed with React >= 17
- Modernized packaging (require node >=15)
- Add BlockUi component as common component
- Add Map component as common component
- Update CI/CD pipeline to use `Node 18`.


## [1.5.0] - 2022-01-12

### Added

- Development uses Webpack v5.


## [1.4.0] - 2022-01-10

### Added

- Support for Node 16 LTS

### Fixed

- Footer - key operating partners styling


## [1.3.0] - 2022-01-10

### Added

- Support for React 17 on Node 14 LTS
